FROM registry.gitlab.com/minutz/web/base AS build
COPY . .
RUN yarn install
RUN ng build --configuration=beta

FROM nginx
ADD default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /home/app/dist/minutz /usr/share/nginx/html
EXPOSE 80
