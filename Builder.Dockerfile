FROM node:11 AS build
LABEL NAME="MINUTZ-WEB BUILD IMAGE"
LABEL CLI-VERSION="6.0.0"
RUN useradd --user-group --create-home --shell /bin/false app
ENV HOME=/home/app
WORKDIR $HOME
RUN npm install -g @angular/cli@6.0.0 \
    && npm install @angular/compiler-cli@6.0.0 --save \
    && npm install yarn
COPY package.json package.json
RUN yarn install