# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Planned

- Payment Gateway integration
- Mult-tenant switcher

### Changed

## 2018-11-01

### Added

- Create meeting
- Manage Meeting
- Manage Actions
- Manage Users
