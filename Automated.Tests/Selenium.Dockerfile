FROM node:11

LABEL Author="Lee-Roy Ashworth <leeroya@gmail.com>"
RUN npm install -g selenium-side-runner
ADD docker-entrypoint.sh /opt/bin/docker-entrypoint.sh
RUN chmod +x /opt/bin/docker-entrypoint.sh

RUN mkdir /Selenium.Tests

WORKDIR /root
VOLUME [ "/Selenium.Tests" ]
CMD "/opt/bin/docker-entrypoint.sh"