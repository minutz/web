# Minutz : Web Application

# Version

- 2.1.0

To update the application version please increase the file in the package.json in the root of the project.

# IDE recomendation

- IDE: [Visual Studio Code](https://code.visualstudio.com/)
- VSCode Plugins: Debugger for Chrome
- VSCode Plugins: ESLint
- Git Application: [GitKraken](https://www.gitkraken.com/)
- Build: [Docker](https://www.docker.com)

### Current Theme Template:

- [sb-admin-bs4-angular4](sstartbootstrap.com/template-overviews/sb-admin-2/).
- angular-v6.0.0
- angular/cli-v6.0.0
- https://ng-bootstrap.github.io
- [Bootstrap 4](https://www.getbootstrap.com) (tested with 4.0.0-beta)
- **node >=v6.9.0 and npm >=3**.
- Docker [Linux images]
- [ng4-select2](https://www.npmjs.com/package/ng4-select2)
- [Demo](https://github.com/NejcZdovc/ng2-select2-demo/)

`NOTE:` The project is currently undergoing Google Material implementation and basic Material components are being implemented.

# Base framework

    npm install -g @angular/cli

# Install the project's dependencies

    $ yarn install

To get json-server run:

    npm install -g json-server

To serve the application you can run the VSCode task: `debug`.

# Run the code

If you do not have VSCode installed then run in terminal from the src folder: `ng serve --port 4500 --configuration=beta`

Navigate to `https://localhost:4500/` in your browser.

The app will automatically reload if you change any of the source files.

If you have VSCode installed then run the task called: `start json-server`
this will start the mock API using json-server with the mock data,
alternatively run : `json-server .run/server.json --routes .run/routes.json --port 5000 `
from the src folder to serve the json mock data as a API.

`NOTE:` Some auth items have been side stepped, the dashboard has been made to load when the app starts ensure that you navigate to the login screen:

    http://localhost:4500/login

If there are any items skipped, clear the session storage or open a new tab.

# Build

    ng build --prod

## Code scaffolding

    Run `ng generate component component-name`

to generate a new component.

You can also use

    `ng generate directive/pipe/service/class/module`.

## Running unit tests

    Run `ng test`

to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

    Run `ng e2e`

to execute the end-to-end tests via [Protractor](https://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
