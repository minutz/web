export const environment = {
    production: true,
    type: 'beta',
    reportsApi: 'https://reports.minutz.net/api/Reports',
    notifyApi: 'https://notify.minutz.net/api/message',
    redirectUri: 'https://beta.minutz.net',
    apiUri: 'https://core.minutz.net',
    authUri: 'https://account.minutz.net',
    attApiUri: 'https://beta-attachments.minutz.net',
    version: require('../../package.json').version
  };
