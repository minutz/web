export const environment = {
  production: true,
  type: '',
  reportsApi: 'https://reports.minutz.net/api/Reports',
  notifyApi: 'https://notify.minutz.net/api/message',
  redirectUri: 'https://beta.minutz.net',
  apiUri: 'https://api.minutz.net',
  authUri: 'http://localhost:5003',
  attApiUri: 'https://attachments.minutz.net',
  version: require('../../package.json').version
};
