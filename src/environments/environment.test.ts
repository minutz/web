export const environment = {
    production: true,
    type: 'test',
    reportsApi: 'https://reports.minutz.net/api/Reports',
    notifyApi: 'https://notify.minutz.net/api/message',
    redirectUri: 'http://localhost:4200',
    apiUri: 'http://localhost:5003',
    authUri: 'https://account.minutz.net',
    attApiUri: 'https://attachments.minutz.net',
    version: require('../../package.json').version
  };
