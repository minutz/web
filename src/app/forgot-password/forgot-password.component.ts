import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
const { version: appVersion } = require('package.json');

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public appVersion;
  public loading = false;
  username = new FormControl();
  constructor() { }

  ngOnInit() {
    this.appVersion = appVersion;
  }
  submit() {
    console.log('submit');
  }
}
