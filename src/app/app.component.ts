import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from './shared/services/authentication.service';
import { UrlService } from 'src/app/shared/services';
import { Intercom } from 'ng-intercom';

import { environment } from 'src/environments/environment';
import { UserProfile } from './shared/models';

declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  private _isAuthenticated: boolean;
  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  public set isAuthenticated(v: boolean) {
    this._isAuthenticated = v;
  }
  _user: UserProfile;
  public get user(): UserProfile {
      return this._user;
  }
  public set user(v: UserProfile) {
      this._user = v;
  }
  constructor(
    public router: Router,
    public authService: AuthenticationService,
    private activeRoute: ActivatedRoute,
    private urlService: UrlService,
    public intercom: Intercom
  ) { }

  ngOnInit() {
    this._isAuthenticated = this.authService.isAuthenticated();
    this.activeRoute.queryParams.subscribe(q => {
        this.urlService.checkQueryParameters(window.location.href);
    });
    // todo: check if meeting item is in the url then load the meeting
    if (this.router.url === '/') {
      this.router.navigate(['/dashboard']);
    }
      this.intercom.boot({
          app_id: 'oili1ns3',
          // Supports all optional configuration.
          widget: {
              'activator': '#intercom'
          }
      });

  }
  toggle() {
    $('#sidebar').toggleClass('collapse');
  }
}
