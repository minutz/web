import { UserProfile } from 'src/app/shared/models';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/services/authentication.service';
import { LocalStorageService } from '../shared/services/local-storage.service';
import { SessionStorageService } from '../shared/services/session-storage.service';
import { environment } from '../../environments/environment';
import { MatSnackBar } from '@angular/material';
import { LoginModel } from '../shared/models/login-model';
import { ProfileService } from '../shared/services/profile.service';
import { FormControl } from '@angular/forms';
const { version: appVersion } = require('package.json');

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public appVersion;
    username = new FormControl();
    password = new FormControl();
    public loading = false;
    private _login: LoginModel;
    public set login(v: LoginModel) {
        this._login = v;
    }
    public get login(): LoginModel {
        return this._login;
    }

    constructor(
        public router: Router,
        private _authenticationService: AuthenticationService,
        private _localStorageService: LocalStorageService,
        private _sessionStorageService: SessionStorageService,
        private _profileService: ProfileService,
        public snack: MatSnackBar
    ) {
        this.appVersion = appVersion;
        this.login = new LoginModel({ username: '', password: '', instanceId: '' });
    }

    ngOnInit() {
    }

    public Login() {
        if (this.login.username) {
            if (this.login.password) {
                this.loading = true;
                this._authenticationService.login(this.login.username, this.login.password).subscribe(
                    profile => {
                        this._profileService.applicationSet(profile);
                        this._profileService.set(profile);
                        this.loading = false;
                        window.location.href = environment.redirectUri;
                    }, err => {
                        this.snack.open(`oops ! something went wrong, ${err.error}`, '', { duration: 2000 });
                        this.loading = false;
                        this.login = new LoginModel();
                    });
            } else {
                this.snack.open('Please provide a password', '', { duration: 2000 });
                this.login = new LoginModel();
            }
        } else {
            this.snack.open('Please provide a username', '', { duration: 2000 });
            this.login = new LoginModel();
        }
    }

    public ForgotPassword() {
        window.location.href = `${environment.authUri}/account/ForgotPassword`;
    }
}
