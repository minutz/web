import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { BreadCrumbComponent } from 'src/app/shared/components/bread-crumb/bread-crumb.component';
import { ServicesModule } from './services/services.module';
import { RepositoriesModule } from './repositories/repositories.module';
import { MeetingRepositoryModule } from './repositories/features/meeting-repository.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';


@NgModule({
    imports: [
    BrowserAnimationsModule,
    MatTooltipModule,
        CommonModule,
        ServicesModule,
        RepositoriesModule,
        MeetingRepositoryModule
    ],
    exports: [
        CommonModule,
        BreadCrumbComponent
    ],
    declarations: [
        BreadCrumbComponent
    ],
    providers: [],
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: []
        };
    }
}
