import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingRecurrenceComponent } from './meeting-recurrence.component';

describe('MeetingRecurrenceComponent', () => {
  let component: MeetingRecurrenceComponent;
  let fixture: ComponentFixture<MeetingRecurrenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingRecurrenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingRecurrenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
