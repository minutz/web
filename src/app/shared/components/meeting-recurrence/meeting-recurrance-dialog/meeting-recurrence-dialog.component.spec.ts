import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingRecurrenceDialogComponent } from './meeting-recurrence-dialog.component';

describe('MeetingRecurrenceDialogComponent', () => {
  let component: MeetingRecurrenceDialogComponent;
  let fixture: ComponentFixture<MeetingRecurrenceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingRecurrenceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingRecurrenceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
