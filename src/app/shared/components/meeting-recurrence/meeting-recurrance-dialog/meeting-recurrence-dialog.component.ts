import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-meeting-recurrence-dialog',
    templateUrl: './meeting-recurrence-dialog.component.html',
    styleUrls: ['./meeting-recurrence-dialog.component.scss']
})
export class MeetingRecurrenceDialogComponent implements OnInit {
    inviteMessage: string;
    agendaOption: boolean;
    attachmentOption: boolean;
    toInput: string;
    constructor(
        public dialogRef: MatDialogRef<MeetingRecurrenceDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
