import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {MeetingModel, RecurrenceType, UserProfile} from '../../models';
import {MeetingService} from '../../services/meeting.service';
import {ProfileService} from '../../services/profile.service';
import {
    MeetingRecurrenceService
} from '../../repositories/features/meeting';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {MatDialog} from '@angular/material';
import {MeetingRecurrenceDialogComponent} from './meeting-recurrance-dialog/meeting-recurrence-dialog.component';
import { BreadcrumbService, UrlService } from '../../services';

@Component({
    selector: 'app-meeting-recurrence',
    templateUrl: './meeting-recurrence.component.html',
    styleUrls: ['./meeting-recurrence.component.scss']
})
export class MeetingRecurrenceComponent implements OnInit {
    mode: string;
    canEdit: boolean;
    private _meetingDate: Date;
    private _weekDay: number;
    private _monthDay: number;
    private _month: number;
    private _meetingRecurrenceType: string;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }


    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _meetingTypes: RecurrenceType[];
    public get meetingTypes(): RecurrenceType[] {
        return this._meetingTypes;
    }

    public set meetingTypes(v: RecurrenceType[]) {
        this._meetingTypes = v;
    }

    private _initialState: string;
    public get initialState(): string {
        return this._initialState;
    }

    public set initialState(v: string) {
        this._initialState = v;
    }

    public get meetingRecurrenceType(): string {
        return this._meetingRecurrenceType;
    }

    public set meetingRecurrenceType(v: string) {
        this._meetingRecurrenceType = v;
    }

    public get month(): number {
        return this._month;
    }

    public set month(v: number) {
        this._month = v;
    }

    public get monthDay(): number {
        return this._monthDay;
    }

    public set monthDay(v: number) {
        this._monthDay = v;
    }

    public get weekDay(): number {
        return this._weekDay;
    }

    public set weekDay(v: number) {
        this._weekDay = v;
    }


    public get meetingDate(): Date {
        return this._meetingDate;
    }


    public set meetingDate(v: Date) {
        this._meetingDate = v;
    }

    closeResult: string;

    constructor(
        private _modalService: NgbModal,
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _meetingRecurrenceService: MeetingRecurrenceService,
        public dialog: MatDialog,
        public _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService,
    ) {
        if (!this.meeting) {
            this.meeting = new MeetingModel();
            this.meeting.recurrenceType = 0;
        }
    }

    ngOnInit() {
        this.initialState = this._meetingRecurrenceType;
        this.user = this._profileService.get();
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this._meeting.recurrenceType = Number(this.meeting.reacuranceType);
            const meetingDate = new Date(this.meeting.date);
            this._meetingTypes = [];
            this._meetingTypes = this._meetingService.RecurrenceTypes(meetingDate);
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._breadcrumbService._LastUpdated$.subscribe( q => {
            this.mode = this._urlService.getModeQueryString(window.location.href);
        });
    }
    public openRecurrenceModal(content: ElementRef): void {
        // this._modalService.open(content);
        this._modalService.open(content).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(MeetingRecurrenceDialogComponent, {
            width: '650px',
            data: { }
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this._meetingService.Meeting(this._meeting, this._user);
            // this.animal = result;
        });
    }

    public save(): void {
    }

    public onChange($event): void {
        if ($event === '7') {
            this.openDialog();
        }
        if ($event > 0) {
            this._meeting.isRecurrence = true;
        }
        this._meeting.recurrenceType = $event.id;
        this._meetingRecurrenceService.update(this._user, this._meeting).subscribe( result => {
            // this._meetingService.Meeting(this._meeting, this._user);
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

}
