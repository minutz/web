import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    Renderer2
} from '@angular/core';
import { MeetingService, UrlService, ProfileService } from "../../services";
import { UserProfile } from '../../models';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
    @Output() editorEmitter = new EventEmitter();
    @Output() editorBlurred = new EventEmitter();
    @Output() editorFocused = new EventEmitter();
    private _editorContent: string;
    canEdit: boolean;
    instanceId: string;
    meetingId: string;
    mode: string;
    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    public options: Object = {
        placeholderText: 'Edit Your Content Here!',
        charCounterCount: false,
        toolbarButtons: [
            'undo',
            'redo',
            'bold',
            'italic',
            'underline',
            'strike'
        ]
    };

    public get editorContent(): string {
        return this._editorContent;
    }

    @Input()
    public set editorContent(v: string) {
        this._editorContent = v;
        this.editorEmitter.emit(this._editorContent);
    }

    constructor(
        private renderer: Renderer2,
        private el: ElementRef,
        private _meetingService: MeetingService,
        private _urlService: UrlService,
        private _profileService: ProfileService
    ) {}

    public onEditorFocused(quill) {
        this.meetingId = this._urlService.getMeetingQueryString(
            window.location.href
        );
        this.instanceId = this._urlService.getInstanceQueryString(
            window.location.href
        );
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe(
            (message: any) => {
                this.canEdit = this._meetingService.canEdit(
                    message.data.meeting,
                    this.user,
                    this.mode
                );
                if (this.canEdit) {
                }
            }
        );
        // console.log('editor focus!', quill);
        this.editorFocused.emit(this.editorContent);
    }
    public onEditorCreated(quill) {
        this.meetingId = this._urlService.getMeetingQueryString(
            window.location.href
        );
        this.instanceId = this._urlService.getInstanceQueryString(
            window.location.href
        );
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe(
            (message: any) => {
                this.canEdit = this._meetingService.canEdit(
                    message.data.meeting,
                    this.user,
                    this.mode
                );
                if (this.canEdit) {
                }
            }
        );
    }
    public onEditorBlured(quill) {
        this.meetingId = this._urlService.getMeetingQueryString(
            window.location.href
        );
        this.instanceId = this._urlService.getInstanceQueryString(
            window.location.href
        );
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe(
            (message: any) => {
                this.canEdit = this._meetingService.canEdit(
                    message.data.meeting,
                    this.user,
                    this.mode
                );
                if (this.canEdit) {
                    this.editorBlurred.emit(this.editorContent);
                }
            }
        );
    }
    public ngOnInit() {}

    onKey($event) {
        this.meetingId = this._urlService.getMeetingQueryString(
            window.location.href
        );
        this.instanceId = this._urlService.getInstanceQueryString(
            window.location.href
        );
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe(
            (message: any) => {
                this.canEdit = this._meetingService.canEdit(
                    message.data.meeting,
                    this.user,
                    this.mode
                );
                if (this.canEdit) {
                    this.editorBlurred.emit(this.editorContent);
                }
            }
        );
    }

    onContentChanged() {
        this.editorEmitter.emit(this.editorContent);
    }
}
