import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import {UserProfile} from '../../../models';
import {SignUpRepositoryService, UserRepositoryService} from '../../../repositories';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../../services/local-storage.service';
import {AuthenticationService} from '../../../services/authentication.service';
import {ProfileService} from '../../../services/profile.service';
import { UrlService } from '../../../services';

@Component({
  selector: 'app-header-user',
  templateUrl: './header-user.component.html',
  styleUrls: ['./header-user.component.scss']
})
export class HeaderUserComponent implements OnInit {
  private _isAuthenticated: boolean;
  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  public set isAuthenticated(v: boolean) {
    this._isAuthenticated = v;
  }

  private _user: UserProfile;

  public get user(): UserProfile {
    return this._user;
  }

  public set user(v: UserProfile) {
    this._user = v;
  }

  private _mode: string;

  public get mode(): string {
    return this._mode;
  }

  public set mode(v: string) {
    this._mode = v;
  }

  constructor(
    public _authenticationService: AuthenticationService,
    private _urlService: UrlService,
    private _profileService: ProfileService
  ) { }

  ngOnInit() {
    this.user = this._profileService.get();
    this.mode = this._urlService.getModeQueryString(window.location.href);
  }

  public onLoggedOut(): void {
    this._authenticationService.logout();
  }

  public signUp(): void {
    this._authenticationService.navigateSignUp();
  }
}
