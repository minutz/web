import { AfterViewInit, Component, OnInit } from '@angular/core';
import { UserProfile } from '../../models';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { ProfileService } from '../../services/profile.service';
import {
    SignUpRepositoryService,
    UserRepositoryService
} from '../../repositories';
import { environment } from '../../../../environments/environment';
import { SideMenuService } from '../../services/side-menu.service';
import { NotificationsService } from '../../services/notifications.service';
import { LoadingService } from '../../services';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
    public loading = false;

    sideElement: any;
    pushRightClass = 'push-right';
    collapse = 'collapse';

    private _isAuthenticated: boolean;
    public get isAuthenticated(): boolean {
        return this._isAuthenticated;
    }
    public set isAuthenticated(v: boolean) {
        this._isAuthenticated = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    constructor(
        public router: Router,
        public _authenticationService: AuthenticationService,
        private _localStorageService: LocalStorageService,
        private _signUpRepo: SignUpRepositoryService,
        private _userRepository: UserRepositoryService,
        private _profileService: ProfileService,
        private _sideMenuService: SideMenuService,
        private _notificationService: NotificationsService,
        private _loadingService: LoadingService
    ) {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
        this._loadingService.onChange.subscribe(loadingEvent => {
            this.loading = loadingEvent.loading;
        });
    }

    ngOnInit() {
        this._profileService._Profile$.subscribe(profile => {
            this.user = profile;
            this.isAuthenticated = this._authenticationService.isAuthenticated();
        });
        if (!this.user) {
            const profile = this._profileService.get();
            this._profileService.Profile(profile);
        }
        this._sideMenuService._SideBar$.subscribe(domElement => {
        });
        if (!this.sideElement) {
            const dom: any = document.getElementById('sidebar');
            this._sideMenuService.SideBar(dom);
        }
        this._notificationService._Loading$.subscribe(loading => {
            this.loading = loading;
        });
    }

    ngAfterViewInit() {
        this.toggleSidebar();
    }
    public isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    public toggleSidebar(): void {
        const dom: any = document.getElementById('sidebar');
        if (dom) {
            dom.classList.toggle(this.collapse);
            this._sideMenuService.SideBar(dom);
        }
    }

    public rltAndLtr(): void {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    public onLoggedOut(): void {
        this._authenticationService.logout();
    }

    public signUp(): void {
        this._authenticationService.navigateSignUp();
    }
}
