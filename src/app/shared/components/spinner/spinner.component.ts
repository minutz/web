import { Component, Input, OnInit } from '@angular/core';

import { MeetingModel, UserProfile } from '../../models';

import { MeetingService, ProfileService, UrlService, BreadcrumbService } from '../../services';
import { MeetingDurationService, MeetingTimeService } from '../../repositories/features/meeting';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss']
})

export class SpinnerComponent implements OnInit {

    @Input() type: string;
    icon: string;
    data: any;
    mode: string;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;

    private _user: UserProfile;
    private _meeting: MeetingModel;

    public get user(): UserProfile {
        return this._user;
    }

    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }
    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        public _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService
    ) {
        this._meeting = new MeetingModel();
    }

    getFullMinutes() {
        if (new Date().getMinutes() < 10) {
            return '0' + new Date().getMinutes();
        }
        return new Date().getMinutes();
    }

    ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._user = this._profileService.get();

        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            if (this._meeting.time === '0:00') {
                this._meeting.time = new Date().getHours() + ':' + this.getFullMinutes();
            } else if (this._meeting.duration === 0) {
                this._meeting.duration = 60;
            }
            this.setValue(this.type);
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService.meetingDurationChangeMessage.subscribe((data: any) => {
            this._meeting.duration = data.message;
        });
        this._meetingService.meetingTimeChangeMessage.subscribe((data: any) => {
            this._meeting.time = data.message;
        });
    }

    setValue(type: string): void {
        if (type) {
            switch (type) {
                case 'time': {
                    this.icon = 'fa fa-clock-o recurrence';
                    this.data = this._meeting.time;
                    break;
                }
                case 'number': {
                    this.icon = 'fa fa-hourglass-half recurrence';
                    this.data = this._meeting.duration;
                    break;
                }
                default: {
                    this.icon = 'fa recurrence';
                    this.data = 0;
                    break;
                }
            }
        }
    }

    update(value: any): void {
        if (this.type) {
            switch (this.type) {
                case 'time': {
                    this._meeting.time = value;
                    this._meetingService.sendGroupMessage({
                        task: 'meetingTimeChange',
                        instanceId: this.instanceId,
                        meetingId: this.meetingId,
                        data: { time : value }
                      }, this.meetingId);
                    break;
                }
                case 'number': {
                    this._meeting.duration = value;
                    this._meetingService.sendGroupMessage({
                        task: 'meetingDurationChange',
                        instanceId: this.instanceId,
                        meetingId: this.meetingId,
                        data: { duration : value }
                      }, this.meetingId);
                    break;
                }
                default: {
                    this.data = 0;
                    break;
                }
            }
        }
    }
}
