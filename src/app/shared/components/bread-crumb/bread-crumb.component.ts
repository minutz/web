import { Component, ElementRef, OnInit, Input } from '@angular/core';
import { BreadcrumbService, SessionStorageService, ProfileService, MeetingService } from '../../services';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../environments/environment';
import { UrlService } from 'src/app/shared/services/url.service';
import { InviteAttendees } from 'src/app/shared/emums/invite-attendees.enum';
import {
    Router
} from '@angular/router';
import {UserProfile, MeetingInviteModel, MeetingAgenda, MeetingModel, MeetingAttendee} from 'src/app/shared/models';
import { MeetingStatusService, MeetingAttendeeService } from '../../repositories/features/meeting';
import { MatDialog, MatDialogConfig, DialogPosition } from '@angular/material';
import { PrintPreviewComponent } from './modal-windows/print-preview/print-preview.component';
import { MeetingInfoComponent } from './modal-windows/meeting-info/meeting-info.component';
import { SendInvitationComponent } from './modal-windows/send-invitation/send-invitation.component';
import {MeetingRepositoryService} from '../../repositories';

@Component({
    selector: 'app-bread-crumb',
    templateUrl: './bread-crumb.component.html',
    styleUrls: ['./bread-crumb.component.scss']
})
export class BreadCrumbComponent implements OnInit {
    @Input() feature: string;
    @Input() page: string;
    @Input() icon: string;
    public modalReference: any;
    public options: NgbModalOptions;
    public inviteMessage: string;
    public agendaOption: boolean;
    public attachmentOption: boolean;
    public InviteAttendees = InviteAttendees;
    public toInput: number;
    public canEdit: boolean;
    public availableAttendees: Array<MeetingAttendee>;
    meetingAttendees: any;
    instanceId: string;
    meetingId: string;


    PageTitle: string;
    PageHeading: string;
    tempMeetingNameAndLocation = false;
    twoORMoreAttendees = false;
    atLeastOneAgenda = false;
    heading: string;
    message: string;
    mode: string;

    hasAgenda: boolean;
    isActiveStopMeeting = false;
    isActivePublishMinutes = false;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(
        public _breadcrumbService: BreadcrumbService,
        public _urlService: UrlService,
        public _router: Router,
        private _meetingAttendeeService: MeetingAttendeeService,
        private _meetingStatusService: MeetingStatusService,
        private _meetingRepositoryService: MeetingRepositoryService,
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private dialog: MatDialog,
        private router: Router) {
        this.message = '';
    }

    ngOnInit() {
        this.user = this._profileService.get();
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);

        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this.meeting = message.data.meeting;
            if (this.mode) {
                this._meeting.status = this.mode;
                switch (this.mode) {
                    case 'create':
                        this.PageHeading = 'Create Meeting';
                        break;
                    case 'edit':
                        this.PageHeading = 'Edit Meeting';
                        break;
                    case 'view':
                        this.PageHeading = 'View Meeting';
                        break;
                    case 'run':
                        this.PageHeading = 'Run Meeting';
                        this.isActiveStopMeeting = true;
                        this.isActivePublishMinutes = false;
                        break;
                }
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            }
        });

        this._meetingService.addMeetingAttendeeMessage.subscribe((message: any) => {
            if (!message.data.attendee.picture) {
                message.data.attendee.picture = 'default';
            }
            if (message.data.attendee.referenceId === '00000000-0000-0000-0000-000000000000') {
                message.data.attendee.referenceId = this.meetingId;
            }
            if (!message.data.attendee.name) {
                message.data.attendee.name = message.data.attendee.email.split('@')[0].replace('.', ' ');
            }
            this.meetingAttendees.push(message.data.attendee);
        });

        this._meetingService.meetingattendeeCollectionMessage.subscribe((message: any) => {
            this.meetingAttendees = [];
            message.data.attendees.forEach((m: any) => {
                if (!m.picture) {
                    m.picture = 'default';
                }
                if (!m.instanceId) {
                    m.instanceId = this.instanceId;
                }
                if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                    m.referenceId = this.meetingId;
                }
                if (!m.name) {
                    m.name = m.email.split('@')[0].replace('.', ' ');
                }
                this.meetingAttendees.push(m);
                this._meetingService.CurrentAttendee(m);
            });
            if (this.meetingAttendees.length > 1) { this.twoORMoreAttendees = true; }
        });

        this._meetingService.meetingAvailableCollectionMessage.subscribe((message: any) => {
            this.availableAttendees = [];

            message.data.attendees.forEach( (m: any) => {
                if (!m.picture) {
                    m.picture = 'default';
                }
                if (!m.instanceId) {
                    m.instanceId = this.instanceId;
                }
                if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                    m.referenceId = this.meetingId;
                }
                this.availableAttendees.push(m);
            });
        });

        this._meetingService.meetingAgendaCollectionMessageReceived.subscribe((message: any) => {
            this.hasAgenda = false;
            if (this.meeting) {
               if (this.meeting.meetingAgendaCollection) {
                   if (this.meeting.meetingAgendaCollection.length > 0) {
                       this.atLeastOneAgenda = true;
                   }
               }
            }
        });

        this._meetingService.addMeetingAgendaMessage.subscribe((message: any) => {
            const q = this._meeting.meetingAgendaCollection.find( a => a.agendaHeading === message.data.agenda.agendaHeading);
            if (!q) {
                this._meeting.meetingAgendaCollection.push(message.data.agenda);
            }
            if (this._meeting.meetingAgendaCollection.length >= 1) { this.atLeastOneAgenda = true; }
        });
    }

    startMeeting(): void {
        this.meeting.status = 'run';
        this._meetingService.sendGroupMessage({
            task: 'setMode',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { meeting: this.meeting }
        }, this.meetingId);
        this._router.navigate(['./meeting'], {
            queryParams: {
                id: this.meetingId,
                instanceId: this.instanceId,
                'mode': 'run'
            }
        });
        this.isActiveStopMeeting = true;
    }

    stopMeeting(): void {
        this.isActivePublishMinutes = true;
        this.meeting.status = 'publish';
        this._meetingService.sendGroupMessage({
            task: 'setMode',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { meeting : this.meeting }
        }, this.meetingId);
        this._router.navigate(['./meeting'], {
            queryParams: {
                id: this.meetingId,
                instanceId: this.instanceId,
                'mode': 'publish'
            }
        });
        this.isActiveStopMeeting = true;
    }

    meetingInfo(event): void {
        // debugger
        const dialogPosition: DialogPosition = {
            top: event.y + 13 + 'px',
            left: event.x - 300 + 'px'
        };

        this.dialog.open(MeetingInfoComponent, {
            width: '350px',
            height: '400px',
            position: dialogPosition,
            data: { meeting: this.meeting },
        });
    }

    printPreview(): void {
        const url = `${environment.reportsApi}?meetingId=${this.meetingId}&instanceId=${this.instanceId}&mode=${this.meeting.status}`;
        const win = window.open(url, '_blank');
        win.focus();
    }

    InviteUsers(): void {
        if (this.twoORMoreAttendees) {
            const dialogConfig = new MatDialogConfig();

            dialogConfig.disableClose = true;
            dialogConfig.autoFocus = true;
            dialogConfig.data = {
                id: 2,
                title: 'Send Meeting Invitations',
                description: 'An invitation to attend the meeting will be sent to the attendees added to the meeting.',
                finput: 'Send',
                sinput: 'Send to New Attendees Only',
                successButton: 'SEND'
            };

            const dialogRef = this.dialog.open(SendInvitationComponent, dialogConfig);

            dialogRef.afterClosed().subscribe(
                data => this.toInput = data
            );
            let sent = 0;
            this.meetingAttendees.forEach( (item: any) => {
                const url = `${environment.notifyApi}`;
                this._meetingRepositoryService.notify(
                    this.user,
                    this.meetingId,
                    this.instanceId,
                    url,
                    item.name,
                    item.email,
                    'MeetingInvite').subscribe( (result: any) => {
                        sent += 1;
                        if (sent === this.meetingAttendees.length) {
                            this.router.navigate(['./meeting'],
                                { queryParams:
                                        {
                                            id: this.meetingId,
                                            instanceId: this.instanceId,
                                            mode: 'edit'
                                        } });
                        }
                    });
            });
        }
    }

    IssueAgenda(): void {
        const meetingId = this._urlService.getMeetingQueryString(window.location.href);

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
            id: 2,
            title: 'Issue Agenda',
            description: 'The agenda will be issued to the attendees added to the meeting',
            finput: 'Send to All',
            sinput: 'Send to New Attendees Only',
            successButton: 'ISSUE AGENDA'
        };

        const dialogRef = this.dialog.open(SendInvitationComponent, dialogConfig);

        dialogRef.afterClosed().subscribe(
            data => this.toInput = data
        );

        const inviteModel = new MeetingInviteModel({
            meetingId: meetingId,
            recipients: this.toInput
        });

        this._meetingService.sendInvitations(inviteModel, this.user).subscribe(
            result => {
                this._meetingStatusService.update(this._user, meetingId, 'edit').subscribe(
                    resultObject => {
                        this.router.navigate(['./meeting'], { queryParams: { id: meetingId, mode: 'edit' } });
                        this._breadcrumbService.LastUpdated();
                    });
            });
    }

    publishMinutes(): void {
        this.meeting.status = 'minute';
        this._meetingService.sendGroupMessage({
            task: 'setMode',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { meeting : this.meeting }
        }, this.meetingId);
        this._router.navigate(['./dashboard'], {
            queryParams: {
            }
        });
        this.isActiveStopMeeting = true;
        let sent = 0;
        this.meetingAttendees.forEach( (item: any) => {
            const url = `${environment.notifyApi}`;
            this._meetingRepositoryService.notify(
                this.user,
                this.meetingId,
                this.instanceId,
                url,
                item.name,
                item.email,
                'MeetingMinutes').subscribe( (result: any) => {
                sent += 1;
                if (sent === this.meetingAttendees.length) {
                    this._router.navigate(['./dashboard']);
                }
            });
        });
    }
}
