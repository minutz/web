import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';


@Component({
    selector: 'app-send-invitation',
    templateUrl: './send-invitation.component.html',
    styleUrls: ['./send-invitation.component.scss']
})
export class SendInvitationComponent {

    form: FormGroup;
    title: string;
    description: string;
    finput: string;
    sinput: string;
    successButton: string;
    toInput: string;

    constructor(private fb: FormBuilder,
        private dialogRef: MatDialogRef<SendInvitationComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.title = data.title;
        this.description = data.description;
        this.finput = data.finput;
        this.sinput = data.sinput;
        this.successButton = data.successButton;
    }

    close() {
        this.dialogRef.close();
    }

    invite() {
        this.dialogRef.close(this.toInput);
    }
}
