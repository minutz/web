import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl, FormGroup} from '@angular/forms';
import {BreadcrumbService, MeetingService, ProfileService, UrlService} from 'src/app/shared/services';
import {MAT_DATE_FORMATS} from '@angular/material';
import {UserProfile, MeetingAgenda, MeetingModel} from 'src/app/shared/models';

export const APP_DATE_FORMATS = {
    parse: {
        dateInput: {
            month: 'short',
            year: 'numeric',
            day: 'numeric'
        },
    },
    display: {
        dateInput: {
            month: 'short',
            year: 'numeric',
            day: 'numeric'
        },
        monthYearLabel: {
            year: 'numeric'
        }
    }
};

@Component({
    selector: 'app-meeting-info',
    templateUrl: './meeting-info.component.html',
    styleUrls: ['./meeting-info.component.scss'],
    providers: [{provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}]
})
export class MeetingInfoComponent implements OnInit {
    meetingDatePicker = new FormControl(Date());
    form: FormGroup;
    title: string;
    description: string;
    mode: string;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    location: string;

    private _user: UserProfile;

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _selectedAgenda: MeetingAgenda;
    public get selectedAgenda(): MeetingAgenda {
        return this._selectedAgenda;
    }

    public set selectedAgenda(v: MeetingAgenda) {
        this._selectedAgenda = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(
        private _meetingService: MeetingService,
        private _urlService: UrlService,
        private _profileService: ProfileService,
        private dialogRef: MatDialogRef<MeetingInfoComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        this.selectedAgenda = new MeetingAgenda();
        this.location = '';
    }

    ngOnInit() {
        this.meeting = this.data.meeting;
        this.meetingDatePicker.setValue(this.meeting.date);
        if (this.meeting.time === '0:00') {
            this.meeting.time = new Date().getHours() + ':' + this.getFullMinutes();
        } else if (this._meeting.duration === 0) {
            this.meeting.duration = 60;
        }
    }

    close() {
        this.dialogRef.close();
    }
    getFullMinutes() {
        if (new Date().getMinutes() < 10) {
            return '0' + new Date().getMinutes();
        }
        return new Date().getMinutes();
    }

}
