import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
    selector: 'app-print-preview',
    templateUrl: './print-preview.component.html',
    styleUrls: ['./print-preview.component.scss']
})
export class PrintPreviewComponent {

    form: FormGroup;
    title: string;
    description: string;

    constructor(private fb: FormBuilder,
        private dialogRef: MatDialogRef<PrintPreviewComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.title = data.title;
        this.description = data.description;
    }

    close() {
        this.dialogRef.close();
    }
}
