import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarCompanyInformationComponent } from './side-bar-company-information.component';

describe('SideBarCompanyInformationComponent', () => {
  let component: SideBarCompanyInformationComponent;
  let fixture: ComponentFixture<SideBarCompanyInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarCompanyInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarCompanyInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
