import { Component, OnInit } from '@angular/core';
import { SideMenuService } from '../../../services/side-menu.service';
import { UserProfile } from '../../../models';
import { ProfileService } from '../../../services/profile.service';
import {AuthenticationService} from '../../../services/authentication.service';

@Component({
  selector: 'app-side-bar-company-information',
  templateUrl: './side-bar-company-information.component.html',
  styleUrls: ['./side-bar-company-information.component.scss']
})
export class SideBarCompanyInformationComponent implements OnInit {
  companyImage: string;
  hide: boolean;
  sideElement: any;

  private _isAuthenticated: boolean;
  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  public set isAuthenticated(v: boolean) {
    this._isAuthenticated = v;
  }

  private _user: UserProfile;
  public get user(): UserProfile {
    return this._user;
  }
  public set user(v: UserProfile) {
    this._user = v;
  }

  constructor(
    private _sideMenuService: SideMenuService,
    private _profileService: ProfileService,
    public _authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this._sideMenuService._SideBar$.subscribe( domElement => {
      this.hide = domElement.classList.contains('collapse');
      const companyDomElement: any = document.getElementById('companyInfo');
      const companyNameDomElement: any = document.getElementById('companyName');
      if (this.hide) {
        companyDomElement.classList.add('collapse');
        companyNameDomElement.classList.add('transform');
      } else {
        companyDomElement.classList.remove('collapse');
        companyNameDomElement.classList.remove('transform');
      }
    });
    this._profileService._Profile$.subscribe( profile => {
      this.user = profile;
      this.isAuthenticated = this._authenticationService.isAuthenticated();
    });
    if (!this.user) {
      const profile = this._profileService.get();
      this._profileService.Profile(profile);
    }
  }

}
