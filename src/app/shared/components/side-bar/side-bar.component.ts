import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { ProfileService } from '../../services/profile.service';
import { UserProfile } from '../../models';
declare let $: any;

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  isActive = true;
  isCollapse: boolean;
  showMenu = '';

  private _isAuthenticated: boolean;
  public get isAuthenticated(): boolean {
    return this._isAuthenticated;
  }
  public set isAuthenticated(v: boolean) {
    this._isAuthenticated = v;
  }

  private _user: UserProfile;
  public get user(): UserProfile {
    return this._user;
  }
  public set user(v: UserProfile) {
    this._user = v;
  }

  constructor(
    public authService: AuthenticationService,
    private _profileService: ProfileService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.user = this._profileService.get();
  }
  eventCalled() {
    this.isActive = !this.isActive;
  }

}
