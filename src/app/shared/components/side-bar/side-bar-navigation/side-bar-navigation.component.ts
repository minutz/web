import { Component, OnInit } from '@angular/core';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { Router } from '@angular/router';
import { UserProfile } from 'src/app/shared/models';
import { SideMenuService } from 'src/app/shared/services/side-menu.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { LoadingService, UrlService } from 'src/app/shared/services';
import { UserCreateMeetingService } from 'src/app/shared/repositories/features/dashboard';

@Component({
    selector: 'app-side-bar-navigation',
    templateUrl: './side-bar-navigation.component.html',
    styleUrls: ['./side-bar-navigation.component.scss']
})
export class SideBarNavigationComponent implements OnInit {
    hide: boolean;
    sideElement: any;
    dom: any;
    mode: string;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;


    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    constructor(
        private _userCreateMeetingService: UserCreateMeetingService,
        private _meetingService: MeetingService,
        private _sideMenuService: SideMenuService,
        private _notificationService: NotificationsService,
        private _profileService: ProfileService,
        private router: Router,
        private _loading: LoadingService,
        private _urlService: UrlService
    ) {
        this.hide = true;
    }

    ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._user = this._profileService.get();
        this._sideMenuService._SideBar$.subscribe(domElement => {
            this.hide = domElement.classList.contains('collapse');
        });
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            const meeting = message.data.meeting;
            this.router.navigate(['/meeting'],
                {
                    queryParams:
                        {
                            id: message.meetingId,
                            instanceId: message.instanceId,
                            'mode': meeting.status
                        }
                });
        });
    }
    getMeetings() {
        if (this.meetingId) {
            this._meetingService.leaveGroup(this.meetingId);
        }

        this._meetingService.sendGroupMessage({
            task: 'getmeetings',
            instanceId: this.user.instanceId,
            meetingId: '',
            data: 'get data'
          }, this.user.instanceId);
          this.router.navigate(['/dashboard']);
    }

    createMeeting() {
        this._loading.loading(true);

        this._userCreateMeetingService.create(this.user)
            .subscribe(meeting => {
                meeting.name = '';
                const meetingId = meeting.id;
                const instanceId = meeting.instanceId;
                this._meetingService.Meeting(meeting, this.user);
                this._loading.loading(false);
                this._meetingService.joinGroup(meetingId);
                this._meetingService.sendGroupMessage({
                    task: 'getmeeting',
                    instanceId: instanceId,
                    meetingId: meetingId,
                    data: 'get data'
                }, meetingId);
        });
    }

    admin() {
        let instanceId = this.instanceId;
        if (!instanceId) {
            const profile = JSON.parse(sessionStorage.getItem('minutz'));
            console.log(profile);
            instanceId = profile.sub;;
        }
        console.log(this.user.instanceId);
        console.log(this.user);
        this.router.navigate(['/admin'],
            {
                queryParams:
                    {
                        instanceId: instanceId
                    }
            });
    }
}
