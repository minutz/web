import { Component, OnInit } from '@angular/core';
import { Notification } from '../../models';
import { NotificationsService } from '../../services/notifications.service';

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.scss']
})
export class UserNotificationsComponent implements OnInit {
  public notes: Notification[];

  constructor(
    private notifications: NotificationsService
  ) {
    this.notes = new Array<Notification>();

    notifications.noteAdded.subscribe((note: Notification) => {

      note.displayClass = this.mapClassToType(note.type);
      this.notes.push(note);

      setTimeout(() => { this.hide.bind(this)(note); }, 5000);
    });
  }
  ngOnInit() {
  }

  hide(note: Notification) {
    const index = this.notes.indexOf(note);

    if (index >= 0) {
      this.notes.splice(index, 1);
    }
  }

  mapClassToType(type: string) {
    let displayClass = '';
    switch (type) {
      case 'Error':
        displayClass = 'toast-error';
        break;
      case 'Info':
        displayClass = 'toast-info';
        break;
      case 'Warning':
        displayClass = 'toast-warning';
        break;
      case 'Success':
        displayClass = 'toast-success';
        break;
    }
    return displayClass;
  }


}
