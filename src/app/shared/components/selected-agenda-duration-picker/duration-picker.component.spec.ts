import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedAgendaDurationPickerComponent } from './selected-agenda-duration-picker.component';

describe('SelectedAgendaDurationPickerComponent', () => {
  let component: SelectedAgendaDurationPickerComponent;
  let fixture: ComponentFixture<SelectedAgendaDurationPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectedAgendaDurationPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedAgendaDurationPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
