import {
    OnInit,
    Component
} from '@angular/core';
import {
    MeetingAgenda,
    MeetingModel,
    UserProfile
} from '../../models';
import { MeetingService, UrlService, ProfileService } from '../../services';
import { MeetingAgendaService } from 'src/app/shared/repositories/features/meeting/agenda/meeting-agenda.service';

@Component({
    selector: 'app-selected-agenda-duration',
    templateUrl: './selected-agenda-duration-picker.component.html',
    styleUrls: ['./selected-agenda-duration-picker.component.scss']
})
export class SelectedAgendaDurationPickerComponent implements OnInit {
    private _user: UserProfile;
    duration: number;
    canEdit: boolean;
    mode: string;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _meetingAgenda: MeetingAgenda;
    public get meetingAgenda(): MeetingAgenda {
        return this._meetingAgenda;
    }
    public set meetingAgenda(v: MeetingAgenda) {
        this._meetingAgenda = v;
    }

    private _meetingAgendaCollection: MeetingAgenda[];
    public get meetingAgendaCollection(): MeetingAgenda[] {
        return this._meetingAgendaCollection;
    }
    public set meetingAgendaCollection(v: MeetingAgenda[]) {
        this._meetingAgendaCollection = v;
    }
    constructor(
        private _meetingService: MeetingService,
        private _urlService: UrlService,
        private _profileService: ProfileService,
        private _agendaService: MeetingAgendaService) {
    }
    public ngOnInit() {
        this.duration = 60;
        this.user = this._profileService.get();
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._meetingService._CurrentAgendaCollection$.subscribe(agendaCollection => {
            this._meetingAgendaCollection = agendaCollection;
        });
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService._CurrentAgenda$.subscribe(agenda => {
            this._meetingAgenda = agenda;
        });
    }
    update(value: any): void {
        if (this._meetingAgenda) {
            this._meetingAgenda.duration = value;
            this._meetingAgenda.referenceId = this._urlService.getMeetingQueryString(window.location.href);
            this._meetingService.CurrentAgenda(this._meetingAgenda);
            this._meetingAgendaCollection.forEach(agenda => {
                if (agenda.id === this._meetingAgenda.id) {
                    agenda.duration = this._meetingAgenda.duration;
                }
            });
            this._agendaService.duration(this._user, this._meetingAgenda).subscribe(x => {
                this._meetingService.Meeting(this._meeting, this._user);
            });
        }
    }
}
