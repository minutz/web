import { Component, OnInit, EventEmitter } from '@angular/core';
import { HubConnectionBuilder, HttpClient } from '@aspnet/signalr';
import { environment } from 'src/environments/environment';
import { ProfileService } from '../../services';
import { UserProfile } from 'src/app/shared/models';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
    _user: UserProfile;

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    _hubConnection: any;
    _connectionIsEstablished = false;
    connectionEstablished = new EventEmitter<Boolean>();

  constructor(private _profileService: ProfileService) { }
    getAccessToken() {
        const token = `${this.user.idToken}`;
        return token;
    }

    ngOnInit() {
        this.user = this._profileService.get();
        this._hubConnection = new HubConnectionBuilder()
            .withUrl(`${environment.apiUri}/chatHub`, {
                accessTokenFactory: () => {
                    return `Bearer ${this.user.idToken}`;
                }
            })
            .build();
        this._hubConnection
            .start()
            .then(() => {
                console.log('connection started');
            }).catch(err => {
                console.log(err);
                console.log('error in connection');
            });
        this._hubConnection.chatHub.subscribe(m => {
            console.log(m);
         });
  }

    sendMessage() {
        this._hubConnection.invoke('SendMessage', 'name', 'message')
            .catch(err => {
            console.log(err);
            console.log('error sending');
        });
    }
}
