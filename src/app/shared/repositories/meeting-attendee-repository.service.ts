import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserRepositoryService } from './user-repository.service';
import { MeetingAttendee, UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingAttendeeRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getAllAttendees(user: UserProfile, referenceId: string, instanceId: string): Observable<MeetingAttendee[]> {
        return this._http.get(user, `/meeting/${referenceId}/attendees?instanceId=${instanceId}`);
    }

    public getAttendee(user: UserProfile, referenceId: string, id: number, instanceId: string): Observable<MeetingAttendee> {
        return this._http.get(user, `/meeting/${referenceId}/attendee/${id}?instanceId=${instanceId}`);
    }

    public updateMeetingAttendees(user: UserProfile): Observable<Array<MeetingAttendee>> {
        return this._http.post(user, ``, null);
    }

    public inviteAttendee(user: UserProfile, attendee: MeetingAttendee): Observable<MeetingAttendee> {
        return this._http.post(user, `/feature/attendee/invite`, attendee);
    }

    public deleteAttendee(user: UserProfile, referenceId: string, id: number): Observable<any> {
        return this._http.delete(user, `/meeting/${referenceId}/attendee/${id}`);
    }

    public updateAttendee(user: UserProfile, body: MeetingAttendee): Observable<MeetingAttendee> {
        return this._http.post(user, `/meeting/${body.referenceId}/attendee/${body.id}`, body);
    }

    public createAttendee(user: UserProfile, referenceId: string, body: MeetingAttendee): Observable<MeetingAttendee> {
        return this._http.put(user, `/meeting/${referenceId}/attendee`, body);
    }
}
