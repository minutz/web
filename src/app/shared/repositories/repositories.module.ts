import { NgModule } from '@angular/core';


import {
    AdminRepositoryService,
    MeetingActionRepositoryService,
    MeetingAgendaRepositoryService,
    MeetingAttachmentRepositoryService,
    MeetingAttendeeRepositoryService,
    MeetingDecisionRepositoryService,
    MeetingNoteRepositoryService,
    MeetingRepositoryService,
    SignUpRepositoryService,
    UserRepositoryService
} from '.';

@NgModule({
    providers: [
        AdminRepositoryService,
        MeetingActionRepositoryService,
        MeetingAgendaRepositoryService,
        MeetingAttachmentRepositoryService,
        MeetingAttendeeRepositoryService,
        MeetingDecisionRepositoryService,
        MeetingNoteRepositoryService,
        MeetingRepositoryService,
        SignUpRepositoryService,
        UserRepositoryService
    ],
})
export class RepositoriesModule { }
