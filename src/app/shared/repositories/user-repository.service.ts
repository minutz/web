import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UrlService } from '../services/url.service';
import { UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _urlService: UrlService
    ) { }

    public resetAccount(): Observable<UserProfile> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(null, `/reset`, null);
    }

    public GetUserProfile(): Observable<UserProfile> {
        const instanceId = sessionStorage.getItem('instanceId');
        const myParams: URLSearchParams = new URLSearchParams();
        const reference = this.getReference();
        return this._http.get(null, `/user?` + reference);
    }

    // invite|instance&meeting;
    public getReference(): string {
        const instanceId = sessionStorage.getItem('instanceId');
        const referenceCheck = this._urlService.getReference(window.location.href);
        if (referenceCheck) {
            const previous = localStorage.getItem('reference');
            if (previous) {
                if (referenceCheck === previous) {
                    return previous;
                }
                localStorage.setItem('reference', referenceCheck);
                return referenceCheck;
            }
            return referenceCheck;
        }
        if (localStorage.getItem('reference')) {
            return localStorage.getItem('reference');
        }
        return 'none';
    }
}
