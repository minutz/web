import { Injectable } from '@angular/core';
import { MeetingInviteModel, MeetingModel, UserProfile } from '../models';
import { Observable } from 'rxjs';
import { SessionStorageService } from '../services/session-storage.service';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';

@Injectable({
    providedIn: 'root'
})
export class MeetingRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService) { }

    public notify(user: UserProfile,
                  meetingId: string,
                  instanceId: string,
                  url: string,
                  name: string,
                  email: string,
                  type: string): Observable<MeetingModel> {
        const data = {
            meetingId : meetingId,
            instanceId : instanceId,
            name: name,
            email: email,
            type: type
        };
        return this._http.postFullUrl(user, url, data);
    }

    public getMeeting(user: UserProfile, id: string): Observable<MeetingModel> {
        const instanceId = sessionStorage.getItem('instanceId');
        const url = `/meeting?id=${id}&related=${user.instanceId}`;
        return this._http.get(user, url);
    }

    public createNewMeeting(user: UserProfile, body?: MeetingModel): Observable<MeetingModel> {
        const instanceId = sessionStorage.getItem('instanceId');
        if (!body) {
            body = null;
        }
        return this._http.put(user, `/meeting/create`, body);
    }

    public sendMeetingInvitations(user: UserProfile, model: MeetingInviteModel): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, '/feature/attendee/invitation', model);
    }

    public sendMeetingMinutes(user: UserProfile, meetingId: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/feature/attendee/minutes?meetingId=${meetingId}`, null);
    }

    public previewMinutes(user: UserProfile, id: string) {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/reports/demo?id=${id}`);
        // return this._http.get(user, `/meetingminutes/${id}/preview`);
    }
}
