import { Injectable } from '@angular/core';
import { UserRepositoryService } from './user-repository.service';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { MeetingNote, UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingNoteRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getMeetingNotes(user: UserProfile, referenceId: string): Observable<MeetingNote[]> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meeting/${referenceId}/note`);
    }

    public createNote(user: UserProfile, referenceId: string, body: MeetingNote): Observable<MeetingNote> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.put(user, `/meeting/${referenceId}/note`, body);
    }

    public deleteNote(user: UserProfile, referenceId: string, id: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.delete(user, `/meeting/${referenceId}/note/${id}`);
    }

    public getNote(user: UserProfile, referenceId: string, id: string): Observable<MeetingNote> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meeting/${referenceId}/note/${id}`);
    }

    public updateNote(user: UserProfile, body: MeetingNote): Observable<MeetingNote> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meeting/${body.referenceId}/note/${body.id}`, body);
    }
}
