import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SignUpRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService
    ) { }

    public signup(name: string, email: string, username: string, password: string,
                  RefInstanceId: string, refMeetingId: string, role: string): Observable<any> {
        const data = {
            name: name,
            email: email,
            username: username,
            password: password,
            refMeetingId: refMeetingId,
            RefInstanceId: RefInstanceId,
            role: role
        };
        return this._http.postWithUrl(null, environment.authUri, `/signup`, data);
    }
}
