import { TestBed, inject } from '@angular/core/testing';

import { SignUpRepositoryService } from './sign-up-repository.service';

describe('SignUpRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignUpRepositoryService]
    });
  });

  it('should be created', inject([SignUpRepositoryService], (service: SignUpRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
