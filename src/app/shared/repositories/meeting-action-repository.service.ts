import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserRepositoryService } from './user-repository.service';
import { MeetingAction, UserProfile } from '../models';

@Injectable({
    providedIn: 'root'
})
export class MeetingActionRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getMeetingActions(user: UserProfile, referenceId: string): Observable<MeetingAction[]> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meetingActions/${referenceId}`);
    }

    public createMeetingAction(user: UserProfile, meetingAction: MeetingAction): Observable<MeetingAction> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.put(user, `/meetingAction/${meetingAction.referenceId}/action`, meetingAction);
    }

    public deleteMeetingAction(user: UserProfile, referenceId: string, id: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.delete(user, `/meetingAction/${referenceId}/action/${id}`);
    }

    public getMeetingAction(user: UserProfile, referenceId: string, id?: string): Observable<MeetingAction> {
        const instanceId = sessionStorage.getItem('instanceId');
        const searchParams = new URLSearchParams();
        if (id) {
            searchParams.append('id', id);
        }
        return this._http.get(user, `/meetingAction/${referenceId}/actions`);
    }

    public updateMeetingAction(user: UserProfile, meetingAction: MeetingAction): Observable<MeetingAction> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meetingAction/${meetingAction.referenceId}/action/${meetingAction.id}`, meetingAction);
    }
}
