import { TestBed, inject } from '@angular/core/testing';

import { MeetingAgendaRepositoryService } from './meeting-agenda-repository.service';

describe('MeetingAgendaRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAgendaRepositoryService]
    });
  });

  it('should be created', inject([MeetingAgendaRepositoryService], (service: MeetingAgendaRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
