import { TestBed, inject } from '@angular/core/testing';

import { AdminRepositoryService } from './admin-repository.service';

describe('AdminRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdminRepositoryService]
    });
  });

  it('should be created', inject([AdminRepositoryService], (service: AdminRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
