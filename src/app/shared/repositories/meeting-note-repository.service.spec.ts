import { TestBed, inject } from '@angular/core/testing';

import { MeetingNoteRepositoryService } from './meeting-note-repository.service';

describe('MeetingNoteRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingNoteRepositoryService]
    });
  });

  it('should be created', inject([MeetingNoteRepositoryService], (service: MeetingNoteRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
