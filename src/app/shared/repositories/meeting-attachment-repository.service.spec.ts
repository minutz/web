import { TestBed, inject } from '@angular/core/testing';

import { MeetingAttachmentRepositoryService } from './meeting-attachment-repository.service';

describe('MeetingAttachmentRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAttachmentRepositoryService]
    });
  });

  it('should be created', inject([MeetingAttachmentRepositoryService], (service: MeetingAttachmentRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
