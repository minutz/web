import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../../../../services/auth-http-request-service.service';
import { UserProfile, MeetingModel } from '../../../../models';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserCreateMeetingService {

    constructor(private _http: AuthHttpRequestServiceService) { }

    public create(user: UserProfile): Observable<MeetingModel> {
        return this._http.put(user, `/feature/dashboard/createusermeeting`, null);
    }
}
