import { TestBed, inject } from '@angular/core/testing';

import { UserMeetingsService } from './user-meetings.service';

describe('UserMeetingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserMeetingsService]
    });
  });

  it('should be created', inject([UserMeetingsService], (service: UserMeetingsService) => {
    expect(service).toBeTruthy();
  }));
});
