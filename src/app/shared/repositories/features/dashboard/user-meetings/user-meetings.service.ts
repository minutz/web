import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';
import {UserProfile, MeetingModel} from '../../../../models';
import {Observable} from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserMeetingsService {
  meeting: any;

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public get(user: UserProfile): Observable<any> {
        return this._http.get(user, `/feature/dashboard/usermeetings`);
    }

    public getHeaders(user: UserProfile): any {
        if (user) {
          const token = `Bearer ${user.idToken}`;
          const access_token = user.accessToken;
          const idToken = user.idToken;
          const ex = user.tokenExpire;
          const jwtHeaders = new HttpHeaders()
            .set('xAuthHeader' , 'xAuthMinutz')
            .set('Content-Type', 'application/json')
            .set('Authorization',  token)
            .set('xAuthHeader', 'xAuthMinutz')
            .set('accessToken' , access_token)
            .set('idToken', idToken)
            .set('xExp' , ex);
          return jwtHeaders;
        } else {
          const headers = new HttpHeaders()
            .set('content-type', 'application/json')
            .set('xAuthHeader', 'xAuthMinutz');
          return headers;
        }
      }

    public getMeeting(user: UserProfile, meetingId: string, instanceId: string): Observable<MeetingModel> {
      // const headers = this.getHeaders(user);
      // const token = `Bearer ${user.idToken}`;
      // const access_token = user.accessToken;
      // const idToken = user.idToken;
      // const ex = user.tokenExpire;
      // this.meeting = JSON.parse(sessionStorage.getItem('m'));
      // const instance = this.meeting.instanceId;
      return this._http.get(user, `/feature/managemeeting/user?meetingId=${meetingId}&instanceId=${instanceId}`);
    }

    public updateMeeting(user: UserProfile, meeting: MeetingModel): Observable<MeetingModel> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/feature/managemeeting/user`, meeting);
    }
}
