import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';
import {UserProfile} from '../../../../models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserActionsService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public get(user: UserProfile): Observable<any> {
        return this._http.get(user, `/feature/dashboard/useractions`);
    }
}
