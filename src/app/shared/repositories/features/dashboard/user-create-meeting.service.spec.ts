/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { UserCreateMeetingService } from './user-create-meeting/user-create-meeting.service';

describe('Service: UserCreateMeeting', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserCreateMeetingService]
    });
  });

  it('should ...', inject([UserCreateMeetingService], (service: UserCreateMeetingService) => {
    expect(service).toBeTruthy();
  }));
});
