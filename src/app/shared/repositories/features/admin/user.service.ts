import { Injectable } from '@angular/core';
import {
    AuthHttpRequestServiceService
} from 'src/app/shared/services/auth-http-request-service.service';
import { UserProfile } from 'src/app/shared/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public users(user: UserProfile): Observable<UserProfile[]> {
        return this._http.get(user, `/feature/admin/users`);
    }

    public user(user: UserProfile, person: any): Observable<UserProfile> {
        const model = {
            firstName: person.firstName,
            lastName: person.lastName,
            name: `${person.firstName} ${person.lastName}`,
            role: person.role,
            email: person.email
        };
        return this._http.put(user, `/feature/admin/user`, model);
    }
}
