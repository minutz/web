import { Injectable } from '@angular/core';
import {
    MeetingModel,
    MeetingAgenda,
    UserProfile,
    QuickAgenda} from 'src/app/shared/models';
import {Observable} from 'rxjs';
import {
    AuthHttpRequestServiceService
} from 'src/app/shared/services/auth-http-request-service.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingAgendaService {
    meeting: any;

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public getCollection(
        user: UserProfile,
        meetingId: string, instanceId: string): Observable<MeetingAgenda[]> {
            return this._http.get(
                user,
                `/feature/agenda/collection?refId=${meetingId}&instanceId=${instanceId}`);
    }

    public complete(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/complete`,
                agenda);
    }

    public order(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/order`,
                agenda);
    }

    public duration(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/duration`,
                agenda);
    }

    public title(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/title`,
                agenda);
    }

    public text(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/text`,
                agenda);
    }

    public attendee(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.post(
                user,
                `/feature/agenda/attendee`,
                agenda);
    }

    public quick(
        user: UserProfile,
        agenda: MeetingAgenda): Observable<MeetingAgenda> {
            return this._http.put(
                user,
                `/feature/agenda/quick`,
                agenda);
    }

    public delete(
        user: UserProfile,
        referenceId: string,
        meetingId: string,
        instanceId: string): Observable<MeetingAgenda> {
        return this._http.delete(
            user,
            `/feature/agendaid=${referenceId}&meetingId=${meetingId}&instanceId=${instanceId}`);
    }
}
