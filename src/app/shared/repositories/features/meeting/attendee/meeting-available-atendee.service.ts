import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services';
import {MeetingAttendee, UserProfile} from '../../../../models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingAvailableAttendeeService {

  constructor(private _http: AuthHttpRequestServiceService) { }

    public get(
      user: UserProfile,
      instanceId: string): Observable<MeetingAttendee[]> {
        return this._http.get(
          user,
          `/feature/availability/attendees?instanceId=${instanceId}`);
    }
}
