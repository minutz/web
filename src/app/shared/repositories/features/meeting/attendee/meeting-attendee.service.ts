import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services';
import {
    MeetingAttendee,
    UserProfile
} from '../../../../models';

import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingAttendeeService {
    meeting: any;

  constructor(private _http: AuthHttpRequestServiceService) { }

    public getCollection(user: UserProfile,
                         meetingId: string,
                         instanceId: string): Observable<MeetingAttendee[]> {
        return this._http.get(user, `/feature/attendee/attendees?meetingId=${meetingId}&instanceId=${instanceId}`);
    }

    public add(user: UserProfile,
               attendee: MeetingAttendee,
               instanceId: string): Observable<MeetingAttendee[]> {
        attendee.instanceId = instanceId;
        return this._http.post(user, `/feature/attendee/add`, attendee);
    }

    public update(user: UserProfile,
                  attendee: MeetingAttendee,
                  instanceId: string): Observable<MeetingAttendee[]> {
        attendee.instanceId = instanceId;
        return this._http.post(user, `/feature/attendee/update`, attendee);
    }

    public delete(user: UserProfile,
                  meetingId: string,
                  attendeeEmail: string,
                  instanceId: string): Observable<MeetingAttendee[]> {
        return this._http.delete(
            user, `/feature/attendee/update?meetingId=${meetingId}&attendeeEmail=${attendeeEmail}&instanceId=${instanceId}`);
    }
}
