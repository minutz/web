import { TestBed, inject } from '@angular/core/testing';

import { MeetingAvailableAttendeeService } from './meeting-available-atendee.service';

describe('MeetingAvailableAtendeeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAvailableAttendeeService]
    });
  });

  it('should be created', inject([MeetingAvailableAttendeeService], (service: MeetingAvailableAttendeeService) => {
    expect(service).toBeTruthy();
  }));
});
