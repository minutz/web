import { TestBed, inject } from '@angular/core/testing';

import { MeetingAttendeeService } from './meeting-attendee.service';

describe('MeetingAttendeeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAttendeeService]
    });
  });

  it('should be created', inject([MeetingAttendeeService], (service: MeetingAttendeeService) => {
    expect(service).toBeTruthy();
  }));
});
