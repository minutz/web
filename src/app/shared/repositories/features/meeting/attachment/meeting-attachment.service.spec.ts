/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MeetingAttachmentService } from './meeting-attachment.service';

describe('Service: MeetingAttachment', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAttachmentService]
    });
  });

  it('should ...', inject([MeetingAttachmentService], (service: MeetingAttachmentService) => {
    expect(service).toBeTruthy();
  }));
});
