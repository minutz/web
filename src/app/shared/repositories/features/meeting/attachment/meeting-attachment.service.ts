import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from 'src/app/shared/services';
import { UserProfile, MeetingAttachment } from 'src/app/shared/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingAttachmentService {

    meeting: any;

    constructor(private _http: AuthHttpRequestServiceService) { }

    public getCollection(user: UserProfile, meetingId: string, instanceId: string): Observable<MeetingAttachment[]> {
        return this._http.getAttachment(
            user,
            `/feature/attachments?meetingId=${meetingId}&instanceId=${instanceId}`);
    }

    public add(user: UserProfile, attachment: MeetingAttachment): Observable<MeetingAttachment> {
        const instanceId = sessionStorage.getItem('instanceId');
        attachment.instanceId = instanceId;
        return this._http.post(user, `/feature/attachment/add`, attachment);
    }

    public update(user: UserProfile, attachment: MeetingAttachment): Observable<MeetingAttachment> {
        const instanceId = sessionStorage.getItem('instanceId');
        attachment.instanceId = instanceId;
        return this._http.post(user, `/feature/attachment/update`, attachment);
    }

    public upload(user: UserProfile, meetingId: string, instanceId: string, order: number, formData: FormData): Observable<any> {
        return this._http.postFile(
            user, `/feature/attachment/uploadFile?meetingId=${meetingId}&instanceId=${instanceId}&order=${order}`,
            formData);
    }

    public delete(user: UserProfile, meetingId: string, instanceId: string, fileName: string): Observable<any> {
        return this._http.deleteAttachment(
            user, `/feature/attachment?meetingId=${meetingId}&instanceId=${instanceId}&fileName=${fileName}`);
    }
}
