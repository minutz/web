import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from 'src/app/shared/services';
import {
    UserProfile,
    MeetingDecision
} from 'src/app/shared/models';
import { Observable } from 'node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public getCollection(user: UserProfile, meetingId: string): Observable<MeetingDecision[]> {
        return this._http.get(user, `/feature/decisions?meetingId=${meetingId}`);
    }

    public quick(
        user: UserProfile,
        meetingId: string,
        decisionText: string,
        order: number,
        instanceId: string): Observable<MeetingDecision> {
        const model = {
            meetingId: meetingId,
            decisionText: decisionText,
            order: order,
            instanceId: instanceId
        };
        return this._http.put(
            user,
            `/feature/decision/quick`,
            model);
    }

    public update(user: UserProfile, decision: MeetingDecision): Observable<MeetingDecision> {
        return this._http.post(
            user,
            `/feature/decision/update`,
            decision);
    }

    public delete(
        user: UserProfile,
        id: string,
        meetingId: string,
        instanceId: string): Observable<MeetingDecision> {
        return this._http.delete(
            user,
            `/feature/decision?decisionId=${id}&meetingId=${meetingId}&instanceId=${instanceId}`);
    }
}
