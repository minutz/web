import { TestBed, inject } from '@angular/core/testing';

import { MeetingObjectivePurposeService } from './meeting-objective-purpose.service';

describe('MeetingObjectivePurposeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingObjectivePurposeService]
    });
  });

  it('should be created', inject([MeetingObjectivePurposeService], (service: MeetingObjectivePurposeService) => {
    expect(service).toBeTruthy();
  }));
});
