import {
    Injectable
} from '@angular/core';
import {
    Observable
} from 'rxjs';
import {
    MeetingModel,
    UserProfile,
    PurposeUpdateRequest,
    ObjectiveUpdateRequest
} from '../../../../models';
import {
    AuthHttpRequestServiceService
} from '../../../../services/auth-http-request-service.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingObjectivePurposeService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public updatePurpose(user: UserProfile, meeting: MeetingModel): Observable<MeetingModel> {
        const data = { value: meeting.purpose, id: meeting.id, instanceId: meeting.instanceId };
        return this._http.post(user, `/feature/purpose/update`, data);
    }

    public updateObjective(user: UserProfile, meeting: MeetingModel): Observable<MeetingModel> {
        const data = { value: meeting.outcome, id: meeting.id, instanceId: meeting.instanceId };
        return this._http.post(user, `/feature/objective/update`, data);
    }
}

