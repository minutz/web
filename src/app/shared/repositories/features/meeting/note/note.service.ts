import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from 'src/app/shared/services';
import {
    UserProfile,
    MeetingNote
} from 'src/app/shared/models';
import { Observable } from 'node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public getCollection(
        user: UserProfile,
        meetingId: string,
        instanceId: string): Observable<MeetingNote[]> {
        return this._http.get(user, `/feature/notes?meetingId=${meetingId}&instanceId=${instanceId}`);
    }

    public quick(
        user: UserProfile,
        meetingId: string,
        noteText: string,
        order: number,
        instanceId: string): Observable<MeetingNote> {
        const model = {
            meetingId: meetingId,
            noteText: noteText,
            order: order,
            instanceId: instanceId
        };
        return this._http.put(user, `/feature/note/quick`, model);
    }

    public update(
        user: UserProfile,
        note: MeetingNote): Observable<MeetingNote> {
        return this._http.post(user, `/feature/note`, note);
    }

    public delete(
        user: UserProfile,
        id: string,
        meetingId: string,
        instanceId: string): Observable<MeetingNote> {
        return this._http.delete(user, `/feature/note?noteId=${id}&meetingId=${meetingId}&instanceId=${instanceId}`);
    }
}
