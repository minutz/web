import { Injectable } from '@angular/core';
import {MeetingModel, UserProfile} from '../../../../models';
import {Observable} from 'rxjs';
import {SessionStorageService} from '../../../../services/session-storage.service';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingTimeService {

    constructor(private _http: AuthHttpRequestServiceService,
                private _sessionStorageService: SessionStorageService) { }

    public update(user: UserProfile, meeting: MeetingModel): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/feature/header/time?id=${meeting.id}&time=${meeting.time}`, null);
    }
}
