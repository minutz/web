import { TestBed, inject } from '@angular/core/testing';

import { MeetingLocationService } from './meeting-location.service';

describe('MeetingLocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingLocationService]
    });
  });

  it('should be created', inject([MeetingLocationService], (service: MeetingLocationService) => {
    expect(service).toBeTruthy();
  }));
});
