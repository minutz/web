import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';
import {SessionStorageService} from '../../../../services/session-storage.service';
import {MeetingModel, UserProfile} from '../../../../models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingRecurrenceService {

    constructor(private _http: AuthHttpRequestServiceService,
                private _sessionStorageService: SessionStorageService) { }

    public update(user: UserProfile, meeting: MeetingModel): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/feature/header/recurrence?id=${meeting.id}&recurrence=${meeting.recurrenceType}`, null);
    }
}
