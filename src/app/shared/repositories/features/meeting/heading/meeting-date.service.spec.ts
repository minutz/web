import { TestBed, inject } from '@angular/core/testing';

import { MeetingDateService } from './meeting-date.service';

describe('MeetingDateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingDateService]
    });
  });

  it('should be created', inject([MeetingDateService], (service: MeetingDateService) => {
    expect(service).toBeTruthy();
  }));
});
