import { Injectable } from '@angular/core';
import { MeetingModel, UserProfile } from '../../../../models';
import { Observable } from 'rxjs';
import { AuthHttpRequestServiceService } from '../../../../services/auth-http-request-service.service';

@Injectable({
    providedIn: 'root'
})

export class MeetingDateService {

    constructor(private _http: AuthHttpRequestServiceService) { }

    public update(user: UserProfile, meeting: MeetingModel): Observable<any> {
        const date = meeting.date.toUTCString();
        const model = { id: meeting.id, meetingDate: meeting.date, instanceId: meeting.instanceId };
        return this._http.post(user, `/feature/header/date`, model);
    }
}
