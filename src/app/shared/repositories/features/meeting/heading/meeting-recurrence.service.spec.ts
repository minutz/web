import { TestBed, inject } from '@angular/core/testing';

import { MeetingRecurrenceService } from './meeting-recurrence.service';

describe('MeetingRecurrenceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingRecurrenceService]
    });
  });

  it('should be created', inject([MeetingRecurrenceService], (service: MeetingRecurrenceService) => {
    expect(service).toBeTruthy();
  }));
});
