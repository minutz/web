import { TestBed, inject } from '@angular/core/testing';

import { MeetingDurationService } from './meeting-duration.service';

describe('MeetingDurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingDurationService]
    });
  });

  it('should be created', inject([MeetingDurationService], (service: MeetingDurationService) => {
    expect(service).toBeTruthy();
  }));
});
