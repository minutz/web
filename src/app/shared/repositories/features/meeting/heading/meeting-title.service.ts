import { Injectable } from '@angular/core';
import {SessionStorageService} from '../../../../services/session-storage.service';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';
import {MeetingModel, UserProfile} from '../../../../models';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingTitleService {

  constructor(private _http: AuthHttpRequestServiceService,
              private _sessionStorageService: SessionStorageService) { }

    public update(
      user: UserProfile,
      meeting: MeetingModel,
      instanceId: string): Observable<any> {
        return this._http.post(
          user,
          `/feature/header/title?id=${meeting.id}&title=${meeting.name}&instanceId=${instanceId}`,
          null);
    }
}
