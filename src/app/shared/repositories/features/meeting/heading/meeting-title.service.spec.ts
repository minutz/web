import { TestBed, inject } from '@angular/core/testing';

import { MeetingTitleService } from './meeting-title.service';

describe('MeetingTitleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingTitleService]
    });
  });

  it('should be created', inject([MeetingTitleService], (service: MeetingTitleService) => {
    expect(service).toBeTruthy();
  }));
});
