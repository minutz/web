import { TestBed, inject } from '@angular/core/testing';

import { MeetingTagsService } from './meeting-tags.service';

describe('MeetingTagsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingTagsService]
    });
  });

  it('should be created', inject([MeetingTagsService], (service: MeetingTagsService) => {
    expect(service).toBeTruthy();
  }));
});
