import {Injectable} from '@angular/core';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';
import {MeetingModel, UserProfile} from '../../../../models';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingLocationService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public update(user: UserProfile, meeting: MeetingModel): Observable<any> {
        return this._http.post(
            user,
            `/feature/header/location?id=${meeting.id}&location=${meeting.location}&instanceId=${meeting.instanceId}`,
            null);
    }
}
