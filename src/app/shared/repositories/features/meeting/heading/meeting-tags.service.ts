import { Injectable } from '@angular/core';
import {MeetingModel, UserProfile} from '../../../../models';
import {Observable} from 'rxjs';
import {SessionStorageService} from '../../../../services/session-storage.service';
import {AuthHttpRequestServiceService} from '../../../../services/auth-http-request-service.service';

@Injectable({
  providedIn: 'root'
})
export class MeetingTagsService {

    constructor(private _http: AuthHttpRequestServiceService,
                private _sessionStorageService: SessionStorageService) { }

    public update(user: UserProfile, meeting: MeetingModel): Observable<any> {
        let tag = '';
        meeting.tag.forEach( t => {
            tag = `${tag},${t}`;
        });
        return this._http.post(user, `/feature/header/tag?id=${meeting.id}&tag=${tag}&instanceId=${meeting.instanceId}`, null);
    }
}
