import { Injectable } from '@angular/core';
import {
    MeetingModel,
    MeetingAction,
    UserProfile
} from 'src/app/shared/models';
import {
    AuthHttpRequestServiceService
} from 'src/app/shared/services/auth-http-request-service.service';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActionService {
    meeting: any;

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public getCollection(
        user: UserProfile,
        meetingId: string, instanceId: string): Observable<MeetingAction[]> {
        return this._http.get(
            user,
            `/feature/actions?meetingId=${meetingId}&instanceId=${instanceId}`);
    }

    public quick(
        user: UserProfile,
        meetingId: string,
        actionText: string,
        order: number,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            meetingId: meetingId,
            actionText: actionText,
            order: order,
            instanceId: instanceId
        };
        return this._http.put(user, `/feature/action/quick`, model);
    }

    public update(
        user: UserProfile,
        id: string,
        text: string,
        instanceId: string,
        meetingId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: text,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/update`, model);
    }

    public title(
        user: UserProfile,
        id: string,
        text: string,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: text,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/title`, model);
    }

    public complete(
        user: UserProfile,
        id: string,
        isComplete: boolean,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: isComplete,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/complete`, model);
    }

    public order(
        user: UserProfile,
        id: string,
        order: number,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: order,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/order`, model);
    }

    public assigned(
        user: UserProfile,
        id: string,
        personId: string,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: personId,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/assigned`, model);
    }

    public due(
        user: UserProfile,
        id: string,
        dueDate: Date,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: dueDate,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/due`, model);
    }

    public raised(
        user: UserProfile,
        id: string,
        raisedDate: Date,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        const model = {
            id: id,
            value: raisedDate,
            instanceId: instanceId,
            meetingId: meetingId
        };
        return this._http.post(user, `/feature/action/raised`, model);
    }

    public delete(
        user: UserProfile,
        actionId: string,
        meetingId: string,
        instanceId: string): Observable<MeetingAction> {
        return this._http.delete(
            user,
            `/feature/action?id=${actionId}&instanceId=${instanceId}&meetingId=${meetingId}`);
    }
}
