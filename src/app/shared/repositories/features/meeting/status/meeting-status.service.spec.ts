import { TestBed, inject } from '@angular/core/testing';

import { MeetingStatusService } from './meeting-status.service';

describe('MeetingStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingStatusService]
    });
  });

  it('should be created', inject([MeetingStatusService], (service: MeetingStatusService) => {
    expect(service).toBeTruthy();
  }));
});
