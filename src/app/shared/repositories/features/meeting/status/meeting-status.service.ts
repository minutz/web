import { Injectable } from '@angular/core';
import {AuthHttpRequestServiceService} from 'src/app/shared/services/auth-http-request-service.service';
import {MeetingModel, UserProfile} from 'src/app/shared/models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeetingStatusService {

    constructor(private _http: AuthHttpRequestServiceService) {
    }

    public update(user: UserProfile, id: string, status: string): Observable<any> {
        // const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/feature/status/update?id=${id}&status=${status}&instanceId=${user.instanceId}`, null);
    }


}
