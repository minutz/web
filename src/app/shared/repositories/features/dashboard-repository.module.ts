import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    UserMeetingsService,
    UserActionsService,
    UserCreateMeetingService
} from './dashboard';
@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
      UserMeetingsService,
      UserActionsService,
      UserCreateMeetingService
  ]
})
export class DashboardRepositoryModule { }
