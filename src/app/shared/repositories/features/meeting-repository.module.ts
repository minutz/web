import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MeetingTitleService,
    MeetingLocationService,
    MeetingDateService,
    MeetingDurationService,
    MeetingTimeService,
    MeetingTagsService,
    MeetingObjectivePurposeService,
    MeetingAttendeeService,
    MeetingAvailableAttendeeService,
    MeetingRecurrenceService,
    MeetingStatusService,
    MeetingAttachmentService,
    ActionService,
    NoteService,
    DecisionService
} from './meeting';

@NgModule({
  imports: [
    CommonModule
  ],
    providers: [
        MeetingTitleService,
        MeetingLocationService,
        MeetingDateService,
        MeetingDurationService,
        MeetingTimeService,
        MeetingTagsService,
        MeetingObjectivePurposeService,
        MeetingAttendeeService,
        MeetingAvailableAttendeeService,
        MeetingRecurrenceService,
        MeetingStatusService,
        MeetingAttachmentService,
        ActionService,
        NoteService,
        DecisionService
    ]
})
export class MeetingRepositoryModule { }
