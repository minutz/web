import { TestBed, inject } from '@angular/core/testing';

import { MeetingActionRepositoryService } from './meeting-action-repository.service';

describe('MeetingActionRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingActionRepositoryService]
    });
  });

  it('should be created', inject([MeetingActionRepositoryService], (service: MeetingActionRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
