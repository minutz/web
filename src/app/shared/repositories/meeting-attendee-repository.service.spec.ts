import { TestBed, inject } from '@angular/core/testing';

import { MeetingAttendeeRepositoryService } from './meeting-attendee-repository.service';

describe('MeetingAttendeeRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingAttendeeRepositoryService]
    });
  });

  it('should be created', inject([MeetingAttendeeRepositoryService], (service: MeetingAttendeeRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
