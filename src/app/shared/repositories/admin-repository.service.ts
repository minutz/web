import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { NewUser, UserRole, UserRolesUser } from '../models';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserProfile } from '../models/user-profile';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class AdminRepositoryService {

    constructor(private _http: AuthHttpRequestServiceService) { }

    public getUsers(user: UserProfile): Observable<UserProfile> {
        return this._http.get(user, '/getuserroles');
    }

    public getUserRolesInfo(user: UserProfile): Observable<UserRole> {
        return this._http.get(user, '/getuserroles');
    }

    public getUserActivity(user: UserProfile): Observable<Array<UserRolesUser>> {
        return this._http.get(user, '/getUserActivity');
    }

    public createNewUser(currentUser: UserProfile, newUser: NewUser): Observable<any> {
        return this._http.post(currentUser, '/createnewuser', newUser);
    }

    public saveUpdates(user: UserProfile): Observable<any> {
        return this._http.post(user, '/saveupdates', null);
    }

    public buyMoreLicences(user: UserProfile): Observable<any> {
        return this._http.post(user, '/buylicences', null);
    }

    public changePlan(user: UserProfile): Observable<any> {
        return this._http.post(user, '/changeplan', null);
    }

    public changeBillingAccess(user: UserProfile, object: Object): Observable<any> {
        return this._http.post(user, '/changebillingaccess', object);
    }

    public changePaymentMethod(user: UserProfile, cardNum: string): Observable<any> {
        return this._http.post(user, '/changepaymentmethod', cardNum);
    }

    public cancelSubscription(user: UserProfile): Observable<any> {
        return this._http.post(user, 'cancelsubscription', null);
    }
}
