import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserRepositoryService } from './user-repository.service';
import { MeetingAgenda, UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingAgendaRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getAllMeetingAgendaItems(user: UserProfile, referenceId: string): Observable<MeetingAgenda[]> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meetingAgendaItems/${referenceId}`);
    }

    public getMeetingAgenda(user: UserProfile, id: string): Observable<Array<MeetingAgenda>> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meetingAgenda/${id}`);
    }

    public deleteMeetingAgenda(user: UserProfile, id: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.delete(user, `/meetingAgenda/${id}`);
    }

    public updateAgendaWithId(user: UserProfile, body: MeetingAgenda): Observable<Array<MeetingAgenda>> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meetingAgenda/${body.id}`, body);
    }

    public updateAgenda(user: UserProfile, body: MeetingAgenda): Observable<MeetingAgenda> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meetingAgenda`, body);
    }

    public createAgenda(user: UserProfile, body: MeetingAgenda): Observable<MeetingAgenda> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.put(user, `/meetingAgenda`, body);
    }
}
