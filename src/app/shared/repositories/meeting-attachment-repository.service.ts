import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserRepositoryService } from './user-repository.service';
import { MeetingAttachment, UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingAttachmentRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getAttachments(user: UserProfile, referenceId: string): Observable<MeetingAttachment[]> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meeting/${referenceId}/attachments`);
    }

    public deleteAttachment(user: UserProfile, referenceId: string, id: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.delete(user, `/meeting/${referenceId}/attachment/${id}`);
    }

    public getAttachment(user: UserProfile, referenceId: string, id: string): Observable<MeetingAttachment> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meeting/${referenceId}/attachment/${id}`);
    }

    public updateAttachments(user: UserProfile, referenceId: string, id: string, body: any): Observable<MeetingAttachment> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meeting/${referenceId}/attachment/${id}`, body);
    }

    public createAttachment(user: UserProfile, referenceId: string, body: MeetingAttachment): Observable<MeetingAttachment> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.put(user, `/meeting/${referenceId}/attachment`, body);
    }
}
