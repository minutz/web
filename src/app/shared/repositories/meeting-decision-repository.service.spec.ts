import { TestBed, inject } from '@angular/core/testing';

import { MeetingDecisionRepositoryService } from './meeting-decision-repository.service';

describe('MeetingDecisionRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingDecisionRepositoryService]
    });
  });

  it('should be created', inject([MeetingDecisionRepositoryService], (service: MeetingDecisionRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
