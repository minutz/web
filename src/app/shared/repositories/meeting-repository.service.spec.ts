import { TestBed, inject } from '@angular/core/testing';

import { MeetingRepositoryService } from './meeting-repository.service';

describe('MeetingRepositoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MeetingRepositoryService]
    });
  });

  it('should be created', inject([MeetingRepositoryService], (service: MeetingRepositoryService) => {
    expect(service).toBeTruthy();
  }));
});
