import { Injectable } from '@angular/core';
import { AuthHttpRequestServiceService } from '../services/auth-http-request-service.service';
import { UserRepositoryService } from './user-repository.service';
import { MeetingDecision, UserProfile } from '../models';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MeetingDecisionRepositoryService {

    constructor(
        private _http: AuthHttpRequestServiceService,
        private _userRepository: UserRepositoryService
    ) { }

    public getMeetingDecisions(user: UserProfile, referenceId: string): Observable<MeetingDecision[]> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.get(user, `/meetingDecisions/${referenceId}`);
    }

    public createMeetingDecision(user: UserProfile, meetingDecision: MeetingDecision): Observable<MeetingDecision> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.put(user, `/meetingDecision/${meetingDecision.referenceId}/decision`, meetingDecision);
    }

    public deleteMeetingDecision(user: UserProfile, referenceId: string, id: string): Observable<any> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.delete(user, `/meetingDecision/${referenceId}/decision/${id}`);
    }

    public updateMeetingDecision(user: UserProfile, meetingDecision: MeetingDecision): Observable<MeetingDecision> {
        const instanceId = sessionStorage.getItem('instanceId');
        return this._http.post(user, `/meetingDecision/${meetingDecision.referenceId}/decision`, meetingDecision);
    }
}
