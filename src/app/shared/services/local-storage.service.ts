import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }
  public userPicture(): string {
    return localStorage.getItem('picture');
  }

  public userName(): string {
    return localStorage.getItem('name');
  }
  public get(key: string): any {
    return localStorage.getItem(key);
  }
  public check(key: string): boolean {
    if (localStorage.getItem(key)) {
      return true;
    }
    return false;
  }
  public set(key: string, value: any): void {
    const serialized = JSON.stringify(value);
    localStorage.setItem(key, serialized);
  }
  public saveSessionDetails(authResult: any, expiresAt: string) {
    localStorage.setItem('accessToken', authResult.accessToken);
    localStorage.setItem('idToken', authResult.idToken);
    localStorage.setItem('expiresAt', expiresAt);
    localStorage.setItem('name', authResult.idTokenPayload.name);
    localStorage.setItem('picture', authResult.idTokenPayload.picture);
  }

  public clearSessionDetails() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('idToken');
    localStorage.removeItem('expiresAt');
    localStorage.removeItem('name');
    localStorage.removeItem('picture');
    localStorage.removeItem('profile');
  }
}
