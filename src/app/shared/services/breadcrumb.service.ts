import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class BreadcrumbService {

    private _modeObject = new Subject<string>();
    _Mode$ = this._modeObject.asObservable();

    private _lastUpdatedObject = new Subject<string>();
    _LastUpdated$ = this._lastUpdatedObject.asObservable();

    constructor() { }

    public LastUpdated() {
        const today = new Date().toUTCString();
        const message = `${today}`;
        // const message = `Last Updated: ${today.getHours()}: ${today.getMinutes()}: ${today.getSeconds()}`;
        this._lastUpdatedObject.next(message);
    }

    public Mode(mode: string) {
        this._modeObject.next(mode);
    }
}
