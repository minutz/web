import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  public MeetingAgendaTemplateItemsCollection: Array<any> = [
    {
      id: 1,
      icon: 'fa fa-balance-scale',
      heading: 'Decide',
      text: 'Make a decision to move work forward',
      template: 'Make a decision to move work forward'
    },
    {
      id: 2,
      icon: 'fa fa-bandcamp',
      heading: 'Align',
      text: 'Get a shared understanding among teams' ,
      template: 'Get a shared understanding among teams'
    },
    {
      id: 3,
      icon: 'fa fa-lightbulb-o',
      heading: 'Idea  ',
      text: 'Brain storm ideas or potential solutions' ,
      template: 'Brain storm ideas or potential solutions'
    },
    {
      id: 4,
      icon: 'fa fa-space-shuttle',
      heading: 'Produce',
      text: 'Collaborate to accomplish a specific output',
      template: 'Collaborate to accomplish a specific output'
    },
    {
      id: 5,
      icon: 'fa fa-map-signs',
      heading: 'Plan',
      text: 'Determine the set of steps tp accomplish a result',
      template: 'Determine the set of steps tp accomplish a result'
    },
    {
      id: 6,
      icon: 'fa fa-ravelry',
      heading: 'Connect',
      text: 'Build relationships among team members',
      template: 'Build relationships among team members'
    }
  ];
  constructor() { }
}
