import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideMenuService {

  private _sidebarObject = new Subject<Element>();
  _SideBar$ = this._sidebarObject.asObservable();

  constructor() { }

  public SideBar(dom: Element): void {
    this._sidebarObject.next(dom);
  }
}
