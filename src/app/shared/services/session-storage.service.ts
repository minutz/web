import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {
  constructor() { }
  public applicationKey = 'minutz';
  public referenceKey = 'reference';
  public referenceRole = 'referenceRole';
  public refInstanceId = 'refInstanceId';
  public refMeetingId = 'refMeetingId';
  public minutzApp = 'minutz-app';
  public minutz = 'minutz';
  public expires_at = 'expiresAt';
  public access_token = 'accessToken';
  public id_token = 'idToken';

  public get(key: string): any {
    return sessionStorage.getItem(key);
  }

  public check(key: string): boolean {
    if (sessionStorage.getItem(key)) {
      return true;
    }
    return false;
  }

  public set(key: string, value: any): void {
    // const serialized = JSON.stringify(value);
    sessionStorage.setItem(key, value);
  }

  public kill(key: string): void {
    sessionStorage.removeItem(key);
  }
}
