import {EventEmitter, Injectable} from '@angular/core';
import {
    MeetingAction,
    MeetingAgenda,
    MeetingAttendee,
    MeetingDecision,
    MeetingInviteModel,
    MeetingModel,
    MeetingNote,
    MeetingRecurrence,
    RecurrenceType,
    UserProfile
} from '../models';
import {Observable, Subject} from 'rxjs';
import {MeetingRepositoryService} from '../repositories';
import {PanelEvent} from '../models/panel-event';
import {AuthHttpRequestServiceService} from './auth-http-request-service.service';
import {HubConnection, HubConnectionBuilder, LogLevel} from '@aspnet/signalr';
import {environment} from 'src/environments/environment';
import {ProfileService} from './profile.service';

@Injectable({
    providedIn: 'root'
})
export class MeetingService {
    public connectionIsEstablished = false;
    public _hubConnection: HubConnection;
    connectionEstablished = new EventEmitter<Boolean>();
    messageReceived = new EventEmitter<any>();
    instanceJoinMessage = new EventEmitter<any>();

    meetingsMessageReceived = new EventEmitter<any>();
    meetingMessageReceived = new EventEmitter<any>();
    updateMeetingMessage = new EventEmitter<any>();
    meetingAgendaCollectionMessageReceived = new EventEmitter<any>();
    meetingActionCollectionMessageReceived = new EventEmitter<any>();
    getActionCollectionMessage = new EventEmitter<any>();

    meetingAttachmentCollectionMessageReceived = new EventEmitter<any>();
    meetingNoteCollectionMessageReceived = new EventEmitter<any>();
    meetingDecisionCollectionMessage = new EventEmitter<any>();
    meetingObjectiveChangeMessage = new EventEmitter<any>();
    meetingOutcomeChangeMessage = new EventEmitter<any>();
    meetingDurationChangeMessage = new EventEmitter<any>();
    meetingTimeChangeMessage = new EventEmitter<any>();
    meetingAvailableCollectionMessage = new EventEmitter<any>();
    meetingattendeeCollectionMessage = new EventEmitter<any>();

    meetingNameChange = new EventEmitter<any>();
    meetingDateChangeMessage = new EventEmitter<any>();
    meetingLocationChangeMessage = new EventEmitter<any>();
    meetingTagsChangeMessage = new EventEmitter<any>();
    addMeetingAttendeeMessage = new EventEmitter<any>();
    addMeetingInviteAttendeeMessage = new EventEmitter<any>();
    meetingattendeeDeleteMessage = new EventEmitter<any>();
    addMeetingAgendaMessage = new EventEmitter<any>();
    deleteMeetingAgendaMessage = new EventEmitter<any>();
    addMeetingActionMessage = new EventEmitter<any>();
    deleteMeetingActionMessage = new EventEmitter<any>();
    deleteMeetingDecisionMessage = new EventEmitter<any>();
    addMeetingDecisionMessage = new EventEmitter<any>();
    addMeetingNoteMessage = new EventEmitter<any>();
    deleteMeetingNoteMessage = new EventEmitter<any>();
    createMeetingMessage = new EventEmitter<any>();
    ///////////////////// ACTION
    assignAttendeeActionMessage = new EventEmitter<any>();
    assignDueActionMessage = new EventEmitter<any>();
    assignCreatedActionMessage = new EventEmitter<any>();
    assignTextActionMessage = new EventEmitter<any>();
    assignTitleActionMessage = new EventEmitter<any>();
    assignStatusActionMessage = new EventEmitter<any>();
    ///////////////////// AGENDA
    assignOrderAgendaMessage = new EventEmitter<any>();
    assignDurationAgendaMessage = new EventEmitter<any>();
    assignTitleAgendaMessage = new EventEmitter<any>();
    assignTextAgendaMessage = new EventEmitter<any>();
    assignAttendeeAgendaMessage = new EventEmitter<any>();
    //////////////////// DECISION
    assignDecisionUpdateMessage = new EventEmitter<any>();
    //////////////////// NOTE
    assignNoteUpdateMessage = new EventEmitter<any>();
    ////////////////////
    updatePersonMessage = new EventEmitter<any>();
    public panelChange: EventEmitter<PanelEvent> = new EventEmitter<PanelEvent>();
    public meetingAttendees: any;

    public DefaultAgendaId = 'e38b69b3-8f2a-4979-9323-1819db4331f8';
    private _meetingObject = new Subject<MeetingModel>();
    _Meeting$ = this._meetingObject.asObservable();

    private _currentAttendee = new Subject<MeetingAttendee>();
    _CurrentAttendee$ = this._currentAttendee.asObservable();

    private _currentAttachment = new Subject<any>();
    _CurrentAttachment$ = this._currentAttachment.asObservable();

    private _currentAction = new Subject<MeetingAction>();
    _CurrentAction$ = this._currentAction.asObservable();

    private _currentDecision = new Subject<MeetingDecision>();
    _CurrentDecision$ = this._currentDecision.asObservable();

    private _currentNote = new Subject<MeetingNote>();
    _CurrentNote$ = this._currentNote.asObservable();

    private _agendaCollection: MeetingAgenda[];
    private _currentAgenda = new Subject<MeetingAgenda>();
    _CurrentAgenda$ = this._currentAgenda.asObservable();

    private _currentAgendaCollection = new Subject<MeetingAgenda[]>();
    _CurrentAgendaCollection$ = this._currentAgendaCollection.asObservable();

    private _panel = new Subject<string>();
    _Panel$ = this._panel.asObservable();

    constructor(
        private _meetingRepository: MeetingRepositoryService,
        private _http: AuthHttpRequestServiceService,
        private _profileService: ProfileService
    ) {
        this.createConnection();
        this.registerOnServerEvents();
        this.registerInstanceServerEvents();
        this.registerMeetingServerEvents();
        this.startConnection();
    }
    private createConnection() {
        const user = this._profileService.get();
        const token = `${user.idToken}`;
        this._hubConnection = new HubConnectionBuilder()
            .withUrl(`${environment.apiUri}/messageHub`,
            {
                accessTokenFactory: () => token
            })
            .configureLogging(LogLevel.Debug)
            .build();

    }

    public startConnection(): void {
        this._hubConnection
            .start()
            .then(() => {
                this.connectionIsEstablished = true;
                console.log('Hub connection started');
                this.connectionEstablished.emit(true);
            })
            .catch(err => {
                console.log(`Error while establishing connection, retrying...${err}`);
            });
    }

    public registerOnServerEvents(): void {
        this._hubConnection.on('ReceiveMessage', (data: any) => {
            this.messageReceived.emit(data);
        });
    }

    public registerInstanceServerEvents(): void {
        this._hubConnection.on('InstanceJoinMessage', (data: any) => {
            this.instanceJoinMessage.emit(data);
        });
    }

    public registerMeetingServerEvents(): void {

        this._hubConnection.on('meetingActionCollectionMessageReceived', (data: any) => {
            this.getActionCollectionMessage.emit(data);
        });

        this._hubConnection.on('MeetingsMessage', (data: any) => {
            this.meetingsMessageReceived.emit(data);
        });
        this._hubConnection.on('MeetingMessage', (data: any) => {
            this.meetingMessageReceived.emit(data);
        });
        this._hubConnection.on('updateMeetingMessage', (data: any) => {
            this.updateMeetingMessage.emit(data);
        });
        this._hubConnection.on('MeetingAgendaCollectionMessage', (data: any) => {
            this.meetingAgendaCollectionMessageReceived.emit(data);
        });
        this._hubConnection.on('MeetingActionCollectionMessage', (data: any) => {
            this.meetingActionCollectionMessageReceived.emit(data);
        });
        this._hubConnection.on('meetingDecisionCollectionMessage', (data: any) => {
            this.meetingDecisionCollectionMessage.emit(data);
        });
        this._hubConnection.on('meetingNoteCollectionMessageReceived', (data: any) => {
            this.meetingNoteCollectionMessageReceived.emit(data);
        });
        this._hubConnection.on('meetingNameChange', (data: any) => {
            this.meetingNameChange.emit(data);
        });
        this._hubConnection.on('meetingDateChangeMessage', (data: any) => {
            this.meetingDateChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingLocationChangeMessage', (data: any) => {
            this.meetingLocationChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingTagsChangeMessage', (data: any) => {
            this.meetingTagsChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingObjectiveChangeMessage', (data: any) => {
            this.meetingObjectiveChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingOutcomeChangeMessage', (data: any) => {
            this.meetingOutcomeChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingDurationChangeMessage', (data: any) => {
            this.meetingDurationChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingTimeChangeMessage', (data: any) => {
            this.meetingTimeChangeMessage.emit(data);
        });
        this._hubConnection.on('meetingAvailableCollectionMessage', (data: any) => {
            this.meetingAvailableCollectionMessage.emit(data);
        });
        this._hubConnection.on('meetingattendeeCollectionMessage', (data: any) => {
            this.meetingattendeeCollectionMessage.emit(data);
        });
        this._hubConnection.on('addMeetingAttendeeMessage', (data: any) => {
            this.addMeetingAttendeeMessage.emit(data);
        });
        this._hubConnection.on('addMeetingInviteAttendeeMessage', (data: any) => {
            this.addMeetingInviteAttendeeMessage.emit(data);
        });
        this._hubConnection.on('meetingattendeeDeleteMessage', (data: any) => {
            this.meetingattendeeDeleteMessage.emit(data);
        });
        this._hubConnection.on('addMeetingAgendaMessage', (data: any) => {
            this.addMeetingAgendaMessage.emit(data);
        });
        this._hubConnection.on('deleteMeetingAgendaMessage', (data: any) => {
            this.deleteMeetingAgendaMessage.emit(data);
        });
        this._hubConnection.on('addMeetingActionMessage', (data: any) => {
            this.addMeetingActionMessage.emit(data);
        });
        this._hubConnection.on('deleteMeetingActionMessage', (data: any) => {
            this.deleteMeetingActionMessage.emit(data);
        });
        this._hubConnection.on('deleteMeetingDecisionMessage', (data: any) => {
            this.deleteMeetingDecisionMessage.emit(data);
        });
        this._hubConnection.on('addMeetingDecisionMessage', (data: any) => {
            this.addMeetingDecisionMessage.emit(data);
        });
        this._hubConnection.on('addMeetingNoteMessage', (data: any) => {
            this.addMeetingNoteMessage.emit(data);
        });
        this._hubConnection.on('deleteMeetingNoteMessage', (data: any) => {
            this.deleteMeetingNoteMessage.emit(data);
        });
        this._hubConnection.on('createMeetingMessage', (data: any) => {
            this.createMeetingMessage.emit(data);
        });
        ///////////////////// ACTION
        this._hubConnection.on('assignAttendeeActionMessage', (data: any) => {
            this.assignAttendeeActionMessage.emit(data);
        });
        this._hubConnection.on('assignDueActionMessage', (data: any) => {
            this.assignDueActionMessage.emit(data);
        });
        this._hubConnection.on('assignCreatedActionMessage', (data: any) => {
            this.assignCreatedActionMessage.emit(data);
        });
        this._hubConnection.on('assignTextActionMessage', (data: any) => {
            this.assignTextActionMessage.emit(data);
        });
        this._hubConnection.on('assignTitleActionMessage', (data: any) => {
            this.assignTitleActionMessage.emit(data);
        });
        this._hubConnection.on('assignStatusActionMessage', (data: any) => {
            this.assignStatusActionMessage.emit(data);
        });
        ///////////////////////// AGENDA
        this._hubConnection.on('assignOrderAgendaMessage', (data: any) => {
            this.assignOrderAgendaMessage.emit(data);
        });
        this._hubConnection.on('assignDurationAgendaMessage', (data: any) => {
            this.assignDurationAgendaMessage.emit(data);
        });
        this._hubConnection.on('assignTitleAgendaMessage', (data: any) => {
            this.assignTitleAgendaMessage.emit(data);
        });
        this._hubConnection.on('assignTextAgendaMessage', (data: any) => {
            this.assignTextAgendaMessage.emit(data);
        });
        this._hubConnection.on('assignAttendeeAgendaMessage', (data: any) => {
            this.assignAttendeeAgendaMessage.emit(data);
        });
        ///////////////////////// DECISION
        this._hubConnection.on('assignDecisionUpdateMessage', (data: any) => {
            this.assignDecisionUpdateMessage.emit(data);
        });
        //////////////////////// NOTE
        this._hubConnection.on('assignNoteUpdateMessage', (data: any) => {
            this.assignNoteUpdateMessage.emit(data);
        });
        //////////////////////// PERSON
        this._hubConnection.on('updatePersonMessage', (data: any) => {
            this.updatePersonMessage.emit(data);
        });
    }

    public sendGroupMessage(message: any, groupId: string) {
        this._hubConnection.invoke('SendMessage', message, groupId);
    }

    public joinGroup(groupId: string): void {
        if (this._hubConnection) {
            this._hubConnection.invoke('AddToGroup', groupId);
        }
    }

    public leaveGroup(groupId: string): void {
        if (this._hubConnection) {
            this._hubConnection.invoke('RemoveFromGroup', groupId);
        }
    }

    public canEdit(meeting: MeetingModel, user: UserProfile, mode: string): boolean {
        if (mode === 'publish') {
            return false;
        }
        if (mode === 'minute') {
            return false;
        }
        if (meeting.meetingOwnerId.toUpperCase() === user.email.toUpperCase()) {
            if (user.role === 'Admin') {
                return true;
            }
            return true;
        }

        if (mode === 'view') {
            return false;
        }
        if (mode === 'run') {
            return false;
        }
        return false;
    }

    public toLocalTime(date: Date): Date {
        const nowTime = new Date();
        const nowHour = nowTime.getHours();
        const nowMin = nowTime.getMinutes();
        date.setHours(nowHour,nowMin,0,0);
        return date;
    }

    public RecurrenceTypes(meetingData: Date): Array<RecurrenceType> {
        const meetingDate = new Date(meetingData);
        const weekDay = this.transformWeekDay(meetingDate.getDay());
        const monthDay = meetingDate.getDate();
        // const month = this.transformMonth(meetingDate.getMonth());
        const data = [];
        data.push(new RecurrenceType({ id: 0, value: `No Recurrence` }));
        data.push(new RecurrenceType({ id: 1, value: `Daily` }));
        data.push(new RecurrenceType({ id: 2, value: `Weekly on ${weekDay}` }));
        data.push(
            new RecurrenceType({ id: 3, value: `Monthly on ${monthDay}` })
        );
        data.push(
            new RecurrenceType({ id: 4, value: `Annually on ${weekDay}` })
        );
        data.push(
            new RecurrenceType({
                id: 5,
                value: `Every week day (Monday to Friday)`
            })
        );
        return data;
    }

    public Meeting(meeting: MeetingModel, user: UserProfile) {
        this._meetingObject.next(meeting);
    }

    public CurrentNote(note: MeetingNote) {
        this._currentNote.next(note);
    }

    public CurrentAttendee(attendee: MeetingAttendee) {
        this._currentAttendee.next(attendee);
    }

    public CurrentAttachment(attachment: any) {
        this._currentAttachment.next(attachment);
    }

    public CurrentAction(action: MeetingAction) {
        this._currentAction.next(action);
    }

    public CurrentDecision(decision: MeetingDecision) {
        this._currentDecision.next(decision);
    }

    public CurrentAgenda(agenda: MeetingAgenda) {
        this._currentAgenda.next(agenda);
    }

    public getMeetingAgenda(): MeetingAgenda[] {
        return this._agendaCollection;
    }

    public addMeetingAgenda(agenda: MeetingAgenda): MeetingAgenda[] {
        this._agendaCollection.push(agenda);
        return this._agendaCollection;
    }

    public setMeetingAgenda(collection: MeetingAgenda[]): MeetingAgenda[] {
        return (this._agendaCollection = collection);
    }

    public CurrentAgendaCollection(agenda: MeetingAgenda[]) {
        this._currentAgendaCollection.next(agenda);
    }

    public MeetingPanel(panel: string) {
        this._panel.next(panel);
    }

    public getPreviewFile(id: string, user: UserProfile): Observable<Blob> {
        return this._http.getFile(user, `/reports/demo?id=${id}`);
    }

    public sendInvitations(
        model: MeetingInviteModel,
        user: UserProfile
    ): Observable<any> {
        return this._meetingRepository.sendMeetingInvitations(user, model);
    }

    public sendMinutes(meetingId: string, user: UserProfile): Observable<any> {
        return this._meetingRepository.sendMeetingMinutes(user, meetingId);
    }

    public get(meetingId: string, user: UserProfile): Observable<MeetingModel> {
        return this._meetingRepository.getMeeting(user, meetingId);
    }

    public create(user: UserProfile): Observable<MeetingModel> {
        return this._meetingRepository.createNewMeeting(user);
    }

    public createEmptyMeeting(): MeetingModel {
        const instanceId = sessionStorage.getItem('instanceId');
        return new MeetingModel({
            tag: [],
            timeZone: '',
            recurrenceType: 0,
            reacuranceType: '0',
            name: '',
            isPrivate: false,
            isFormal: false,
            duration: 0,
            time: '',
            purpose: '',
            outcome: '',
            location: '',
            status: 'create',
            meetingActionCollection: [],
            meetingNoteCollection: [],
            meetingAttachmentCollection: [],
            meetingAgendaCollection: [],
            meetingAttendeeCollection: [],
            meetingDecisionCollection: [],
            id: '',
            instanceId: instanceId,
            availableAttendeeCollection: [],
            isLocked: false,
            isRecurrence: false,
            meetingOwnerId: '',
            recurrence: new MeetingRecurrence({
                endDate: new Date(),
                monthOnDay: 'Jan',
                monthOnWeekDayNumber: 1,
                recurrenceNumber: 1,
                startDate: new Date(),
                type: '',
                weekOnWeekDay: 1,
                number: 1
            }),
            date: new Date(),
            updatedDate: new Date(),
            timeZoneOffSet: 1
        });
    }

    private getAgenda(
        agendaItems: Array<MeetingAgenda>,
        id: string
    ): MeetingAgenda {
        agendaItems.forEach(item => {
            if (item.id === id) {
                return item;
            }
        });
        return null;
    }

    transformMonth(value: any): any {
        let month: string;
        switch (value) {
            case 0:
                month = 'January';
                break;
            case 1:
                month = 'February';
                break;
            case 2:
                month = 'March';
                break;
            case 3:
                month = 'April';
                break;
            case 4:
                month = 'May';
                break;
            case 5:
                month = 'June';
                break;
            case 6:
                month = 'July';
                break;
            case 7:
                month = 'August';
                break;
            case 8:
                month = 'September';
                break;
            case 9:
                month = 'October';
                break;
            case 10:
                month = 'November';
                break;
            case 11:
                month = 'December';
                break;

            default:
                break;
        }

        return month;
    }

    transformWeekDay(value: any): any {
        let day: string;
        switch (value) {
            case 1:
                day = 'Monday';
                break;

            case 2:
                day = 'Tuesday';
                break;
            case 3:
                day = 'Wednesday';
                break;
            case 4:
                day = 'Thursday';
                break;
            case 5:
                day = 'Friday';
                break;
            case 6:
                day = 'Saturday';
                break;
            case 7:
                day = 'Sunday';
                break;

            default:
                break;
        }

        return day;
    }
}
