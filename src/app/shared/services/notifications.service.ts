import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Notification } from '../models';
import {MeetingModel} from '../models';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  private _loading = new Subject<boolean>();
  _Loading$ = this._loading.asObservable();

  public notifications = new Subject<Notification>();
  public noteAdded = this.notifications.asObservable();

  private notificationTypes: Array<string> = [];

  constructor() {
    this.notificationTypes = [];
    this.notificationTypes.push('Error');
    this.notificationTypes.push('Info');
    this.notificationTypes.push('Warning');
    this.notificationTypes.push('Success');
  }

  public Loading(load: boolean): void {
    this._loading.next(load);
  }

  public add(notification: Notification) {
    if (this.notificationTypes.includes(notification.type)) {
      this.notifications.next(notification);
    } else {
      console.log('Notification Type does not exist: ' + notification.type);
    }
  }
}
