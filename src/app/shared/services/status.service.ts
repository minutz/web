import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class StatusService {

    public MeetingAttendeeStatusCollection: Array<string> = [
        'Accepted',
        'Declined',
        'Pending',
        'Invited',
        'Available'
    ];

    public MeetingActionStatusCollection: Array<string> = [
        'Pending',
        'Complete'
    ];

    public StatusCollection: Array<string> = [
        'Accepted',
        'Declined',
        'Pending',
        'Invited'
    ];

    constructor() {
    }
}
