import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  public MeetingRoleCollection: Array<string> = ['User', 'Guest', 'Admin', 'Owner', 'Attendee'];
  constructor() { }
}
