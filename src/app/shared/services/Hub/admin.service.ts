import {EventEmitter, Injectable} from '@angular/core';
import {HubConnection, HubConnectionBuilder, LogLevel} from '@aspnet/signalr';
import {environment} from 'src/environments/environment';
import {AuthHttpRequestServiceService} from '../auth-http-request-service.service';
import {ProfileService} from '../profile.service';


@Injectable({
  providedIn: 'root'
})
export class AdminService {
    public connectionIsEstablished = false;
    public _hubConnection: HubConnection;
    connectionEstablished = new EventEmitter<Boolean>();
    messageReceived = new EventEmitter<any>();
    instanceJoinMessage = new EventEmitter<any>();
    instanceLeaveMessage = new EventEmitter<any>();

    getUsersAsync = new EventEmitter<any>();
    getAvailableUsersAsync = new EventEmitter<any>();
    updateUserAsync = new EventEmitter<any>();
    createUserAsync = new EventEmitter<any>();
    deleteUserAsync = new EventEmitter<any>();
  constructor(
      private _http: AuthHttpRequestServiceService,
      private _profileService: ProfileService) {
      this.createConnection();
  }

    private createConnection() {
        const user = this._profileService.get();
        const token = `${user.idToken}`;
        this._hubConnection = new HubConnectionBuilder()
            .withUrl(`${environment.apiUri}/adminHub`,
                {
                    accessTokenFactory: () => token
                })
            .configureLogging(LogLevel.Debug)
            .build();

    }

    public startConnection(): void {
        this._hubConnection
            .start()
            .then(() => {
                this.connectionIsEstablished = true;
                console.log('Hub connection started');
                this.connectionEstablished.emit(true);
            })
            .catch(err => {
                console.log(`Error while establishing connection, retrying...${err}`);
            });
    }

    public registerAdminServerEvents(): void {
        this._hubConnection.on('instanceJoinMessage', (data: any) => {
            this.instanceJoinMessage.emit(data);
        });
        this._hubConnection.on('instanceLeaveMessage', (data: any) => {
            this.instanceLeaveMessage.emit(data);
        });
        this._hubConnection.on('getUsersAsync', (data: any) => {
            this.getUsersAsync.emit(data);
        });
        this._hubConnection.on('getAvailableUsersAsync', (data: any) => {
            this.getAvailableUsersAsync.emit(data);
        });
        this._hubConnection.on('updateUserAsync', (data: any) => {
            this.updateUserAsync.emit(data);
        });
        this._hubConnection.on('createUserAsync', (data: any) => {
            this.createUserAsync.emit(data);
        });
        this._hubConnection.on('deleteUserAsync', (data: any) => {
            this.deleteUserAsync.emit(data);
        });
    }

    public sendAdminMessage(message: any, groupId: string) {
        this._hubConnection.invoke('SendMessage', message, groupId);
    }

    public joinAdminHub(groupId: string): void {
        if (this._hubConnection) {
            this._hubConnection.invoke('AddToGroup', groupId);
        }
    }

    public leaveAdminHub(groupId: string): void {
        if (this._hubConnection) {
            this._hubConnection.invoke('RemoveFromGroup', groupId);
        }
    }
}
