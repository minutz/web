import {EventEmitter, Injectable} from '@angular/core';
import {LoadEvent} from '../models/load-event';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    public onChange: EventEmitter<LoadEvent> = new EventEmitter<LoadEvent>();
    public loading(loading: boolean) {
        this.onChange.emit({ loading: loading, eventId: 42});
    }
}
