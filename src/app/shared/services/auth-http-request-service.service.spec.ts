import { TestBed, inject } from '@angular/core/testing';

import { AuthHttpRequestServiceService } from './auth-http-request-service.service';

describe('AuthHttpRequestServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthHttpRequestServiceService]
    });
  });

  it('should be created', inject([AuthHttpRequestServiceService], (service: AuthHttpRequestServiceService) => {
    expect(service).toBeTruthy();
  }));
});
