import { NgModule } from '@angular/core';

import {
    AuthHttpRequestServiceService,
    AuthenticationService,
    LocalStorageService,
    MeetingService,
    NotificationsService,
    ProfileService,
    RoleService,
    SessionStorageService,
    SideMenuService,
    StatusService,
    TemplateService,
    UrlService,
    ValidationService,
    LoadingService,
    BreadcrumbService
} from '.';

@NgModule({
    providers: [
        AuthHttpRequestServiceService,
        AuthenticationService,
        LocalStorageService,
        MeetingService,
        NotificationsService,
        ProfileService,
        RoleService,
        SessionStorageService,
        SideMenuService,
        StatusService,
        TemplateService,
        UrlService,
        ValidationService,
        LoadingService,
        BreadcrumbService
    ],
})
export class ServicesModule { }
