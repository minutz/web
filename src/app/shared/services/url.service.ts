import { Injectable } from '@angular/core';
import { SessionStorageService } from './session-storage.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

  mode: string;

  constructor(
    private _sessionStorageService: SessionStorageService,
    private route: ActivatedRoute
  ) { }

  public getReference(query: string): any {
    if (query && query !== '') {
      if (query.indexOf('?') > -1) {
        return query.split('?')[1];
      }
    }
  }

  public getReferencePair(query: string, separator: string): [string, string] {
    if (query && query !== '') {
      if (query.indexOf(separator) > -1) {
        const splits = query.split(separator);
        return [ splits[0], splits[1]];
      }
    }
  }

  public getInstanceQueryString(url: string): string {
        if (url.indexOf('?') > -1) {
            if (url.indexOf('id') > -1) {
                const splitString = url.split('?')[1];
                const instanceId = splitString.split('&')[1].split('=')[1];
                return instanceId;
            }
            if (url.indexOf('meetingId') > -1) {
                const splitString = url.split('?')[1];
                const meetingSection = splitString.split('=')[1];
                const instanceId = meetingSection.split('&')[1];
                return instanceId;
            }
            return '';
        }
        return '';
    }

  public getMeetingQueryString(url: string): string {
    if (url.indexOf('?') > -1) {
      if (url.indexOf('id') > -1) {
        const splitstring = url.split('?')[1];
        const meetingId = splitstring.split('&')[0].split('=')[1];
        if (splitstring.split('&')[2]) {
          this._sessionStorageService.set('mode', splitstring.split('&')[2].split('=')[1]);
        }
        return meetingId;
      }
      if (url.indexOf('meetingId') > -1) {
        const splitstring = url.split('?')[1];
        const meetingSection = splitstring.split('=')[1];
        const meetingId = meetingSection.split('&')[0];
        // const modeSection = meetingSection.split('&')[1];
        if (splitstring.split('=')[2]) {
          const mode = splitstring.split('=')[2];
          this._sessionStorageService.set('mode', mode);
        }
        return meetingId;
      }
      return '';
    }
    return '';
  }

  public getModeQueryString(url: any): any {
      this.route.queryParams
      .filter(params => params.mode)
      .subscribe(params => {
        url = params.mode;
      });

      return url;
  }

  public convertQueryStringToJson(query: string): any {
    if (query && query !== '') {
      let hash: any;
      const myJson: any = {};
      const hashes = query.slice(query.indexOf('?') + 1).split('&');

      for (let i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        myJson[hash[0]] = hash[1];
      }
      return myJson;
    }
    return null;
  }

  public checkQueryParameters(query: string): void {
    if (query && query !== '') {
        if (query.indexOf('?') > -1) {
            const baseQuery = query.split('?')[1];
            if (baseQuery.indexOf('&') > -1) {
                // multiple
                const referenceItems = baseQuery.split('&');
                referenceItems.forEach( q => {
                    if ( q.indexOf('=') > -1) {
                        const referenceItem = q.split('=');
                        sessionStorage.setItem(referenceItem[0], referenceItem[1]);
                    }
                });
            } else {
                // single
                const baseArray = baseQuery.split('=');
                if (baseArray[0] === 'r') {
                    sessionStorage.setItem('r', baseArray[1]);
                }
            }
          }
    }
  }

}
