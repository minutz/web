import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UrlService } from './url.service';
import { LocalStorageService } from './local-storage.service';
import { SessionStorageService } from './session-storage.service';
import { ProfileService } from './profile.service';
import { ReferenceModel } from '../models';
import { AuthHttpRequestServiceService } from './auth-http-request-service.service';
import { UserProfile } from '../models';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private static getQueryParameters(): string {
    return window.location.hash.substring(1, window.location.hash.length);
  }

  constructor(
    private router: Router,
    private _urlService: UrlService,
    private _localStorageService: LocalStorageService,
    private _profileService: ProfileService,
    private _sessionStorageService: SessionStorageService,
    private _authHttpRequestService: AuthHttpRequestServiceService,
    public http: HttpClient
  ) {
  }

  public login(username: string, password: string): Observable<UserProfile> {
    const urlReference = this._profileService.getReferenceFromUrl();
    const payload = {
      password: password,
      username: username,
      instanceId: ''
    };
    if (urlReference) {
      payload.instanceId = urlReference.instanceId;
    }
    const storageReference = this._profileService.getReferenceFromStorage();
    if (storageReference) {
      payload.instanceId = storageReference.instanceId;
    }
    const url = `${environment.authUri}/api/user/login`;
    return this.http.post<UserProfile>(url, payload);
  }

  public checkAuthentication(): void {
    const authenticated = this.isAuthenticated();
    const referenceModel: ReferenceModel = this._profileService.getReferenceFromUrl();
    if (this._localStorageService.get(this._sessionStorageService.minutzApp)) {
      if (!authenticated) {
        this.navigateLogin();
      }
    } else {
        this.navigateLogin();
    }
    this.handleAuthentication();
  }

  public navigateLogin(): void {
    this.router.navigate(['/login']);
  }

  public navigateSignUp(): void {
    this.router.navigate(['/signup']);
  }

  public isAuthenticated(): boolean {
    if (this._sessionStorageService.get(this._sessionStorageService.minutz)) {
      const expiresAt = this._sessionStorageService.get(this._sessionStorageService.expires_at);
      return new Date() < new Date(expiresAt);
    }
    return false;
  }

  public handleAuthentication(): void {
  }

  public logout(): void {
    this._sessionStorageService.kill(this._sessionStorageService.expires_at);
    this._sessionStorageService.kill(this._sessionStorageService.access_token);
    this._sessionStorageService.kill(this._sessionStorageService.id_token);
    this._sessionStorageService.kill(this._sessionStorageService.minutzApp);
    this._sessionStorageService.kill(this._sessionStorageService.minutz);
    window.location.href = environment.redirectUri;
  }

}
