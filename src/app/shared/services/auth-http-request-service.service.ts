import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NotificationsService } from './notifications.service';
import { SessionStorageService } from './session-storage.service';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UserProfile } from '../models';
import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class AuthHttpRequestServiceService {
    authorization = 'Authorization';
    access_token = 'accessToken';
    expires_at = 'expiresAt';
    id_token = 'idToken';
    x_auth_header = 'xAuthHeader';
    x_exp = 'xExp';
    x_auth_minutz = 'xAuthMinutz';

    constructor(
        public http: HttpClient,
        public notificationService: NotificationsService,
        private _sessionStorageService: SessionStorageService,
        public router: Router
    ) {
    }
    public isAuthenticated(): boolean {
        if (this._sessionStorageService.get(this._sessionStorageService.minutz)) {
          const expiresAt = this._sessionStorageService.get(this._sessionStorageService.expires_at);
          return new Date() < new Date(expiresAt);
        }
        return false;
      }
    public postFullUrl(user: UserProfile, url: string, data: any): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const headers = new HttpHeaders()
            .set('content-type', 'application/json');
        return this.http.post(url, data, {headers: headers});
    }
    public postWithUrl(user: UserProfile, baseUrl: string, action: string, data: any): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${baseUrl}/api` + action;
        const headers = new HttpHeaders()
            .set('content-type', 'application/json');
        return this.http.post(url, data, {headers: headers});
    }
    public post(user: UserProfile, action: string, data: any): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.apiUri}/api` + action;
        const headers = this.getHeaders(user);
        return this.http.post(url, data, {headers: headers});
    }

    public postFile(user: UserProfile, action: string, data: any): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.attApiUri}/api` + action;
        const headers = this.getFileHeaders(user);
        return this.http.post(url, data, {headers: headers});
    }
    public deleteAttachment(user: UserProfile, action: string): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.attApiUri}/api` + action;
        const headers = this.getFileHeaders(user);
        return this.http.delete(url, {headers: headers});
    }

    public getAttachment(user: UserProfile, action: string): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.attApiUri}/api` + action;
        const token = `Bearer ${user.idToken}`;
        const access_token = user.accessToken;
        const idToken = user.idToken;
        return this.http.get(url, {
            headers: {
                'xAuthHeader': 'xAuthMinutz',
                'Content-Type': 'application/json',
                'Authorization': token,
                'accessToken': access_token,
                'idToken': idToken
            }
        });
        // todo: Uncomment this on merge request return this.http.get(url, { headers : headers });
    }

    public get(user: UserProfile, action: string): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.apiUri}/api` + action;
        const token = `Bearer ${user.idToken}`;
        const access_token = user.accessToken;
        const idToken = user.idToken;
        return this.http.get(url, {
            headers: {
                'xAuthHeader': 'xAuthMinutz',
                'Content-Type': 'application/json',
                'Authorization': token,
                'accessToken': access_token,
                'idToken': idToken
            }
        });
        // todo: Uncomment this on merge request return this.http.get(url, { headers : headers });
    }

    public getFile(user: UserProfile, action: string) {
        const url = `${environment.apiUri}/api` + action;
        const headers = this.getHeaders(user);
        return this.http.get(url,
            {
                headers: headers,
                responseType: 'blob'
            }
        );
    }

    public put(user: UserProfile, action: string, data: any): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.apiUri}/api` + action;
        const headers = this.getHeaders(user);
        const token = `Bearer ${user.idToken}`;
        const access_token = user.accessToken;
        const idToken = user.idToken;
        const ex = user.tokenExpire;
        return this.http.put(url, data, {
            headers:
                {
                    'xAuthHeader': 'xAuthMinutz',
                    'Content-Type': 'application/json',
                    'Authorization': token,
                    'accessToken': access_token,
                    'idToken': idToken,
                    'xExp': ex
                }
        });
    }

    public delete(user: UserProfile, action: string): Observable<any> {
        if (!this.isAuthenticated()) {
            this.router.navigate(['/login']);
        }
        const url = `${environment.apiUri}/api` + action;
        const headers = this.getHeaders(user);
        return this.http.delete(url, {headers: headers});
    }

    public getFileHeaders(user: UserProfile): any {
        if (user) {
            const token = `Bearer ${user.idToken}`;
            const access_token = user.accessToken;
            const idToken = user.idToken;
            const ex = user.tokenExpire;
            const jwtHeaders = new HttpHeaders()
                .set('xAuthHeader', 'xAuthMinutz')
                .set('Authorization', token)
                .set('xAuthHeader', 'xAuthMinutz')
                .set('accessToken', access_token)
                .set('idToken', idToken)
                .set('xExp', ex);
            return jwtHeaders;
        } else {
            const headers = new HttpHeaders()
                .set('content-type', 'application/json')
                .set('xAuthHeader', 'xAuthMinutz');
            return headers;
        }
    }

    public getHeaders(user: UserProfile): any {
        if (user) {
            const token = `Bearer ${user.idToken}`;
            const access_token = user.accessToken;
            const idToken = user.idToken;
            const ex = user.tokenExpire;
            const jwtHeaders = new HttpHeaders()
                .set('xAuthHeader', 'xAuthMinutz')
                .set('Content-Type', 'application/json')
                .set('Authorization', token)
                .set('xAuthHeader', 'xAuthMinutz')
                .set('accessToken', access_token)
                .set('idToken', idToken)
                .set('xExp', ex);
            return jwtHeaders;
        } else {
            const headers = new HttpHeaders()
                .set('content-type', 'application/json')
                .set('xAuthHeader', 'xAuthMinutz');
            return headers;
        }
    }
}
