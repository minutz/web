import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UrlService } from './url.service';
import { LocalStorageService } from './local-storage.service';
import { SessionStorageService } from './session-storage.service';
import { ReferenceModel, UserProfile } from '../models';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    _defaultCompanyLogo = 'assets/images/minutz_logo-icon.png';
    private _userProfile = new Subject<UserProfile>();
    _Profile$ = this._userProfile.asObservable();

    constructor(
        private router: Router,
        private _urlService: UrlService,
        private _localStorageService: LocalStorageService,
        private _sessionStorageService: SessionStorageService
    ) {}

    public Profile(userProfile: UserProfile): void {
        if (userProfile) {
            if (!userProfile.companyLogo) {
                userProfile.companyLogo = this._defaultCompanyLogo;
            }
            if (!userProfile.company) {
                userProfile.company = `${userProfile.nickname}' company`;
            }
        }
        this._userProfile.next(userProfile);
    }

    public set(profile: UserProfile): UserProfile {
        const idToken = JSON.stringify(profile.idToken);
        const userString = JSON.stringify(profile);
        const exp = profile.tokenExpire;
        this._sessionStorageService.set(
            this._sessionStorageService.id_token,
            idToken
        );
        this._sessionStorageService.set(
            this._sessionStorageService.expires_at,
            exp
        );
        this._sessionStorageService.set(
            this._sessionStorageService.access_token,
            profile.accessToken
        );
        this._sessionStorageService.set(
            this._sessionStorageService.refInstanceId,
            profile.instanceId
        );
        this._sessionStorageService.set(
            this._sessionStorageService.referenceRole,
            profile.role
        );
        this._sessionStorageService.set(
            this._sessionStorageService.minutzApp,
            profile.sub
        );
        this._sessionStorageService.set(
            this._sessionStorageService.minutz,
            userString
        );
        this._userProfile.next(profile);
        return profile;
    }

    public get(): UserProfile {
        if (
            this._sessionStorageService.check(
                this._sessionStorageService.applicationKey
            )
        ) {
            if (
                this._sessionStorageService.get(
                    this._sessionStorageService.applicationKey
                ) === ''
            ) {
                this.router.navigate(['/login']);
            }
            const value = this._sessionStorageService.get(
                this._sessionStorageService.applicationKey
            );
            return JSON.parse(value);
        }
        this.router.navigate(['/login']);
    }

    public getReferenceFromUrl(): ReferenceModel {
        const referenceCheck = this._urlService.getReference(
            window.location.href
        );
        if (referenceCheck) {
            const referencePairs = this._urlService.getReferencePair(
                referenceCheck,
                '|'
            );
            if (referencePairs) {
                const referenceRole = referencePairs[0];
                this._sessionStorageService.set(
                    this._sessionStorageService.referenceRole,
                    referenceRole
                );
                const referenceInfo = this._urlService.getReferencePair(
                    referencePairs[1],
                    '&'
                );
                if (referenceInfo) {
                    this._sessionStorageService.set(
                        this._sessionStorageService.refInstanceId,
                        referenceInfo[0]
                    );
                    this._sessionStorageService.set(
                        this._sessionStorageService.refMeetingId,
                        referenceInfo[1]
                    );
                    return new ReferenceModel({
                        meetingId: referenceInfo[1],
                        instanceId: referenceInfo[0],
                        role: referenceRole
                    });
                }
            }
        }
        return new ReferenceModel({
            meetingId: '',
            instanceId: '',
            role: ''
        });
    }

    public getReferenceFromStorage(): ReferenceModel {
        let role = this._sessionStorageService.get(
            this._sessionStorageService.referenceRole
        );
        if (!role) {
            role = '';
        }
        let instanceId = this._sessionStorageService.get(
            this._sessionStorageService.refInstanceId
        );
        if (!instanceId) {
            instanceId = '';
        }
        let meetingId = this._sessionStorageService.get(
            this._sessionStorageService.refMeetingId
        );
        if (!meetingId) {
            meetingId = '';
        }
        return new ReferenceModel({
            meetingId: meetingId,
            instanceId: instanceId,
            role: role
        });
    }

    public applicationSet(profile: UserProfile): void {
        if (
            !this._localStorageService.get(
                this._sessionStorageService.minutzApp
            )
        ) {
            this._localStorageService.set(
                this._sessionStorageService.minutzApp,
                profile.sub
            );
        }
    }
}
