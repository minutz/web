import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        const time = value.split(':');
        if (time[0] > 11) {
            let hour = 12;
            if (time[0] === 13) {
                hour = 1;
            }
            if (time[0] === 14) {
                hour = 2;
            }
            if (time[0] === 15) {
                hour = 3;
            }
            if (time[0] === 16) {
                hour = 4;
            }
            if (time[0] === 17) {
                hour = 5;
            }
            if (time[0] === 18) {
                hour = 6;
            }
            if (time[0] === 19) {
                hour = 7;
            }
            if (time[0] === 20) {
                hour = 8;
            }
            if (time[0] === 21) {
                hour = 9;
            }
            if (time[0] === 22) {
                hour = 10;
            }
            if (time[0] === 23) {
                hour = 11;
            }
            return `${hour}:${time[1]} PM`;
        } else {
            let hour = time[0];
            if (hour !== '0') {
                if (time[0] < 10) {
                    hour = time[0].replace('0', '');
                }
            }
            return `${hour}: ${time[1]} AM`;
        }
  }

}
