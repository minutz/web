import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncateFileName'
})
export class TruncateFileNamePipe implements PipeTransform {

  transform(value: string, limit = 8, completeWords = false, ellipsis = '...') {
    if (completeWords) {
      limit = value.substr(0, 13).lastIndexOf(' ');
    }
    return `${value.substr(0, limit)}${ellipsis}`;
  }

}
