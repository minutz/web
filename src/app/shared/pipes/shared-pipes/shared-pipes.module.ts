import { NgModule } from '@angular/core';

import {
    TimePipe,
    DurationPipe,
    DurationTextPipe,
    WeekDayPipe,
    MonthPipe,
    TruncatePipe,
    TruncateFileNamePipe,
    SafePipe,
    AcronymPipe,
    HtmlToTextPipe
} from '..';

@NgModule({
    exports: [
        DurationPipe,
        DurationTextPipe,
        WeekDayPipe,
        MonthPipe,
        TruncatePipe,
        TruncateFileNamePipe,
        SafePipe,
        AcronymPipe,
        TimePipe
    ],
    declarations: [
        DurationPipe,
        DurationTextPipe,
        WeekDayPipe,
        MonthPipe,
        TruncatePipe,
        TruncateFileNamePipe,
        SafePipe,
        AcronymPipe,
        HtmlToTextPipe,
        TimePipe
    ]
})
export class SharedPipesModule { }
