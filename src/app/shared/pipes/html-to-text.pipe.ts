import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'html2text'
})
export class HtmlToTextPipe implements PipeTransform {

  transform(value: string): any {
    const tmp = document.createElement('DIV');
    tmp.innerHTML = value;
    return tmp.textContent || tmp.innerText || '';
  }

}
