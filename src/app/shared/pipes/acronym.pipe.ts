import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'acronym'
})
export class AcronymPipe implements PipeTransform {

  transform(val) {
      if (val === '') {
          return '';
      }
      if (val === ' ') {
          return '';
      }
      const matches = val.match(/\b(\w)/g);
      return matches.join('').toUpperCase();
  }
}
