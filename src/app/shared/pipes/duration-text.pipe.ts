import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'durationText'
})
export class DurationTextPipe implements PipeTransform {

  transform(value: any, arg: boolean): any {
    if (value) {
      const hours = Math.trunc(value / 60);
      const minutes = value % 60;

      if (arg) {
        if (+hours > 0) {
          return `${hours} hrs:${minutes} min`;
        }

        return `${minutes} min.`;
      }
      if (+hours > 0) {
        return `${hours} hours and ${minutes} minutes.`;
      }

      return `${minutes} minutes.`;
    }
  }

}
