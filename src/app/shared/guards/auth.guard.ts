import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private _authService: AuthenticationService) {
      const q = window.location.href;
      console.log(`${q}`);
   }

  public canActivate() {
    this._authService.checkAuthentication();
    return this._authService.isAuthenticated();
  }
}
