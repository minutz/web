export class UserRolesUser {
    public id: string;
    public fullName: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public dateActivated: Date;
    public lastLogin: Date;
    public role: string;
    public profilePicture: string;
    public status: string;

    constructor(obj?: UserRolesUser) {
        this.id = obj && obj.id || '';
        this.fullName = obj && obj.fullName || '';
        this.firstName = obj && obj.firstName || '';
        this.lastName = obj && obj.lastName || '';
        this.email = obj && obj.email || '';
        this.dateActivated = obj && obj.dateActivated || new Date();
        this.lastLogin = obj && obj.lastLogin || new Date();
        this.profilePicture = obj && obj.profilePicture || '';
        this.role = obj && obj.role || '';
        this.status = obj && obj.status || '';
    }
}
