export interface IMeetingAgenda {
    id: string;
    instanceId: string;
    agendaHeading: string;
    referenceId: string;
    agendaText: string;
    meetingAttendeeId: string;
    duration: number;
    durationLabel: string;
    createdDate: Date;
    purpose: string;
    outcome: string;
    isComplete: boolean;
    order: number;
}

export class MeetingAgenda {
    public id: string;
    public instanceId: string;
    public agendaHeading: string;
    public referenceId: string;
    public agendaText: string;
    public meetingAttendeeId: string;
    public duration: number;
    public durationLabel: string;
    public createdDate: Date;
    public purpose: string;
    public outcome: string;
    public isComplete: boolean;
    public order: number;

    constructor(obj?: MeetingAgenda) {
        this.id = obj && obj.id || '';
        this.instanceId = obj && obj.instanceId || '';
        this.agendaHeading = obj && obj.agendaHeading || '';
        this.referenceId = obj && obj.referenceId || '';
        this.agendaText = obj && obj.agendaText || '';
        this.meetingAttendeeId = obj && obj.meetingAttendeeId || '';
        this.duration = obj && obj.duration || 0;
        this.durationLabel = obj && obj.durationLabel || '00:00';
        this.createdDate = obj && obj.createdDate || new Date();
        this.purpose = obj && obj.purpose || '';
        this.outcome = obj && obj.outcome || '';
        this.isComplete = obj && obj.isComplete || false;
        this.order = obj && obj.order || 0;
    }
}
