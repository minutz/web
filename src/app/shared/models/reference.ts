export class ReferenceModel {
  public instanceId: string;
  public meetingId: string;
  public role: string;
  constructor(obj?: ReferenceModel) {
    this.instanceId = obj && obj.instanceId || '';
    this.meetingId = obj && obj.meetingId || '';
    this.role = obj && obj.role || '';
  }
}
