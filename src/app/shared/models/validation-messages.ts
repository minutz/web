export class ValidationMessages {
  AgendaName: Object;
  [field: string]: Object;

  constructor(obj?: ValidationMessages) {
    this.AgendaName = new Object({
      required: 'Agenda topic is required to add a new agenda'
    });
  }
}
