export class FormErrors {
  AgendaName: string;
  [field: string]: string;

  constructor(obj?: FormErrors) {
    this.AgendaName = obj && obj.AgendaName || '';
  }
}
