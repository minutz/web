export class Select2Model {
  public id: string;
  public text: string;

  constructor(obj?: Select2Model) {
    this.id = obj && obj.id || '';
    this.text = obj && obj.text || '';
  }
}
