import { UserRolesUser } from '.';

export class UserRole {
    public fullAccessTotal: number;
    public fullAccessActive: number;
    public fullAccessRevoked: number;
    public fullAccessRequests: Array<UserRolesUser>;
    public GuestAccessTotal: number;
    public GuestAccessActive: number;
    public GuestAccessRevoked: number;
    public GuestAccessInvited: number;
    public allActiveUsers: Array<UserRolesUser>;

    constructor(obj?: UserRole) {
        this.fullAccessTotal = obj && obj.fullAccessTotal || null;
        this.fullAccessActive = obj && obj.fullAccessActive || null;
        this.fullAccessRevoked = obj && obj.fullAccessRevoked || null;
        this.fullAccessRequests = obj && obj.fullAccessRequests || null;
        this.GuestAccessTotal = obj && obj.GuestAccessTotal || null;
        this.GuestAccessActive = obj && obj.GuestAccessActive || null;
        this.GuestAccessRevoked = obj && obj.GuestAccessRevoked || null;
        this.GuestAccessInvited = obj && obj.GuestAccessInvited || null;
        this.allActiveUsers = obj && obj.allActiveUsers || null;
    }
}
