export interface IMeetingAttendee {
    id: string;
    instanceId: string;
    personIdentity: string;
    picture: string;
    referenceId: string;
    status: string;
    role: string;
    name: string;
    email: string;
    company: string;
    department: string;
}

export class MeetingAttendee {
    public id: string;
    public instanceId: string;
    public personIdentity: string;
    public picture: string;
    public referenceId: string;
    public status: string;
    public role: string;
    public name: string;
    public email: string;
    public company: string;
    public department: string;

    constructor(obj?: MeetingAttendee) {
        this.id = obj && obj.id || '';
        this.instanceId = obj && obj.instanceId || '';
        this.picture = obj && obj.picture || '';
        this.personIdentity = obj && obj.personIdentity || '';
        this.referenceId = obj && obj.referenceId || '';
        this.status = obj && obj.status || '';
        this.role = obj && obj.role || '';
        this.name = obj && obj.name || '';
        this.email = obj && obj.email || '';
        this.company = obj && obj.company || '';
        this.department = obj && obj.department || '';
    }
}
