import {
  MeetingAction,
  MeetingAgenda,
  MeetingAttachment,
  MeetingAttendee,
  MeetingNote,
  MeetingRecurrence,
  MeetingDecision
} from '.';

export interface IMeeting {
    id: string;
    instanceId: string;
    name: string;
    location: string;
    date: Date;
    duration: number;
    status: string;
    recurrenceType: number;
}

export class MeetingModel {
  public isLocked: boolean;
  public id: string;
  public instanceId: string;
  public meetingOwnerId: string;
  public name: string;
  public location: string;
  public date: Date;
  public updatedDate: Date;
  public time: string;
  public duration: number;
  public isRecurrence: boolean;
  public status: string;
  public recurrenceType: number;
  public reacuranceType: string; // todo: fix the API:  this is a spelling mistake that comes from the api
  public isPrivate: boolean;
  public isFormal: boolean;
  public timeZone: string;
  public timeZoneOffSet: number;
  public tag: Array<string>;
  public availableAttendeeCollection: Array<MeetingAttendee>;
  public meetingAttendeeCollection: Array<MeetingAttendee>;
  public meetingAgendaCollection: Array<MeetingAgenda>;
  public meetingActionCollection: Array<MeetingAction>;
  public meetingAttachmentCollection: Array<MeetingAttachment>;
  public meetingNoteCollection: Array<MeetingNote>;
  public meetingDecisionCollection: Array<MeetingDecision>;
  public purpose: string;
  public outcome: string;
  public recurrence: MeetingRecurrence;


  constructor(obj?: MeetingModel) {
    const today = new Date(new Date());
    this.isLocked = obj && obj.isLocked || false;
    this.id = obj && obj.id || '';
    this.instanceId = obj && obj.instanceId || '';
    this.meetingOwnerId = obj && obj.meetingOwnerId || '';
    this.name = obj && obj.name || '';
    this.location = obj && obj.location || '';
    this.date = obj && new Date(obj.date) || today;
    this.updatedDate = obj && obj.updatedDate || today;
    this.time = obj && obj.time || '';
    this.duration = obj && obj.duration || 0;
    this.isRecurrence = obj && obj.isRecurrence || false;
    this.recurrenceType = obj && obj.recurrenceType || 0;
    this.reacuranceType = obj && obj.reacuranceType || ''; // todo: fix the API:  this is a spelling mistake that comes from the api
    this.isPrivate = obj && obj.isPrivate || false;
    this.status = obj && obj.status || 'create';
    this.isFormal = obj && obj.isFormal || true;
    this.timeZone = obj && obj.timeZone || '';
    this.timeZoneOffSet = obj && obj.timeZoneOffSet || 0;
    this.tag = obj && obj.tag || new Array<string>();
    this.availableAttendeeCollection = obj && obj.availableAttendeeCollection || new Array<MeetingAttendee>();
    this.meetingAttendeeCollection = obj && obj.meetingAttendeeCollection || new Array<MeetingAttendee>();
    this.meetingAgendaCollection = obj && obj.meetingAgendaCollection || new Array<MeetingAgenda>();
    this.meetingActionCollection = obj && obj.meetingActionCollection || new Array<MeetingAction>();
    this.meetingAttachmentCollection = obj && obj.meetingAttachmentCollection || new Array<MeetingAttachment>();
    this.meetingNoteCollection = obj && obj.meetingNoteCollection || new Array<MeetingNote>();
    this.meetingDecisionCollection = obj && obj.meetingDecisionCollection || new Array<MeetingDecision>();
    this.purpose = obj && obj.purpose || '';
    this.outcome = obj && obj.outcome || '';
    this.recurrence = obj && obj.recurrence || null;
  }
}

