export class MeetingDecision {
    public id: string;
    public instanceId: string;
    public personId: string;
    public decisionHeader: string;
    public decisionText: string;
    public referenceId: string;
    public decisionComment: string;
    public agendaId: string;
    public createdDate: Date;
    public isOverturned: boolean;
    public order: number;

    constructor(obj?: MeetingDecision) {
        this.id = obj && obj.id || '';
        this.instanceId = obj && obj.instanceId || '';
        this.referenceId = obj && obj.referenceId || '';
        this.decisionHeader = obj && obj.decisionHeader || '';
        this.decisionText = obj && obj.decisionText || '';
        this.decisionComment = obj && obj.decisionComment || '';
        this.personId = obj && obj.personId || '';
        this.agendaId = obj && obj.agendaId || '';
        this.createdDate = obj && obj.createdDate || new Date();
        this.isOverturned = obj && obj.isOverturned || false;
        this.order  = obj && obj.order || 0;
    }
}
