export class UserProfile {
  public sub: string;
  public nickname: string;
  public firstName: string;
  public lastName: string;
  public picture: string;
  public email: string;
  public role: string;
  public company: string;
  public companyLogo: string;
  public instanceId: string;
  public isVerified: boolean;
  public accessToken: string;
  public tokenExpire: string;
  public idToken: string;

  constructor(obj?: UserProfile) {
    this.sub = obj && obj.sub || '';
    this.nickname = obj && obj.nickname || '';
    this.firstName = obj && obj.firstName || '';
    this.lastName = obj && obj.lastName || '';
    this.picture = obj && obj.picture || '';
    this.company = obj && obj.company || '';
    this.companyLogo = obj && obj.companyLogo || '';
    this.email = obj && obj.email || '';
    this.role = obj && obj.role || '';
    this.instanceId = obj && obj.instanceId || '';
    this.isVerified = obj && obj.isVerified || false;
    this.accessToken = obj && obj.accessToken || '';
    this.tokenExpire = obj && obj.tokenExpire || '';
    this.idToken = obj && obj.idToken || '';
  }
}
