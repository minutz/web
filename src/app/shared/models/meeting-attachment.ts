export interface IMeetingAttachment {
    id: string;
    instanceId: string;
    referenceId: string;
    meetingAttendeeId: string;
    fileName: string;
    fileExtension: string;
    date: Date;
    fileData: any;
    order: number;
}

export class MeetingAttachment {
    public id: string;
    public instanceId: string;
    public referenceId: string;
    public meetingAttendeeId: string;
    public fileName: string;
    public fileExtension: string;
    public date: Date;
    public fileData: any;
    public order: number;

    constructor(obj?: MeetingAttachment) {
        this.id = obj && obj.id || '';
        this.instanceId = obj && obj.instanceId || '';
        this.referenceId = obj && obj.referenceId || '';
        this.meetingAttendeeId = obj && obj.meetingAttendeeId || '';
        this.fileName = obj && obj.fileName || '';
        this.fileExtension = obj && obj.fileExtension || '';
        this.date = obj && obj.date || new Date();
        this.fileData = obj && obj.fileData || '';
        this.order = obj && obj.order || 0;
    }
}
