export interface IMeetingNote {
    id: string;
    referenceId: string;
    noteText: string;
    noteHeader: string;
    meetingAttendeeId: string;
    createdDate: Date;
}

export class MeetingNote {
    public id: string;
    public referenceId: string;
    public instanceId: string;
    public noteHeader: string;
    public noteText: string;
    public meetingAttendeeId: string;
    public createdDate: Date;
    public order: number;

    constructor(obj?: MeetingNote) {
        this.id = obj && obj.id || '';
        this.referenceId = obj && obj.referenceId || '';
        this.instanceId = obj && obj.instanceId || '';
        this.noteText = obj && obj.noteText || '';
        this.noteHeader = obj && obj.noteHeader || '';
        this.meetingAttendeeId = obj && obj.meetingAttendeeId || '';
        this.createdDate = obj && obj.createdDate || new Date();
        this.order = obj && obj.order || 0;
    }
}
