export class LoginModel {
  public username: string;
  public password: string;
  public instanceId: string;
  constructor(obj?: LoginModel) {
    this.username = obj && obj.username || '';
    this.password = obj && obj.password || '';
    this.instanceId = obj && obj.instanceId || '';
  }
}
