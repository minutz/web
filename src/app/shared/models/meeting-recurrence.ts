export class MeetingRecurrence {
  public type: string;
  public number: number;
  public startDate: Date;
  public endDate: Date;
  public recurrenceNumber: number;
  public weekOnWeekDay: number;
  public monthOnWeekDayNumber: number;
  public monthOnDay: string;

  constructor(obj?: MeetingRecurrence) {
    this.type = obj && obj.type || '';
    this.number = obj && obj.number || null;
    this.startDate = obj && obj.startDate || new Date();
    this.endDate = obj && obj.endDate || new Date;
    this.recurrenceNumber = obj && obj.recurrenceNumber || null;
    this.weekOnWeekDay = obj && obj.weekOnWeekDay || null;
    this.monthOnWeekDayNumber = obj && obj.monthOnWeekDayNumber || null;
    this.monthOnDay = obj && obj.monthOnDay || '';
  }
}
