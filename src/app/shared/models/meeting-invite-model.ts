import { InviteAttendees } from '../emums/invite-attendees.enum';

export class MeetingInviteModel {
  public meetingId: string;
  public recipients: InviteAttendees;

  constructor(obj?: MeetingInviteModel) {
    this.meetingId = obj && obj.meetingId || '';
    this.recipients = obj && obj.recipients || null;
  }
}
