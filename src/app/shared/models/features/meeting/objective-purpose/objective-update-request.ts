export class ObjectiveUpdateRequest {
    public id: string;
    public objective: string;
    constructor(obj?: ObjectiveUpdateRequest) {
        this.id = obj && obj.id || '';
        this.objective = obj && obj.objective || '';
    }
}
