export class PurposeUpdateRequest {
    public id: string;
    public purpose: string;
    constructor(obj?: PurposeUpdateRequest) {
        this.id = obj && obj.id || '';
        this.purpose = obj && obj.purpose || '';
    }

}
