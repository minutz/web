export class RecurrenceType {
    id: number;
    value: string;
    constructor(obj?: RecurrenceType) {
        this.id = obj && obj.id || 0;
        this.value = obj && obj.value || '';
    }
}
