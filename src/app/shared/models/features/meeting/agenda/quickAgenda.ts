export class QuickAgenda {
    public meetingId: string;
    public agendaTitle: string;
    public order: number;
    constructor(obj?: QuickAgenda) {
        this.meetingId = obj && obj.meetingId || '';
        this.agendaTitle = obj && obj.agendaTitle || '';
        this.order = obj && obj.order || 0;
    }
}
