export class NewUser {
    public firstName: string;
    public lastName: string;
    // public shortName: string;
    public email: string;
    // public organization: string;
    // public representing: string;
    public status: string;
    public profilePicture: string;
    public role: string;


    constructor(obj?: NewUser) {
        this.firstName = obj && obj.firstName || '';
        this.lastName = obj && obj.lastName || '';
        // this.shortName = obj && obj.shortName || '';
        this.status = obj && obj.email || '';
        this.email = obj && obj.email || '';
        this.profilePicture = obj && obj.profilePicture || '';
        // this.organization = obj && obj.organization || '';
        // this.representing = obj && obj.representing || '';
        this.role = obj && obj.role || '';
    }
}
