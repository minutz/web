export interface IMeetingAction {
    id: string;
    instanceId: string;
    referenceId: string;
    referanceId: string;
    actionTitle: string;
    actionText: string;
    personId: string;
    dueDate: Date;
    createdDate: Date;
    isComplete: boolean;
    type: string;
    order: number;
}

export class MeetingAction {
    public id: string;
    public instanceId: string;
    public referenceId: string;
    public referanceId: string;
    public actionTitle: string;
    public actionText: string;
    public personId: string;
    public dueDate: Date;
    public createdDate: Date;
    public isComplete: boolean;
    public type: string;
    public order: number;

    constructor(obj?: MeetingAction) {
        this.id = obj && obj.id || '';
        this.instanceId = obj && obj.instanceId || '';
        this.referenceId = obj && obj.referenceId || '';
        this.referanceId = obj && obj.referanceId || '';
        this.actionTitle = obj && obj.actionTitle || '';
        this.actionText = obj && obj.actionText || '';
        this.personId = obj && obj.personId || '';
        this.dueDate = obj && obj.dueDate || new Date();
        this.createdDate = obj && obj.createdDate || new Date();
        this.isComplete = obj && obj.isComplete || false;
        this.type = obj && obj.type || '';
        this.order = obj && obj.order || 0;
    }
}
