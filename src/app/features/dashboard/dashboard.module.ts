import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatTooltipModule} from '@angular/material/tooltip';

import {
    MeetingArchieveComponent,
    MeetingListComponent,
    ActionListComponent,
    UserMessageComponent
} from '.';
import {
    MatFormFieldModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule
} from '@angular/material';
import { DashboardNamePipe } from './meeting-list/dashboard-name.pipe';
import { DashboardDurationPipe } from './meeting-list/dashboard-duration.pipe';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        MatInputModule,
        MatIconModule,
        MatProgressBarModule,
        MatCardModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        BrowserModule,
        MatDatepickerModule,
        BrowserAnimationsModule,
        MatTooltipModule
    ],
    exports: [DashboardComponent],
    declarations: [
        DashboardComponent,
        MeetingArchieveComponent,
        MeetingListComponent,
        ActionListComponent,
        UserMessageComponent,
        DashboardNamePipe,
        DashboardDurationPipe
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'en-gb' }
    ]
})
export class DashboardModule { }
