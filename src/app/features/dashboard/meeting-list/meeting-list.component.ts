import {Component, OnInit, ViewChild, AfterViewInit, LOCALE_ID} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';
import { MeetingModel, UserProfile } from '../../../shared/models';
import {
    LoadingService,
    ProfileService,
    MeetingService
} from '../../../shared/services';
import { UserMeetingsService
} from '../../../shared/repositories/features/dashboard';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
    selector: 'app-meeting-list',
    templateUrl: './meeting-list.component.html',
    styleUrls: ['./meeting-list.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({height: '0px', minHeight: '0'})),
            state('expanded', style({height: '*'})),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
    ]
})
export class MeetingListComponent implements OnInit {
    // displayedColumns = ['name', 'meetingOwnerId', 'duration', 'start', 'date', 'status'];
    expandedElement: any | null;
    columnsToDisplay = ['name', 'date', 'duration', 'status'];
    dataSource: MatTableDataSource<MeetingModel>;
    sortedData: MeetingModel[];
    busy: boolean;
    @ViewChild(MatPaginator) userMeetingsPaginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meetings: Array<MeetingModel>;
    public get meetings(): Array<MeetingModel> {
        return this._meetings;
    }

    public set meetings(v: Array<MeetingModel>) {
        this._meetings = v;
    }

    constructor(
        private router: Router,
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _userMeetingsService: UserMeetingsService,
        private _loadingService: LoadingService
    ) {
        this._meetings = [];
        this.sortedData = this._meetings.slice();
        this.dataSource = new MatTableDataSource(this._meetings);
    }

    ngOnInit() {
        this.user = this._profileService.get();
        this.busy = true;
        this._meetingService.meetingsMessageReceived.subscribe((message: any) => {
            this._meetings = [];
            this.meetings = message.data.meetings;
            this.meetings.forEach((meeting: any) => {
                // const t = meeting.time.split(':');
                // if (t[0].length < 2) {
                //     const updatetime = `0${t[0]}`;
                //     meeting.time = `${updatetime}:${t[1]}`;
                // }
                // const split = meeting.date.split('T')[0];
                // const mo = moment(`${split}T${meeting.time}`).locale('en-gb').format('LLL');

                const x = moment(meeting.date).locale('en-gb').format('LLL');
                const date = new Date(x);
                // const nowDate = this._meetingService.toLocalTime(date);

                meeting.date = x; // mo;
            });
            this.busy = false;
            this.dataSource = new MatTableDataSource(this.meetings);
            this.dataSource.sort = this.sort;
            this.sortedData = this._meetings.slice();
            this.dataSource.paginator = this.userMeetingsPaginator;
           });
    }

    rewriteHeader(column: string): string {
        switch (column) {
            case 'name':
                return 'Meeting';
                break;
            case 'date':
                return 'Meeting Date';
                break;
            case 'status':
                return 'Status';
                break;
            case 'meetingOwnerId':
                return 'Owner';
                break;
            case 'duration':
                return 'Duration';
                break;
            default:
                return column;
        }
    }

    rewriteStatus(status: string): string {
        switch (status) {
            case 'stop':
                return 'Paused';
                break;
            case 'invite':
                return 'Invite Sent';
                break;
            case 'agenda':
                return 'Agenda Issued';
                break;
            case 'start':
                return 'Started';
                break;
            case 'run':
                return 'Running';
                break;
            case 'minute':
                return 'Issued';
                break;
            case 'cancel':
                return 'Cancelled';
                break;
            default:
                return status;
        }
    }

    rewriteDate(date: any) {
        return date;
    }

    rewriteOwner(item: string): string {
        const owner =  item.split('@')[0].charAt(0).toUpperCase() + item.split('@')[0].slice(1);
        return `${owner}`;
    }
    rewriteCompany(item: string): string {
        const owner =  item.split('@')[0].charAt(0).toUpperCase() + item.split('@')[0].slice(1);
        return `${owner}'s Organization`;
    }

    rewriteDuration(item: any): string {
        return this.timeConvert(item);
    }


    timeConvert(n) {
        const num = n;
        const hours = (num / 60);
        const rhours = Math.floor(hours);
        const minutes = (hours - rhours) * 60;
        const rminutes = Math.round(minutes);
        if (rminutes === 0) {
            return  rhours + ' h ' + '00 m';
        }
        return  rhours + ' h ' + rminutes + ' m';
    }
    rowEvent(row: any) {
        console.log(`${row.status}`);
        if (row.id !== '') {
            this.router.navigate(['/meeting'],
                {
                    queryParams:
                        {
                            id: row.id,
                            instanceId: row.instanceId,
                            'mode': row.status
                        }
                });
        }
    }

    getMeetings() {
        this._loadingService.loading(true);
        this._userMeetingsService.get(this.user).subscribe(meetings => {
            this._meetings = [];
            this.meetings = meetings;
            this._loadingService.loading(false);
            this.dataSource.paginator = this.userMeetingsPaginator;
            this.dataSource.sort = this.sort;
            this.sortedData = this._meetings.slice();
            this.dataSource = new MatTableDataSource(this.meetings);
        }, error => {
            this._loadingService.loading(false);
        });
    }

    // applyFilter(filterValue: string) {
    //     filterValue = filterValue.trim();
    //     filterValue = filterValue.toLowerCase();
    //     this.dataSource.filter = filterValue;
    //     if (this.dataSource.paginator) {
    //         this.dataSource.paginator.firstPage();
    //     }
    // }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    sortData(sort: Sort) {
        const data = this.meetings.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }

        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'date': return compare(a.date, b.date, isAsc);
                case 'status': return compare(a.status, b.status, isAsc);
                default: return 0;
            }
        });
    }
}
function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
