import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dashboardDuration'
})
export class DashboardDurationPipe implements PipeTransform {

  transform(value: any, args?: any): any {
      const toInt = parseInt(value, 0);
      const now = new Date(0, 0, 0, 0, toInt, 0, 0);
      if (toInt > 60) {
          let hours = 'hour';
          let minutes = 'minute';
          if (now.getHours() > 1) {
              hours = 'hours';
          }
          if (now.getMinutes() > 1) {
              minutes = 'minutes';
          }
         return `${now.getHours()} ${hours} and ${now.getMinutes()} ${minutes}`;
      } else {
          return `${toInt} minutes`;
      }
  }

}
