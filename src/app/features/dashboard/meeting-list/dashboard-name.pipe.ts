import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dashboardName'
})
export class DashboardNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
      const loggedInUserEmail = args.email;
      if (loggedInUserEmail.toUpperCase() === value.toUpperCase()) {
          return '';
      } else {
          return `${value.split('@')[0].replace('.', '')}'s meeting`;
      }
  }

}
