import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {IMeeting} from '../../../shared/models/meeting-model';
import {IMeetingAction, MeetingModel, UserProfile} from '../../../shared/models';
import {MeetingService} from '../../../shared/services/meeting.service';
import {ProfileService} from '../../../shared/services/profile.service';
import {MeetingRepositoryService} from '../../../shared/repositories';
import {UserActionsService} from '../../../shared/repositories/features/dashboard/user-actions/user-actions.service';
import {Action} from 'rxjs/internal/scheduler/Action';
import {MeetingAction} from '../../../shared/models/meeting-action';

@Component({
  selector: 'app-action-list',
  templateUrl: './action-list.component.html',
  styleUrls: ['./action-list.component.scss']
})
export class ActionListComponent implements OnInit {
    displayedColumns = ['raisedDate', 'actionTitle', 'dueDate', 'id'];
    dataSource: MatTableDataSource<IMeetingAction>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _actions: Array<MeetingAction>;
    public get actions(): Array<MeetingAction> {
        return this._actions;
    }

    public set actions(v: Array<MeetingAction>) {
        this._actions = v;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _userActionsService: UserActionsService
    ) {
        this._actions = [];
        this.dataSource = new MatTableDataSource(this._actions);
    }

    ngOnInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.user = this._profileService.get();

        this._meetingService.getActionCollectionMessage.subscribe((message: any) => {
            console.log(message);
        });
    }
    getActions() {
        this._userActionsService.get(this.user).subscribe(a => {
            this._actions = a;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.dataSource = new MatTableDataSource(this._actions);
        });
    }

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
        this.dataSource.filter = filterValue;
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}
