import { Component, OnInit } from '@angular/core';
import { MeetingModel, UserProfile } from '../../shared/models';
import { ProfileService } from '../../shared/services/profile.service';
import { MeetingService } from '../../shared/services/meeting.service';
import { UrlService} from '../../shared/services';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';

const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [{
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
  }, {
      provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS}, {
      provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
  ]
})
export class DashboardComponent implements OnInit {
  feature = 'Dashboard';
  PageTitle: string;
  PageHeading: string;
  instanceId: string;
  meetingId: string;

  date = new FormControl(moment());
  private _user: UserProfile;
  public get user(): UserProfile {
    return this._user;
  }
  public set user(v: UserProfile) {
    this._user = v;
  }
  canSendMessage: boolean;
  constructor(
    private _profileService: ProfileService,
    private _meetingService: MeetingService,
    private _urlService: UrlService,
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
    'calendar',
    sanitizer.bypassSecurityTrustResourceUrl('assets/images/calendar.svg'));
  }

  ngOnInit() {
    this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
    this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
    this._profileService._Profile$.subscribe( profile => {
      this.user = profile;
      this.subscribeToEvents();
    });
    if (!this.user) {
      const profile = this._profileService.get();
      this._profileService.Profile(profile);
    }
  }
  private subscribeToEvents(): void {
    this._meetingService.connectionEstablished.subscribe(() => {
      this.canSendMessage = true;
      this._meetingService.joinGroup(this.user.instanceId);
    });
    this._meetingService.instanceJoinMessage.subscribe((message: any) => {
      console.log(`${message} instance Message`);
      this._meetingService.sendGroupMessage({
        task: 'getmeetings',
        instanceId: this.instanceId,
        meetingId: this.meetingId,
        data: 'get data'
      }, this.user.instanceId);
      this._meetingService.sendGroupMessage({
            task: 'getActionCollection',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: 'get data'
        }, this.user.instanceId);
    });
  }
}
