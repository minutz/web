import { Component, OnInit } from '@angular/core';
import { MeetingModel, UserProfile, MeetingAgenda } from '../../../shared/models';
import { ProfileService } from '../../../shared/services/profile.service';
import { MeetingService } from '../../../shared/services/meeting.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MAT_DATE_FORMATS } from '@angular/material';
import { BreadcrumbService, UrlService } from '../../../shared/services';
import { MeetingAgendaService } from 'src/app/shared/repositories/features/meeting/agenda/meeting-agenda.service';

export const APP_DATE_FORMATS = {
    parse: {
        dateInput: {
            month: 'short',
            year: 'numeric',
            day: 'numeric'
        },
    },
    display: {
        dateInput: {
            month: 'short',
            year: 'numeric',
            day: 'numeric'
        },
        monthYearLabel: {
            year: 'numeric'
        }
    }
};
@Component({
    selector: 'app-meeting-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.scss'],
    providers: [{ provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }]
})
export class OptionsComponent implements OnInit {
    mode: string;
    canEdit: boolean;
    toggleTime = false;
    value = 50;
    instanceId: string;
    meetingId: string;
    private _user: UserProfile;
    agendaCount: number;
    items = [];

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _selectedAgenda: MeetingAgenda;
    public get selectedAgenda(): MeetingAgenda {
        return this._selectedAgenda;
    }

    public set selectedAgenda(v: MeetingAgenda) {
        this._selectedAgenda = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingAgendaService: MeetingAgendaService,
        private _meetingService: MeetingService,
        public _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService,
        private router: Router
    ) {
        this.selectedAgenda = new MeetingAgenda();
        this._meeting = this._meetingService.createEmptyMeeting();
        this.instanceId = '';
        this.meetingId = '';
        this.mode = 'create';
    }

    public ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._user = this._profileService.get();

        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });

        this.router.events.pipe(filter(event => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
            this.mode = this._urlService.getModeQueryString(window.location.href);
        });
    }
}
