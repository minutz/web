import { Component, OnInit } from '@angular/core';
import { MeetingModel, UserProfile } from 'src/app/shared/models';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingLocationService } from 'src/app/shared/repositories/features/meeting/heading/meeting-location.service';
import { BreadcrumbService, UrlService } from 'src/app/shared/services';
import { UserMeetingsService } from 'src/app/shared/repositories/features/dashboard';

@Component({
    selector: 'app-meeting-location',
    templateUrl: './meeting-location.component.html',
    styleUrls: ['./meeting-location.component.scss']
})
export class MeetingLocationComponent implements OnInit {
    mode: string;
    instanceId: string;
    canEdit: boolean;
    meetingId: string;
    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    private _meetingLocation: string;
    public get meetingLocation() {
        return this._meetingLocation;
    }
    public set meetingLocation(value: string) {
        this._meetingLocation = value;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _userMeetingsService: UserMeetingsService,
        private _meetingLocationService: MeetingLocationService,
        public _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService
    ) {
        this._meeting = new MeetingModel();
        this._meeting.location = '';
    }

    ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe(
            (message: any) => {
                this.meeting = message.data.meeting;
                this.meetingLocation = this.meeting.location;
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            }
        );
        this._meetingService.meetingLocationChangeMessage.subscribe( (message: any) => {
            this._meeting.location = message.data.message;
        });
    }
    blur() {
        this._meetingService.sendGroupMessage({
            task: 'meetingLocationChange',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {location: this.meetingLocation }
        }, this.meetingId);
    }
}
