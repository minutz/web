import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingLocationComponent } from './meeting-location.component';

describe('MeetingLocationComponent', () => {
  let component: MeetingLocationComponent;
  let fixture: ComponentFixture<MeetingLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
