import { Component, OnInit } from '@angular/core';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
const moment = _rollupMoment || _moment;

import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { BreadcrumbService, UrlService } from 'src/app/shared/services';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MeetingModel, UserProfile } from 'src/app/shared/models';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-meeting-date',
    templateUrl: './meeting-date.component.html',
    styleUrls: ['./meeting-date.component.scss']
})
export class MeetingDateComponent implements OnInit {
    meetingDatePicker = new FormControl(Date());
    _user: UserProfile;
    _meeting: MeetingModel;
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;

    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this._meeting = new MeetingModel();
    }

    ngOnInit() {
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            if (this._meeting.date) {
                this.meetingDatePicker.setValue(this._meeting.date);
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            }
        });
        this._meetingService.meetingDateChangeMessage.subscribe((message: any) => {
            const updatedDate = new Date(message.data.message);
            this._meeting.date = updatedDate;
            if (this._meeting.date) {
                this.meetingDatePicker.setValue(this._meeting.date);
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            }
        });
    }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        this._meeting.date = event.value;
        const hours = new Date().getHours();
        const minutes = new Date().getMinutes();
        const seconds = new Date().getSeconds();
        const milliseconds = new Date().getMilliseconds();
        this._meeting.date.setHours(hours, minutes, seconds, milliseconds);
        const date = this.meeting.date.toUTCString();
        this._meetingService.sendGroupMessage({
            task: 'meetingDateChange',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {date: date }
        }, this.meetingId);
    }
}
