import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingDateComponent } from './meeting-date.component';

describe('MeetingDateComponent', () => {
  let component: MeetingDateComponent;
  let fixture: ComponentFixture<MeetingDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
