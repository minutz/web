import { Component, OnInit } from '@angular/core';
import { MeetingModel, UserProfile } from 'src/app/shared/models';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingTagsService } from 'src/app/shared/repositories/features/meeting/heading/meeting-tags.service';
import { BreadcrumbService, UrlService } from 'src/app/shared/services';

@Component({
    selector: 'app-meeting-tags',
    templateUrl: './meeting-tags.component.html',
    styleUrls: ['./meeting-tags.component.scss']
})
export class MeetingTagsComponent implements OnInit {
    tag: string;
    mode: string;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    constructor(private _profileService: ProfileService,
                private _meetingService: MeetingService,
                private _meetingTagsService: MeetingTagsService,
                private _breadcrumbService: BreadcrumbService,
                private _urlService: UrlService) {
        this.tag = '';
    }

    ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this._user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            if (this._meeting.tag) {
                this.tag = '';
                if (this._meeting.tag.length > 0) {
                    if (this._meeting.tag.length > 1) {
                        this.tag = this._meeting.tag[0];
                    }
                }
            }
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService.meetingTagsChangeMessage.subscribe((message: any) => {
            if (message.data.tags) {
                this.tag = '';
                if (message.data.tags.length > 0) {
                    if (message.data.tags.length > 1) {
                        this.tag = message.data.tags[0];
                    }
                }
            }
        });
    }
    public textChange($event: any): void {
        const input = $event.target.value;
        this.meeting.instanceId = this.instanceId;
        if (input.length > 0) {
            if (input.includes(',')) {
                const tagArray = $event.target.value.split(',');
                this._meeting.tag = [];
                tagArray.forEach( t => {
                    this._meeting.tag.push(t);
                });
                // this._meetingTagsService.update(this._user, this._meeting).subscribe( result => {
                //     this._meetingService.Meeting(this._meeting, this.user);
                // });
                let tag = '';
                this._meeting.tag.forEach( t => {
                    tag = `${tag},${t}`;
                });
                this._meetingService.sendGroupMessage({
                    task: 'meetingTagChange',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: {tags: tag }
                }, this.meetingId);
            }
            if (input.includes(';')) {
                const tagArray = $event.target.value.split(';');
                this._meeting.tag = [];
                tagArray.forEach( t => {
                    this._meeting.tag.push(t);
                });
                // this._meetingTagsService.update(this._user, this._meeting).subscribe( result => {
                //     this._meetingService.Meeting(this._meeting, this.user);
                // });
                let tag = '';
                this._meeting.tag.forEach( t => {
                    tag = `${tag},${t}`;
                });
                this._meetingService.sendGroupMessage({
                    task: 'meetingTagChange',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: {tags: tag }
                }, this.meetingId);
            } else {
                // this._meeting.tag = [];
                // this._meeting.tag.push(input);
                // this._meetingTagsService.update(this._user, this._meeting).subscribe( result => {
                //     this._meetingService.Meeting(this._meeting, this.user);
                // });
                let tag = '';
                this._meeting.tag.forEach( t => {
                    tag = `${tag},${t}`;
                });
                this._meetingService.sendGroupMessage({
                    task: 'meetingTagChange',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: {tags: tag }
                }, this.meetingId);
            }
        }
    }
}
