import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingTagsComponent } from './meeting-tags.component';

describe('MeetingTagsComponent', () => {
  let component: MeetingTagsComponent;
  let fixture: ComponentFixture<MeetingTagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingTagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
