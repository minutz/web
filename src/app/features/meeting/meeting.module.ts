import { MeetingLocationComponent } from './options/meeting-location/meeting-location.component';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MeetingComponent } from './meeting.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import {
    AngularDraggableModule
} from 'angular2-draggable';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {
    ServicesModule
} from 'src/app/shared/services/services.module';
import {
    NotesPanelComponent,
    MeetingHeaderComponent,
    ActionsPanelComponent,
    AgendaPanelComponent,
    AttachmentPanelComponent,
    AttendeesPanelComponent,
    MeetingPersonPanelComponent,
    MeetingInfoPanelComponent,
    MeetingAttachmentPanelComponent,
    DecisionPanelComponent,
    AttendeesPanelInviteDialogComponent, // Dialogs
    MeetingInvatationDialogComponent,
    MeetingTabsComponent, // Tabs
    MeetingTabAgendaComponent,
    MeetingActionsComponent,
    MeetingDecisionsComponent,
    MeetingNotesComponent,
    MeetingTabObjectiveComponent,
    // MeetingLocationComponent,
    OptionsComponent,
    MeetingTagsComponent,
    MeetingDateComponent
} from '.';
import {
    MatDatepickerModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatNativeDateModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule
} from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';

import {
    MeetingRecurrenceDialogComponent
} from '../../shared/components/meeting-recurrence/meeting-recurrance-dialog/meeting-recurrence-dialog.component';
import {
    MeetingRecurrenceComponent
} from '../../shared/components/meeting-recurrence/meeting-recurrence.component';

import { SharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { Select2Module } from 'ng4-select2';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import {
    NgSelectModule
} from '@ng-select/ng-select';
import { SharedPipesModule } from '../../shared/pipes/shared-pipes/shared-pipes.module';
import { LoadingModule } from 'ngx-loading';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { EditorComponent } from '../../shared/components/editor/editor.component';
import { SpinnerComponent } from '../../shared/components/spinner/spinner.component';
import {
    SelectedAgendaDurationPickerComponent
} from '../../shared/components/selected-agenda-duration-picker/selected-agenda-duration-picker.component';
import { QuillModule } from 'ngx-quill';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DragulaModule } from 'ng2-dragula';
import { DragulaService } from 'ng2-dragula';
import {ClickAnywhereDirective} from './click-anywhere.directive';

@NgModule({
    imports: [
        FormsModule,
        NgSelectModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        // DndModule,
        NgbModule,
        CommonModule,
        ServicesModule,
        MatSnackBarModule, // material modules
        MatFormFieldModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatInputModule,
        MatIconModule,
        MatProgressBarModule,
        QuillModule,
        AngularEditorModule, ///
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatExpansionModule,
        MatButtonModule,
        MatListModule,
        MatDividerModule,
        MatDialogModule,
        MatTooltipModule,
        SharedModule.forRoot(),
        HttpClientModule,
        RouterModule,
        LoadingModule,
        SharedPipesModule,
        DragDropModule,
        Select2Module,
        AngularDraggableModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        EditorModule,
        DragulaModule,
        // AngularFireModule.initializeApp(environment.firebase),
        // AngularFireAuthModule,
        // AngularFireStorageModule,
        PdfViewerModule
    ],
    exports: [
        MeetingLocationComponent,
        MeetingDateComponent,
        SpinnerComponent,
        MeetingRecurrenceComponent,
        MeetingTagsComponent
    ],
    declarations: [
        EditorComponent,
        MeetingLocationComponent,
        SpinnerComponent,
        MeetingInvatationDialogComponent,
        MeetingRecurrenceDialogComponent,
        AttendeesPanelInviteDialogComponent,
        SelectedAgendaDurationPickerComponent,
        MeetingRecurrenceComponent,
        MeetingComponent,
        MeetingHeaderComponent,
        ActionsPanelComponent,
        AgendaPanelComponent,
        AttachmentPanelComponent,
        AttendeesPanelComponent,
        DecisionPanelComponent,
        MeetingAttachmentPanelComponent,
        MeetingInfoPanelComponent,
        MeetingPersonPanelComponent,
        MeetingTabsComponent,
        NotesPanelComponent,
        OptionsComponent,
        MeetingDateComponent,
        MeetingTagsComponent,
        MeetingTabAgendaComponent,
        MeetingTabObjectiveComponent,
        MeetingActionsComponent,
        MeetingDecisionsComponent,
        MeetingNotesComponent,
        ClickAnywhereDirective
    ],
    entryComponents: [
        AttendeesPanelInviteDialogComponent,
        MeetingRecurrenceDialogComponent
    ],
    providers: [DragulaService]
})
export class MeetingModule { }
