import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingInvatationDialogComponent } from './meeting-invatation-dialog.component';

describe('MeetingInvatationDialogComponent', () => {
  let component: MeetingInvatationDialogComponent;
  let fixture: ComponentFixture<MeetingInvatationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingInvatationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingInvatationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
