import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'app-meeting-invatation-dialog',
    templateUrl: './meeting-invatation-dialog.component.html',
    styleUrls: ['./meeting-invatation-dialog.component.css']
})
export class MeetingInvatationDialogComponent implements OnInit {
    toInput: string;
    inviteMessage: string;
    agendaOption: boolean;
    attachmentOption: boolean;

    constructor(
        public dialogRef: MatDialogRef<MeetingInvatationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {
       // const instanceId = this._urlService.getInstanceQueryString(window.location.href);
    }
    onNoClick(): void {
        this.dialogRef.close();
    }
}
