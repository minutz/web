import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
    MeetingAction,
    MeetingModel,
    UserProfile
} from 'src/app/shared/models';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { UrlService } from 'src/app/shared/services/url.service';
import { DomSanitizer } from '@angular/platform-browser';
import { BreadcrumbService } from 'src/app/shared/services';
import { DragulaService } from 'ng2-dragula';

import {
    ActionService
} from '../../../../shared/repositories/features/meeting';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-actions-panel',
    templateUrl: './actions-panel.component.html',
    styleUrls: ['./actions-panel.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class ActionsPanelComponent implements OnInit {
    public quickActionTitle: string;
    public actions: Array<MeetingAction>;
    public selectedAction: MeetingAction;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    busy: boolean;
    Name: string;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _meetingAction: MeetingAction;
    public get meetingAction(): MeetingAction {
        return this._meetingAction;
    }
    public set meetingAction(v: MeetingAction) {
        this._meetingAction = v;
    }


    @Input() Id: string;
    @Input() ActionCollection: Array<MeetingAction>;
    @Output() Action = new EventEmitter<MeetingAction>();
    @Output() quickActionAdd = new EventEmitter<MeetingAction>();
    @Output() SelectedMeetingAction = new EventEmitter();

    constructor(
        private _profileService: ProfileService,
        private dragula: DragulaService,
        private _meetingService: MeetingService,
        private _urlService: UrlService,
        private sanitizer: DomSanitizer,
        public _breadcrumbService: BreadcrumbService,
        private _actionRepository: ActionService
    ) {
        if (!this.meeting) {
            this.meeting = new MeetingModel();
        }
        this.quickActionTitle = '';
        this.instanceId = '';
        this.busy = false;
    }

    public ngOnInit() {
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);

        this._meetingService.meetingMessageReceived
            .subscribe((message: any) => {
                this.meeting = message.data.meeting;
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
                this._meetingService.sendGroupMessage({
                    task: 'getMeetingCollectionActions',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: 'get data'
                  }, this.meetingId);
                this.busy = true;
        });

        this._meetingService.assignStatusActionMessage
            .subscribe((message: any) => {
                const data = message.data;
                this.actions.forEach((i: any) => {
                    if (i.id === data.actionId) {
                        if (message.data.message) {
                            i.status = 'Complete';
                        } else {
                            i.status = 'Pending';
                        }
                    }
                });
                console.log(message);
            });

        this._meetingService.meetingActionCollectionMessageReceived
            .subscribe((message: any) => {
                this.actions = message.data.actions;
                console.log(message.data.actions);
                this.busy = false;
        });

        this._meetingService.addMeetingActionMessage
            .subscribe((message: any) => {
                if (!this.actions) { this.actions = []; }
                const query = this.actions.find(i => i.actionTitle === message.data.action.actionTitle);
                if (!query) {
                    this.actions.push(message.data.action);
                }
                this.busy = false;
                this.SelectAction(message.data.action);
        });

        this._meetingService.deleteMeetingActionMessage
            .subscribe((message: any) => {
                const index = this.actions.findIndex(i => i.id === message.data.message);
                this.actions.splice(index, 1);
                this.busy = false;
        });
    }

    public AddAction() {
        if (!this.actions) {
            this.actions = [];
        }
        const quickAction = new MeetingAction();
        quickAction.id = this._meetingService.DefaultAgendaId;

        if (this.quickActionTitle === '') {
            quickAction.actionTitle = 'Action Title';
        } else {
            quickAction.actionTitle = this.quickActionTitle;
        }

        quickAction.referenceId = this.meetingId;
        quickAction.instanceId = this.instanceId;
        quickAction.actionTitle = this.quickActionTitle;
        const order = this.actions.length + 1;
        this.busy = true;
        this._meetingService.sendGroupMessage({
            task: 'addMeetingAction',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {
                actionTitle: this.quickActionTitle,
                order: order
            }
          }, this.meetingId);
    }

    public SelectAction(action: MeetingAction) {
        this.selectedAction = action;
        this.selectedAction.instanceId = this.instanceId;
        this._meetingService.CurrentAction(action);
    }

    public deleteAction(action: MeetingAction) {
        this.busy = true;
        this._meetingService.sendGroupMessage({
            task: 'deleteMeetingAction',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {
                actionId: action.id
            }
          }, this.meetingId);
    }
}
