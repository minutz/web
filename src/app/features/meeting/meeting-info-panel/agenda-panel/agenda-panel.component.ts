import { SessionStorageService } from './../../../../shared/services/session-storage.service';
import { MatIconRegistry } from '@angular/material';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  FormErrors, MeetingAgenda,
  MeetingModel,
  UserProfile
} from 'src/app/shared/models';
import { ValidationService } from 'src/app/shared/services/validation.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { UrlService } from 'src/app/shared/services/url.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import {
  IMeetingAgenda
} from 'src/app/shared/models/meeting-agenda';
import {
  MeetingAgendaService
} from 'src/app/shared/repositories/features/meeting/agenda/meeting-agenda.service';
import {
  UserMeetingsService
} from 'src/app/shared/repositories/features/dashboard/user-meetings/user-meetings.service';
import { BreadcrumbService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import { DragulaService } from 'ng2-dragula';
import {DomSanitizer} from '@angular/platform-browser';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-agenda-panel',
  templateUrl: './agenda-panel.component.html',
  styleUrls: ['./agenda-panel.component.scss']
})
export class AgendaPanelComponent implements OnInit {

  agendaDisplayedColumns = ['agendaHeading', 'duration', 'id'];
  agendaDataSource: MatTableDataSource<IMeetingAgenda>;
  @ViewChild(MatPaginator) agendaPaginator: MatPaginator;
  @ViewChild(MatSort) agendaSort: MatSort;

  Name: string;
  instanceId: string;
  meetingId: string;
  canEdit: boolean;
  mode: string;
  busy: boolean;
  public agendaForm: FormGroup;
  public formErrors: FormErrors = new FormErrors();
  public selectedAgenda: MeetingAgenda;
  public quickTopicTile: string;
  public referenceId: string;
  public order: number;

  private _agenda: MeetingAgenda;
  public get agenda(): MeetingAgenda {
    return this._agenda;
  }
  public set agenda(v: MeetingAgenda) {
    this._agenda = v;
  }

  private _user: UserProfile;
  public get user(): UserProfile {
    return this._user;
  }
  public set user(v: UserProfile) {
    this._user = v;
  }

  private _meeting: MeetingModel;
  public get meeting(): MeetingModel {
    return this._meeting;
  }
  public set meeting(v: MeetingModel) {
    this._meeting = v;
  }


  editorConfig: AngularEditorConfig = {
      editable: true
  };

  @Input() Id: string;
  @Input() AgendaCollection: Array<MeetingAgenda>;
  @Input() MeetingId: string;
  @Output() Topic = new EventEmitter<MeetingAgenda>();
  @Output() quickTopicAdd = new EventEmitter<MeetingAgenda>();


  constructor(
    public iconRegistry: MatIconRegistry,
    public sanitizer: DomSanitizer,
    private dragula: DragulaService,
    private _fb: FormBuilder,
    private _userMeetingsService: UserMeetingsService,
    private _validationService: ValidationService,
    private _meetingService: MeetingService,
    private _profileService: ProfileService,
    private _meetingAgendaService: MeetingAgendaService,
    private _urlService: UrlService,
    private router: Router,
    public _breadcrumbService: BreadcrumbService,
    public _sessionStorageService: SessionStorageService
  ) {
    this.meeting = new MeetingModel();
    this.meeting.meetingAgendaCollection = [];
    this.quickTopicTile = '';
    this.busy = false;
    this.instanceId = '';
      iconRegistry.addSvgIcon(
          'google-plus-blue',
          sanitizer.bypassSecurityTrustResourceUrl('assets/add.svg'));
  }

  public ngOnInit() {
    this._user = this._profileService.get();
    this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
    this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
    this.mode = this._urlService.getModeQueryString(window.location.href);

    this._meetingService.meetingMessageReceived.subscribe((message: any) => {
      this._meeting = message.data.meeting;
      if (!this._meeting.meetingAgendaCollection) {
        this._meeting.meetingAgendaCollection = [];
        this.busy = true;
        this._meetingService.sendGroupMessage({
            task: 'getMeetingCollectionAgenda',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: 'get data'
          }, this.meetingId);
      }

      this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
      this.editorConfig.editable = this.canEdit;
    });

    this._meetingService.meetingAgendaCollectionMessageReceived.subscribe((message: any) => {
      this._meeting.meetingAgendaCollection = message.data.agendaCollection;
      console.log(`Stamp: ${message.stamp}`);
      console.log(`Owner: ${this._meeting.meetingOwnerId}`);
      console.log(`CanEdit: ${this.canEdit}`);
      if (this.selectedAgenda) {
          this.SelectTopic(this.selectedAgenda);
      }
        this.busy = false;
    });

    this._meetingService.deleteMeetingAgendaMessage.subscribe((message: any) => {
      this._meeting.meetingAgendaCollection.forEach((m: any) => {
          this.busy = false;
          console.log('deleted message');
          if (m.id === message.message) {
              this._meeting.meetingAgendaCollection.splice(m.index, 1);
          }
      });
    });

    this._meetingService.addMeetingAgendaMessage.subscribe((message: any) => {
        const q = this._meeting.meetingAgendaCollection.find( a => a.agendaHeading === message.data.agenda.agendaHeading);
        if (!q) {
            this._meeting.meetingAgendaCollection.push(message.data.agenda);
        }
        this.busy = false;
        this.selectedAgenda = message.data.agenda;
        this._meetingService.CurrentAgenda(message.data.agenda);
    });
  }

  public AddTopic() {
    if (!this.AgendaCollection) {
      this.AgendaCollection = [];
    }
    const quickTopic = new MeetingAgenda();
    quickTopic.id = this._meetingService.DefaultAgendaId;

    if (this.quickTopicTile === '') {
      quickTopic.agendaHeading = 'Agenda Topic Title';
    } else {
      quickTopic.agendaHeading = this.quickTopicTile;
    }
    quickTopic.instanceId = this.instanceId;
    quickTopic.agendaText = '';
    quickTopic.createdDate = new Date();
    quickTopic.duration = 60;
    quickTopic.meetingAttendeeId = this.user.email;
    quickTopic.referenceId = this.meeting.id;
    quickTopic.order = this._meeting.meetingAgendaCollection.length + 1;
    this.quickTopicTile = '';
    this.busy = true;
    this._meetingService.sendGroupMessage({
      task: 'addMeetingAgenda',
      instanceId: this.instanceId,
      meetingId: this.meetingId,
      data: {agenda: quickTopic}
    }, this.meetingId);
  }

  public DeleteTopic(agenda: MeetingAgenda, index: number) {
      this.busy = true;
      this._meetingService.sendGroupMessage({
      task: 'deleteMeetingAgenda',
      instanceId: this.instanceId,
      meetingId: this.meetingId,
      data: {agendaId: agenda.id}
    }, this.meetingId);

    this._meeting.meetingAgendaCollection.splice(index, 1);
  }

  public SelectTopic(agenda: MeetingAgenda) {
    this.selectedAgenda = agenda;
    this.selectedAgenda.instanceId = this.instanceId;
    this._meetingService.CurrentAgenda(agenda);
  }
}
