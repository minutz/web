import {
    Component,
    EventEmitter,
    OnInit,
    Output,
    Inject
} from '@angular/core';
import {
    MeetingAttendee,
    MeetingModel,
    UserProfile
} from 'src/app/shared/models';
import {
    NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import {
    MeetingAttendeeRepositoryService, MeetingRepositoryService,
    UserRepositoryService,
} from 'src/app/shared/repositories';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { StatusService } from 'src/app/shared/services/status.service';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';
import {
    MeetingService
} from 'src/app/shared/services/meeting.service';
import { UUID } from 'angular2-uuid';
import {
    MAT_DIALOG_DATA,
    MatDialog,
    MatDialogRef
} from '@angular/material';
import {
    MeetingAttendeeService
} from 'src/app/shared/repositories/features/meeting/attendee/meeting-attendee.service';
import {
    MeetingAvailableAttendeeService
} from 'src/app/shared/repositories/features/meeting/attendee/meeting-available-atendee.service';
import { UrlService, BreadcrumbService } from 'src/app/shared/services';
import { Router } from '@angular/router';
import {environment} from '../../../../../environments/environment';

@Component({
    selector: 'app-attendees-panel',
    templateUrl: './attendees-panel.component.html',
    styleUrls: ['./attendees-panel.component.scss']
})
export class AttendeesPanelComponent implements OnInit {
    public availableAttendees: Array<MeetingAttendee>;
    public options: Select2Options;
    availableAttendeesData: any;
    meetingId: string;
    meetingAttendees: any;
    selectedAttendee: any;
    mode: string;
    instanceId: string;
    canEdit: boolean;
    hasCalled: boolean;
    busy: boolean;


    public isCollapsed = false;
    public modalReference: any;
    public closeResult: string;
    public InviteeName: string;
    public InviteeEmail: string;
    public StatusCollection: Array<string>;
    public AvailableAttendees: Array<any>;
    public startValue: string;
    public selected: string;

    private _user: UserProfile;
    referenceId: string;
    data: any;
    emailAttendee: any;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    @Output() SelectedAttendee = new EventEmitter();

    constructor(
        private _meetingAttendeeRepository: MeetingAttendeeRepositoryService,
        private _meetingAttendeeService: MeetingAttendeeService,
        private _userRepository: UserRepositoryService,
        private modalService: NgbModal,
        private _profileService: ProfileService,
        private _statusService: StatusService,
        private _sessionStorageService: SessionStorageService,
        private _meetingService: MeetingService,
        public inviteDialog: MatDialog,
        private _urlService: UrlService,
        private router: Router,
        private _meetingAvailableAttendeeService: MeetingAvailableAttendeeService,
        private _meetingRepositoryService: MeetingRepositoryService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this.availableAttendees = [];
        this.meetingAttendees = [];
        this.instanceId = '';
        this.hasCalled = false;
        this.busy = false;
    }

    public ngOnInit() {
        this.hasCalled = false;

        this.meetingAttendees = [];
        this.availableAttendees = [];
        this._user = this._profileService.get();
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.StatusCollection = this._statusService.MeetingAttendeeStatusCollection;

        this._meetingService.addMeetingInviteAttendeeMessage.subscribe( (message: any) => {
            const query = this.availableAttendees.find(x => x.email === message.data.email);
            if (!query) {
                if (!message.data.picture) {
                    message.data.picture = 'default';
                }
                if (!message.data.instanceId) {
                    message.data.instanceId = this.instanceId;
                }
                if (message.data.referenceId === '00000000-0000-0000-0000-000000000000') {
                    message.data.referenceId = this.meetingId;
                }
                if (!message.data.name) {
                    message.data.name = message.data.email.split('@')[0].replace('.', ' ');
                }
                const url = `${environment.notifyApi}`;
                this._meetingRepositoryService.notify(
                    this.user,
                    this.meetingId,
                    this.instanceId,
                    url,
                    message.data.name,
                    message.data.email,
                    'Invite').subscribe( (result: any) => {
                });
                this.availableAttendees.push(message.data);
                this.availableAttendeeSelectedEvent(message.data);
            }
        });

        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });

        this._meetingService.meetingAvailableCollectionMessage.subscribe((message: any) => {
            this.availableAttendees = [];
            message.data.attendees.forEach( (m: any) => {
                if (!m.picture) {
                    m.picture = 'default';
                }
                if (!m.instanceId) {
                    m.instanceId = this.instanceId;
                }
                if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                    m.referenceId = this.meetingId;
                }
                const attendee = this.meetingAttendees.find(x => x.email === m.email);
                if(!attendee) {
                    this.availableAttendees.push(m);
                }
            });
        });

        this._meetingService.addMeetingAttendeeMessage
            .subscribe((message: any) => {
                if (!message.data.attendee.picture) {
                    message.data.attendee.picture = 'default';
                }
                if (message.data.attendee.referenceId === '00000000-0000-0000-0000-000000000000') {
                    message.data.attendee.referenceId = this.meetingId;
                }
                if (!message.data.attendee.name) {
                    message.data.attendee.name = message.data.attendee.email.split('@')[0].replace('.', ' ');
                }
                // const query = this.availableAttendees.find
                const index = this.availableAttendees.findIndex(x => x.email === message.data.attendee.email);
                this.availableAttendees.splice(index, 1);
                this.busy = false;
                this.meetingAttendees.push(message.data.attendee);
        });

        this._meetingService.meetingattendeeCollectionMessage
            .subscribe((message: any) => {
                this.meetingAttendees = [];
                message.data.attendees.forEach((m: any) => {
                    if (!m.picture) {
                        m.picture = 'default';
                    }
                    if (!m.instanceId) {
                        m.instanceId = this.instanceId;
                    }
                    if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                        m.referenceId = this.meetingId;
                    }
                    if (!m.name) {
                        m.name = m.email.split('@')[0].replace('.', ' ');
                    }
                    if (this.availableAttendees) {
                        const index = this.availableAttendees.findIndex(x => x.email === m.email);
                        if (index) {
                            this.availableAttendees.splice(index, 1);
                        }
                    }

                    this.meetingAttendees.push(m);
                    this._meetingService.CurrentAttendee(m);
                });
                this.busy = false;
        });

        this._meetingService.meetingattendeeDeleteMessage
            .subscribe((message: any) => {
                const index = this.meetingAttendees.findIndex(x => x.email === message.data.message);
                this.meetingAttendees.splice(index, 1);
        });

        this.busy = true;
        this._meetingService.sendGroupMessage({
            task: 'getMeetingAvailableCollection',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {}
          }, this.instanceId);

        this._meetingService.sendGroupMessage({
            task: 'getMeetingAttendeeCollection',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {}
        }, this.instanceId);
    }

    public availableAttendeeSelectedEvent($event: any) {
        console.log($event);
        if ($event) {
            if (this.meetingAttendees) {
                const query = this.meetingAttendees.find(x => x.email === $event.email);
                if (!query) {
                    const att = new MeetingAttendee();
                    att.id = UUID.UUID();
                    att.email = $event.email;
                    att.picture = 'default';
                    att.name = name;
                    att.personIdentity = 'Invited';
                    att.referenceId = this.meetingId;
                    att.instanceId = this.instanceId;
                    att.status = 'Pending';
                    att.role = 'Invited';
                    this._meetingService.sendGroupMessage({
                        task: 'addMeetingAttendee',
                        instanceId: this.instanceId,
                        meetingId: this.meetingId,
                        data: { attendee: att }
                      }, this.instanceId);
                }
            }
        }
    }

    public refreshAvailableAttendees(): void {
        this._meetingAvailableAttendeeService.get(
            this._user,
            this.instanceId).subscribe(
                att => {
                    this.availableAttendees = [];
                    att.forEach(m => {
                        if (!m.picture) {
                            m.picture = 'default';
                        }
                        if (!m.instanceId) {
                            m.instanceId = this.instanceId;
                        }
                        if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                            m.referenceId = this.meetingId;
                        }
                        this.availableAttendees.push(m);
                    });
                    // this.hasCalled = true;
                });
    }

    public refreshAttendees(): void {
        this._meetingAttendeeService.getCollection(
            this._user,
            this.meetingId,
            this.instanceId).subscribe(a => {
                this.meetingAttendees = [];
                a.forEach(m => {
                    if (!m.picture) {
                        m.picture = 'default';
                    }
                    if (!m.instanceId) {
                        m.instanceId = this.instanceId;
                    }
                    if (m.referenceId === '00000000-0000-0000-0000-000000000000') {
                        m.referenceId = this.meetingId;
                    }
                    if (!m.name) {
                        m.name = m.email.split('@')[0].replace('.', ' ');
                    }
                    this.meetingAttendees.push(m);
                    this._meetingService.CurrentAttendee(m);
                });
        });
    }

    public SelectAttendee(attendee: MeetingAttendee) {
        this.selectedAttendee = attendee;
        this.selectedAttendee.instanceId = this.instanceId;
        this._meetingService.CurrentAttendee(this.selectedAttendee);
    }

    public selectUser($event) {
        this._meetingService.CurrentAttendee($event);
    }

    openDialog(): void {
        const dialogRef = this.inviteDialog.open(AttendeesPanelInviteDialogComponent, {
            width: '450px',
            data: {
                name: this.InviteeName,
                email: this.InviteeEmail,
                instanceId: this.instanceId,
                meetingId: this.meetingId
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result.InviteeEmail) {
                // const availableAttendee = this.availableAttendees.find(x => x.email === result.InviteeEmail);
                if (this.meetingAttendees) {
                    const query = this.meetingAttendees.find(x => x.email === result.InviteeEmail);
                    if (!query) {
                        this.InviteUser(result.InviteeName, result.InviteeEmail);
                    } else {
                        alert('oops, user already exists');
                    }
                }
            }
        });
    }

    public deleteAttendee(attendee: MeetingAttendee, index: number) {
        this.selectedAttendee = attendee;
        this._meetingService.CurrentAttendee(this.selectedAttendee);
        this.referenceId = this.meeting.id;

        this.data = JSON.parse(sessionStorage.getItem('minutz'));
        this.emailAttendee = this.data.email;
        this._meetingService.sendGroupMessage({
            task: 'deleteMeetingAttendee',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {email: attendee.email}
          }, this.instanceId);
    }

    public attendeeSelected(e: any): void {
        if (this.meeting) {
            if (this.meeting.meetingAttendeeCollection) {
                let hasAttendee = false;
                this.meeting.meetingAttendeeCollection.forEach(attendee => {
                    if (attendee.id === e.id) {
                        hasAttendee = true;
                    }
                });
                if (!hasAttendee) {
                    this.meeting.meetingAttendeeCollection.push(e);
                }
            }
        }
    }

    public InviteUser(name: string, email: string) {

        const att = new MeetingAttendee();
        att.id = UUID.UUID();
        att.email = email;
        att.picture = 'default';
        att.name = name;
        att.personIdentity = 'Invited';
        att.referenceId = this.meetingId;
        att.instanceId = this.instanceId;
        att.status = 'Pending';
        att.role = 'Invited';
        this.busy = true;
        this._meetingService.sendGroupMessage({
            task: 'addMeetingAttendeeInvite',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {attendee : att}
          }, this.instanceId);
    }

    public removeAttendee(attendee: MeetingAttendee) {
        const index = this._meeting.meetingAttendeeCollection.findIndex(x => x.id === attendee.id);
        if (index > -1) {
            this._meeting.meetingAttendeeCollection.splice(index, 1);
        }
    }

    public closeModal(): void {
        this.modalReference.close();
    }

    public dismissModal(): void {
        this.modalReference.dismiss();
    }
}

@Component({
    selector: 'app-attendee-invite-dialog',
    templateUrl: 'attendees-panel-invite-dialog.component.html',
})
export class AttendeesPanelInviteDialogComponent {

    constructor(
        public dialogRef: MatDialogRef<AttendeesPanelInviteDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
