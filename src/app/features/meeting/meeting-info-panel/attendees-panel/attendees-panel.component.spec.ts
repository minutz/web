import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendeesPanelComponent } from './attendees-panel.component';

describe('AttendeesPanelComponent', () => {
  let component: AttendeesPanelComponent;
  let fixture: ComponentFixture<AttendeesPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendeesPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendeesPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
