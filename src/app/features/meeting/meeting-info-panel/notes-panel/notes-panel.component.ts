import { Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
    MeetingModel,
    MeetingNote,
    UserProfile
} from 'src/app/shared/models';
import { Select2OptionData } from 'ng4-select2';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ValidationService } from 'src/app/shared/services/validation.service';
import { UrlService } from 'src/app/shared/services/url.service';
import { BreadcrumbService } from 'src/app/shared/services';
import { DragulaService } from 'ng2-dragula';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';


@Component({
    selector: 'app-notes-panel',
    templateUrl: './notes-panel.component.html',
    styleUrls: ['./notes-panel.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class NotesPanelComponent implements OnInit {
    quickNoteTitle: string;
    notes: Array<MeetingNote>;
    selectedNote: MeetingNote;
    Name: string;
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;
    busy: boolean;

    public StatusCollection = new Array<Select2OptionData>();

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    @Input() Id: string;
    @Input() NoteCollection: Array<MeetingNote>;
    @Input() MeetingId: string;
    @Output() Note = new EventEmitter<MeetingNote>();
    @Output() quickNoteAdd = new EventEmitter<MeetingNote>();
    @Output() SelectedAttendee = new EventEmitter();

    constructor(
        private _profileService: ProfileService,
        private dragula: DragulaService,
        private _meetingService: MeetingService,
        private _fb: FormBuilder,
        private _validationService: ValidationService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this.instanceId = '';
        this.StatusCollection = [];
        this.StatusCollection.push({ id: '1', text: 'complete', disabled: false });
        this.StatusCollection.push({ id: '1', text: 'open', disabled: false });
        if (!this.meeting) {
            this.meeting = new MeetingModel();
            this.meeting.meetingNoteCollection = [];
        }
        this.quickNoteTitle = '';
    }

    public ngOnInit() {
        this.busy = false;
        this.user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);

        this._meetingService.meetingMessageReceived
            .subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            this._meetingService.sendGroupMessage({
                task: 'getMeetingCollectionNote',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: 'get data'
            }, this.meetingId);
            this.busy = true;
        });

        this._meetingService.meetingNoteCollectionMessageReceived
            .subscribe((message: any) => {
              this.notes = [];
              if (message.data.noteCollection) {
                  if (message.data.noteCollection.length > 0) {
                      message.data.noteCollection.forEach( n => {
                          this.notes.push(n);
                      });
                  }
              }
              this.busy = false;
        });

        this._meetingService.addMeetingNoteMessage
            .subscribe((message: any) => {
            const q = this.notes.find( a => a.id === message.data.note.id);
            if (!q) {
                this.notes.push(message.data.note);
            }
            this._meetingService.CurrentNote(message.data.note);
            this.busy = false;
        });

        this._meetingService.deleteMeetingNoteMessage
            .subscribe((message: any) => {
            this.notes.forEach((m: any) => {
                const i = this.notes.indexOf(m);
                if (m.id === message.data.message) {
                    this.notes.splice(i, 1);
                }
            });
            this.busy = false;
        });

        this._meetingService._CurrentNote$.subscribe(note => {
            this.selectedNote = note;
        });
    }

    public AddNote() {
        if (!this.NoteCollection) {
            this.NoteCollection = [];
        }
        const quickNote = new MeetingNote();
        quickNote.id = this._meetingService.DefaultAgendaId;
        quickNote.referenceId = this.meetingId;
        quickNote.instanceId = this.instanceId;
        quickNote.noteHeader = this.quickNoteTitle;
        quickNote.noteText = this.quickNoteTitle;
        if (this.notes) {
            quickNote.order = this.notes.length + 1;
        } else {
            this.notes = [];
            quickNote.order = 1;
        }
        this._meetingService.sendGroupMessage({
            task: 'addMeetingNote',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { note: quickNote }
        }, this.meetingId);
        this.busy = true;
    }

    public SelectNote(note: MeetingNote) {
        this.selectedNote = note;
        this._meetingService.CurrentNote(note);
    }

    public removeNoteItem(note: MeetingNote) {
        this._meetingService.sendGroupMessage({
            task: 'deleteMeetingNote',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { noteId: note.id }
        }, this.meetingId);
        this.busy = true;
    }
}
