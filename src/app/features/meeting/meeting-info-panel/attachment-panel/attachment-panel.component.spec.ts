import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentPanelComponent } from './attachment-panel.component';

describe('AttachmentPanelComponent', () => {
  let component: AttachmentPanelComponent;
  let fixture: ComponentFixture<AttachmentPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
