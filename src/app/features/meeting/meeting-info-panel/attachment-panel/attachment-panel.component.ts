import { Component, EventEmitter, OnInit, Output, ViewChild, Input, InjectionToken } from '@angular/core';
import { MeetingAttachment, MeetingModel, UserProfile } from 'src/app/shared/models';
import { UrlService } from 'src/app/shared/services/url.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { SessionStorageService } from 'src/app/shared/services/session-storage.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { IMeetingAttachment } from 'src/app/shared/models/meeting-attachment';
import { Select2OptionData } from 'ng4-select2';
import { Observable } from 'rxjs';
import { BreadcrumbService } from 'src/app/shared/services';
import { MeetingAttachmentService } from 'src/app/shared/repositories/features/meeting';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-attachment-panel',
    templateUrl: './attachment-panel.component.html',
    styleUrls: ['./attachment-panel.component.scss']
})
export class AttachmentPanelComponent implements OnInit {
    @Input() param = 'file';
    @Input() target = 'https://file.io';

    uploadPercent: Observable<number>;
    downloadURL: Observable<string>;
    attachmentDisplayedColumns = ['fileName', 'date', 'isComplete', 'id'];
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;
    attachments: Array<MeetingAttachment> = [];
    listFiles = [];


    public selectedAttachment: MeetingAttachment;

    attachmentDataSource: MatTableDataSource<IMeetingAttachment>;
    @ViewChild(MatPaginator) attachmentPaginator: MatPaginator;
    @ViewChild(MatSort) attachmentSort: MatSort;
    public StatusCollection = new Array<Select2OptionData>();
    icon = 'images/fileicon.png';

    _person: UserProfile;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _alive: boolean;
    public get alive(): boolean {
        return this._alive;
    }

    public set alive(v: boolean) {
        this._alive = v;
    }

    @Output() SelectedAttachment = new EventEmitter();

    constructor(
        private _http: HttpClient,
        private _meetingService: MeetingService,
        private _profileService: ProfileService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService,
        private _meetingAttachmentService: MeetingAttachmentService,
        public snackBar: MatSnackBar
    ) {
        if (!this.meeting) {
            this.meeting = new MeetingModel();
            this.meeting.meetingAttachmentCollection = [];
            this.attachmentDataSource = new MatTableDataSource(this.meeting.meetingAttachmentCollection);
        }
        this.StatusCollection = [];
        this.StatusCollection.push({ id: '1', text: 'complete', disabled: false });
        this.StatusCollection.push({ id: '1', text: 'open', disabled: false });
    }

    ngOnInit() {
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._user = this._profileService.get();
        this.refresh();
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
    }

    uploadFile() {
        const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;

        const formData = new FormData();
        formData.append('file', fileUpload.files[0]);
        const order = (this.attachments.length) + 1;

        const fileInfo = fileUpload.files[0];
        const fileInfoSplit = fileInfo.name.split('.');
        const extension = fileInfoSplit[fileInfoSplit.length - 1];
        this._meetingService.CurrentAttachment({ name: fileInfo.name, src: '', fileExtension: extension });
        this._meetingAttachmentService.upload(
            this._user,
            this.meetingId,
            this.instanceId,
            order,
            formData).subscribe(f => {
                if (f.condition === false) {
                    console.log(f.message);
                    this.snackBar.open(f.message, 'open', {
                        duration: 2000,
                    });
                } else {
                    const fileExtension = f.fileExtension;
                    const fullNameAndLink = f.fileName.split('|');
                    this.listFiles.push({ name: fullNameAndLink[0], src: fullNameAndLink[1], fileExtension: fileExtension });
                    const attachment = new MeetingAttachment({
                        date: new Date(),
                        instanceId: this.instanceId,
                        referenceId: this.meetingId,
                        fileName: fullNameAndLink[0],
                        fileExtension: fileExtension,
                        meetingAttendeeId: '',
                        order: order,
                        fileData: '',
                        id: ''
                    });
                    this._meetingService.CurrentAttachment(attachment);
                    this.selectedAttachment = attachment;
                }
                });
    }

    public SelectAttachment($event) {
        this.selectedAttachment = $event;
        console.log($event);
        this._meetingService.CurrentAttachment($event);
    }

    DeleteFile($event) {
        this._meetingAttachmentService.delete(
            this._user,
            this.meetingId,
            this.instanceId,
            $event.name).subscribe(collection => {
                this.listFiles.forEach(att => {
                    if (att.name === $event.name) {
                        this.listFiles.splice(this.listFiles.indexOf(att.name), 1);
                    }
                });
        });
    }

    refresh(): void {
        this.attachments = [];
        this._meetingAttachmentService.getCollection(
            this._user,
            this.meetingId,
            this.instanceId)
            .subscribe(collection => {
                this.listFiles = [];
                collection.forEach(att => {
                    const attachmentName = att.fileName.split('|')[0];
                    const storageUrl = att.fileName.split('|')[1];
                    this.listFiles.push({ name: attachmentName, src: storageUrl});
                });
        });
    }
}
