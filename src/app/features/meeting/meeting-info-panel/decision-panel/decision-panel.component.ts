import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output} from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
import {
    FormErrors,
    MeetingDecision,
    MeetingModel,
    UserProfile
} from 'src/app/shared/models';
import { Select2OptionData } from 'ng4-select2';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ValidationService } from 'src/app/shared/services/validation.service';
import { UrlService } from 'src/app/shared/services/url.service';
import { BreadcrumbService } from 'src/app/shared/services';
import { DragulaService } from 'ng2-dragula';

@Component({
    selector: 'app-decision-panel',
    templateUrl: './decision-panel.component.html',
    styleUrls: ['./decision-panel.component.scss']
})
export class DecisionPanelComponent implements OnInit {

    public quickDecisionTitle: string;
    public decisions: Array<MeetingDecision>;
    public selectedDecision: MeetingDecision;
    Name: string;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    busy: boolean;
    public StatusCollection = new Array<Select2OptionData>();

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    @Input() Id: string;
    @Input() DecisionCollection: Array<MeetingDecision>;
    @Input() MeetingId: string;
    @Output() Decision = new EventEmitter<MeetingDecision>();
    @Output() quickDecisionAdd = new EventEmitter<MeetingDecision>();
    @Output() SelectedAttendee = new EventEmitter();

    constructor(
        private _profileService: ProfileService,
        private dragula: DragulaService,
        private _meetingService: MeetingService,
        private _fb: FormBuilder,
        private _validationService: ValidationService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this.StatusCollection = [];
        this.StatusCollection.push({ id: '1', text: 'complete', disabled: false });
        this.StatusCollection.push({ id: '1', text: 'open', disabled: false });
        if (!this.meeting) {
            this.meeting = new MeetingModel();
            this.meeting.meetingDecisionCollection = [];
        }
        this.quickDecisionTitle = '';
        this.instanceId = '';
    }

    public ngOnInit() {
        this.busy = false;
        this.user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);

        this._meetingService.meetingMessageReceived
            .subscribe((message: any) => {
                this._meeting = message.data.meeting;
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
                this.decisions = [];
                this._meetingService.sendGroupMessage({
                    task: 'getMeetingCollectionDecision',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: 'get data'
                  }, this.meetingId);
                this.busy = true;
        });

        this._meetingService._CurrentDecision$
            .subscribe(decision => {
                this.selectedDecision = decision;
        });

        this._meetingService.meetingDecisionCollectionMessage
            .subscribe((message: any) => {
                this.decisions = [];
                this.decisions = message.data.decisionCollection;
                this.busy = false;
        });

        this._meetingService.addMeetingDecisionMessage
            .subscribe((message: any) => {
                if (!this.decisions) {
                    this.decisions = [];
                }
                if (this.decisions.indexOf(message.data.decision.decisionText) !== -1) {
                } else {
                    this.decisions.push(message.data.decision);
                }
                this._meetingService.CurrentDecision(message.data.decision);
                this.busy = false;
        });

        this._meetingService.deleteMeetingDecisionMessage
            .subscribe((message: any) => {
                this.decisions.forEach((m: any) => {
                    const i = this.decisions.indexOf(m);
                    if (m.id === message.data.message) {
                        this.decisions.splice(i, 1);
                    }
                });
                this.busy = false;
        });

    }

    public AddDecision() {
        if (!this.DecisionCollection) {
            this.DecisionCollection = [];
        }
        const quickDecision = new MeetingDecision();
        quickDecision.id = this._meetingService.DefaultAgendaId;
        quickDecision.decisionHeader = this.quickDecisionTitle;
        quickDecision.referenceId = this.meetingId;
        quickDecision.instanceId = this.instanceId;
        quickDecision.decisionText = this.quickDecisionTitle;
        if (this.decisions) {
            quickDecision.order = this.decisions.length;
        } else {
            quickDecision.order = 1;
            this.decisions = [];
        }
        this._meetingService.sendGroupMessage({
            task: 'addMeetingDecision',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { decision: quickDecision }
        }, this.meetingId);
        this.busy = true;
    }

    public SelectDecision(decision: MeetingDecision) {
        this.selectedDecision = decision;
        this._meetingService.CurrentDecision(decision);
    }

    public removeDecisionItem(decision: MeetingDecision) {
        this._meetingService.sendGroupMessage({
            task: 'deleteMeetingDecision',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { decisionId: decision.id }
        }, this.meetingId);
        this.busy = true;
    }

}
