import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MeetingAction,
    MeetingAgenda,
    MeetingDecision,
    MeetingModel,
    MeetingNote,
    UserProfile} from 'src/app/shared/models';
import {
    MeetingActionRepositoryService,
    MeetingAgendaRepositoryService,
    MeetingDecisionRepositoryService,
    UserRepositoryService
} from 'src/app/shared/repositories';
import {NotificationsService} from 'src/app/shared/services/notifications.service';
import {ProfileService} from 'src/app/shared/services/profile.service';
import {MeetingService} from 'src/app/shared/services/meeting.service';
import {TemplateService} from 'src/app/shared/services/template.service';
import {SessionStorageService} from 'src/app/shared/services/session-storage.service';
import {BreadcrumbService, UrlService} from 'src/app/shared/services';
import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';

@Component({
    selector: 'app-meeting-info-panel',
    templateUrl: './meeting-info-panel.component.html',
    styleUrls: ['./meeting-info-panel.component.scss']
})
export class MeetingInfoPanelComponent implements OnInit {
    @Output() agendaSelected = new EventEmitter();
    @Output() actionSelected = new EventEmitter();
    @Output() decisionSelected = new EventEmitter();
    @Output() attendeeSelected = new EventEmitter();
    @Output() noteSelected = new EventEmitter();
    @Output() selectedPanel = new EventEmitter();
    @Output() templateSelectedPanel = new EventEmitter();
    _panel: string;
    public MeetingId: string;
    templateItems: Array<any> = [];
    mode: string;
    canEdit: boolean;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this.MeetingId = v.id;
        this._meeting = v;
    }

    constructor(
        private _meetingAgendaRepo: MeetingAgendaRepositoryService,
        private _notificationService: NotificationsService,
        private _userRepository: UserRepositoryService,
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _templateService: TemplateService,
        private _actionRepository: MeetingActionRepositoryService,
        private _decisionRepository: MeetingDecisionRepositoryService,
        private _sessionStorageService: SessionStorageService,
        public _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService,
        private config: NgbAccordionConfig
    ) {
        this.config.closeOthers = true;
        this.templateItems = this._templateService.MeetingAgendaTemplateItemsCollection;
    }

    ngOnInit() {
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService._Panel$.subscribe(panel => {
            this._panel = panel;
        });
    }

    public uploadedfile($event): void {
        console.log($event);
    }

    public newAgenda(agenda: MeetingAgenda) {
        agenda.referenceId = this.meeting.id;
        this._meetingAgendaRepo.createAgenda(this.user, agenda)
            .subscribe((createdAgenda: MeetingAgenda) => {
                this.meeting.meetingAgendaCollection.push(createdAgenda);
            }, (err) => {
                // const userMessage: Notification = new Notification({
                //   type: 'Error',
                //   message: err
                // });
                // this._notificationService.add(new Notification(userMessage));
            });
    }

    public newAction(action: MeetingAction) {
        action.referenceId = this.meeting.id;
        this._actionRepository.createMeetingAction(this.user, action)
            .subscribe((createdAction: MeetingAction) => {
                this.meeting.meetingActionCollection.push(createdAction);
            }, (err) => {
                // const userMessage: Notification = new Notification({
                //   type: 'Error',
                //   message: err
                // });
                // this._notificationService.add(new Notification(userMessage));
            });
    }

    public newDecision(decision: MeetingDecision) {
        this._decisionRepository.createMeetingDecision(this.user, decision)
            .subscribe((createdDecision: MeetingDecision) => {
                this.meeting.meetingDecisionCollection.push(createdDecision);
            }, (err) => {
                // const userMessage: Notification = new Notification({
                //   type: 'Error',
                //   message: err
                // });
                // this._notificationService.add(new Notification(userMessage));
            });
    }

    public SelectTemplate(template: any): void {
        const temp = this._meeting.outcome + '\n' + template.template;
        this.meeting.outcome = temp;
        this._meetingService.Meeting(this._meeting, this.user);
        this.templateSelectedPanel.emit(template.template);
        // this._meetingService.updateMeeting(this._meeting);
    }

    public selectedTopic(agenda: MeetingAgenda): void {
        this.agendaSelected.emit(agenda);
    }

    public selectedAction(action: MeetingAction): void {
        this.actionSelected.emit(action);
    }

    public selectedDecision(decision: MeetingDecision): void {
        this.decisionSelected.emit(decision);
    }

    public selectedNote(note: MeetingNote): void {
        this.noteSelected.emit(note);
    }

    public selectedAttendee(): void {
        this.attendeeSelected.emit();
    }

    public transform(value: string, limit = 25, completeWords = false, ellipsis = '...') {
        if (completeWords) {
            limit = value.substr(0, 13).lastIndexOf(' ');
        }
        return `${value.substr(0, limit)}${ellipsis}`;
    }

    public openPanel(panelName: string) {
        if (this.mode === 'create') {
            // if (panelName === 'notes' || panelName === 'actions' || panelName === 'decisions') {
            //     return;
            // } else {
                this._panel = panelName;
                this._meetingService.MeetingPanel(this._panel);
            // }
        } else {
            this._panel = panelName;
            this._meetingService.MeetingPanel(this._panel);
        }
    }
}
