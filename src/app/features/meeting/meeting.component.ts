import {
    Component,
    ElementRef,
    OnInit,
    OnDestroy
} from '@angular/core';

import { Subscription } from 'rxjs';
import { TimerObservable } from 'rxjs/observable/TimerObservable';
import {
    MeetingAgenda,
    MeetingModel,
    UserProfile
} from 'src/app/shared/models';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { MeetingAgendaRepositoryService } from '../../shared/repositories';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { UrlService } from 'src/app/shared/services/url.service';
import { LoadingService, BreadcrumbService } from 'src/app/shared/services';
import { MeetingStatusService } from 'src/app/shared/repositories/features/meeting';


@Component({
    selector: 'app-meeting',
    templateUrl: './meeting.component.html',
    styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit, OnDestroy {
    public feature = 'Meeting';
    public mode: string;
    public canEdit: boolean;

    public modalReference: any;
    public options: NgbModalOptions;
    public optionsModel: string[] = [];


    public progress = 0;
    timer = 'running';
    private _Topic: MeetingAgenda;
    private _user: UserProfile;
    private _meeting: MeetingModel;
    private _meetingId: string;
    private _alive: boolean;

    private _showPanel: string;
    PageTitle: string;
    PageHeading: string;
    sub: Subscription;
    instanceId: string;
    meetingId: string;

    //#region Get / Set
    public get Topic(): MeetingAgenda {
        return this._Topic;
    }

    public set Topic(v: MeetingAgenda) {
        this._Topic = v;
    }

    public get ShowPanel(): string {
        return this._showPanel;
    }

    public set ShowPanel(v: string) {
        this._showPanel = v;
    }

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    public get alive(): boolean {
        return this._alive;
    }

    public set alive(v: boolean) {
        this._alive = v;
    }

    constructor(
        private _meetingService: MeetingService,
        private _route: ActivatedRoute,
        private router: Router,
        private _meetingAgendaRepo: MeetingAgendaRepositoryService,
        private _modalService: NgbModal,
        private _profileService: ProfileService,
        private _loadingService: LoadingService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this.meeting = new MeetingModel();
        this.alive = true;
        this.PageHeading = 'Edit Meeting';
        this.PageTitle = 'Meeting Planned';
        this._showPanel = 'objective';
        this.options = {
            size: 'lg'
        };
        this.meeting.meetingAgendaCollection.push(new MeetingAgenda());
        this.Topic = this.meeting.meetingAgendaCollection[0];
    }

    ngOnInit() {
        this.user = this._profileService.get();
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);

        if (this.meetingId === '') {
            this._meetingService.create(this.user).subscribe(meeting => {
                this._meetingService.joinGroup(this.meetingId);
                this.meeting = meeting;
                this.PageHeading = 'Create Meeting';
                this.PageTitle = 'Meeting Planned';
                this._breadcrumbService.LastUpdated();
                this._meetingService.Meeting(meeting, this.user);
                this.canEdit = true;
            });
        } else {
            this._meetingService.joinGroup(this.meetingId);
            this._meetingService.sendGroupMessage({
                task: 'getmeeting',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: 'get data'
            }, this.meetingId);
        }
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            if (this.mode) {
                this._meeting.status = this.mode;
                switch (this.mode) {
                    case 'create':
                    this.PageHeading = 'Create Meeting';
                    break;
                    case 'edit':
                    this.PageHeading = 'Edit Meeting';
                    break;
                    case 'view':
                    this.PageHeading = 'View Meeting';
                    break;
                    case 'run':
                    this.PageHeading = 'Run Meeting';
                    this.startTimer();
                    break;
                }
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        }
        });

    }

    ngOnDestroy() {
    }

    public createAgenda(agenda: MeetingAgenda): void {
        this._meetingAgendaRepo.createAgenda(this.user, agenda)
            .subscribe((createdAgenda: MeetingAgenda) => {
                this.meeting.meetingAgendaCollection.push(createdAgenda);
                this.Topic = new MeetingAgenda();
            }, (err) => {
            });
    }

    public closeModal(content: ElementRef): void {
        this.modalReference.close();
    }

    public dismissModal(): void {
        this.modalReference.dismiss();
    }

    public onChange() {
    }

    public startTimer() {
        this.timer = 'running';
        let devider = 60;
        if (this.meeting) {
            if (this.meeting.duration) {
                if (this.meeting.duration > 0) {
                    devider = this.meeting.duration;
                }
            }
        }
        const incrementor = (devider / 100);
        const runner = incrementor * 100000;
        const timer = TimerObservable.create(runner, runner);
        this.sub = timer.subscribe(t => {
            if (this.timer === 'running') {
                this.progress = (this.progress + incrementor);
                console.log(this.progress);
                if (this.progress > 100) {
                    this.stopTimer();
                }
            }
        });
    }

    public stopTimer() {
        this.timer = 'paused';
        this.sub.unsubscribe();
    }
}
