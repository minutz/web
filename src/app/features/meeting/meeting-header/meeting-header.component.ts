import { Component, OnInit } from '@angular/core';

import { ProfileService } from '../../../shared/services/profile.service';
import { MeetingService } from '../../../shared/services/meeting.service';
import { MeetingTitleService } from '../../../shared/repositories/features/meeting';
import { BreadcrumbService, UrlService } from '../../../shared/services';

import { MeetingModel, UserProfile } from '../../../shared/models';


@Component({
  selector: 'app-meeting-header',
  templateUrl: './meeting-header.component.html',
  styleUrls: ['./meeting-header.component.scss']
})

export class MeetingHeaderComponent implements OnInit {
  instanceId: string;
  meetingId: string;
  _user: UserProfile;
  _meeting: MeetingModel;
  canEdit: boolean;
  mode: string;

  public get user(): UserProfile {
    return this._user;
  }

  public get meetingName() {
    return this._meeting.name;
  }

  public get meeting(): MeetingModel {
    return this._meeting;
  }

  public set meeting(v: MeetingModel) {
    this._meeting = v;
  }

  public set user(v: UserProfile) {
    this._user = v;
  }

  public set meetingName(value: string) {
    this.meetingNameChange(value);
  }

  constructor(private _profileService: ProfileService,
    private _meetingService: MeetingService,
    private _meetingTitleService: MeetingTitleService,
    private _urlService: UrlService,
    public _breadcrumbService: BreadcrumbService
  ) {
    this._meeting = new MeetingModel();
  }

  ngOnInit(): void {
    this._user = this._profileService.get();
    this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
    this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
    this.mode = this._urlService.getModeQueryString(window.location.href);
    this._meetingService.meetingMessageReceived.subscribe((message: any) => {
      this._meeting = message.data.meeting;
      this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
    });
    this._meetingService.meetingNameChange.subscribe((message: any) => {
      this._meeting.name = message.data.message;
    });
  }

  public meetingNameChange(value: any) {
    this._meeting.name = value;
    this._meetingService.sendGroupMessage({
      task: 'meetingNameChange',
      instanceId: this.instanceId,
      meetingId: this.meetingId,
      data: { title : this.meeting.name }
    }, this.meetingId);
  }
}
