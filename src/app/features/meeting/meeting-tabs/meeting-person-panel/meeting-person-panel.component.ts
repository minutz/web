import { Component, Input, OnInit } from '@angular/core';
import { MeetingAttendee, MeetingModel, UserProfile } from '../../../../shared/models';
import {
    UrlService,
    RoleService,
    StatusService,
    MeetingService,
    ProfileService
} from '../../../../shared/services';
import {
    MeetingAttendeeService
} from '../../../../shared/repositories/features/meeting';
import { BreadcrumbService } from '../../../../shared/services';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-meeting-person-panel',
    templateUrl: './meeting-person-panel.component.html',
    styleUrls: ['./meeting-person-panel.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingPersonPanelComponent implements OnInit {
    person: any = { picture: '' };
    Show: boolean;
    @Input() showPanel: string;
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;

    private _statusCollection: Array<string>;
    public get statusCollection(): Array<string> {
        return this._statusCollection;
    }

    public set statusCollection(v: Array<string>) {
        this._statusCollection = v;
    }

    private _roleCollection: Array<string>;
    public get roleCollection(): Array<string> {
        return this._roleCollection;
    }

    public set roleCollection(v: Array<string>) {
        this._roleCollection = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _currentAttendee: MeetingAttendee;
    public get currentAttendee(): MeetingAttendee {
        return this._currentAttendee;
    }

    public set currentAttendee(v: MeetingAttendee) {
        this._currentAttendee = v;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _statusService: StatusService,
        private _roleService: RoleService,
        private _meetingAttendeeService: MeetingAttendeeService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
    }

    ngOnInit() {
        this._statusCollection = this._statusService.MeetingAttendeeStatusCollection;
        this._roleCollection = this._roleService.MeetingRoleCollection;
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._user = this._profileService.get();
        this.Show = false;

        this._meetingService.meetingMessageReceived
            .subscribe((message: any) => {
                this._meeting = message.data.meeting;
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });

        this._meetingService.updatePersonMessage.subscribe((message: any) => {
        });

        this._meetingService._CurrentAttendee$
            .subscribe(attendee => {
                attendee.status = this.pascalCase(attendee.status);
                attendee.role = this.pascalCase(attendee.role);
                this._currentAttendee = attendee;
                this.Show = true;
        });
    }

    public pascalCase(input: string): string {
        return input.charAt(0).toUpperCase() + input.slice(1);
    }
    public onStatusChange($event): void {
        this._currentAttendee.status = $event.value;
        console.log(this._currentAttendee.status);
        this._meetingService.sendGroupMessage({
            task: 'updatePerson',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {person : this._currentAttendee }
        }, this.meetingId);
    }
    public onRoleChange($event): void {
        this._currentAttendee.role = $event.value;
        console.log(this._currentAttendee.role);
        this._meetingService.sendGroupMessage({
            task: 'updatePerson',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {person : this._currentAttendee }
        }, this.meetingId);
    }
    update() {
        if (this._currentAttendee.referenceId) {
            if (this._currentAttendee.referenceId !== '') {
                this._currentAttendee.instanceId = this.instanceId;
                this._meetingService.sendGroupMessage({
                    task: 'updatePerson',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: {person : this._currentAttendee }
                }, this.meetingId);
            }
        }
    }
}
