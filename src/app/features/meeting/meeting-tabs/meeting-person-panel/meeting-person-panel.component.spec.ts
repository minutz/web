import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingPersonPanelComponent } from './meeting-person-panel.component';

describe('MeetingPersonPanelComponent', () => {
  let component: MeetingPersonPanelComponent;
  let fixture: ComponentFixture<MeetingPersonPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingPersonPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingPersonPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
