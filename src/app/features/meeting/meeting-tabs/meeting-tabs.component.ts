import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';

import {
    MeetingAction,
    MeetingAgenda,
    MeetingDecision,
    MeetingModel,
    UserProfile
} from '../../../shared/models';

import {NgbDateStruct, NgbModal, NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import {MeetingService} from '../../../shared/services/meeting.service';
import {NotificationsService} from '../../../shared/services/notifications.service';
import {ProfileService} from '../../../shared/services/profile.service';
import {StatusService} from '../../../shared/services/status.service';
import {BreadcrumbService, UrlService} from '../../../shared/services';

@Component({
    selector: 'app-meeting-tabs',
    templateUrl: './meeting-tabs.component.html',
    styleUrls: ['./meeting-tabs.component.scss']
})
export class MeetingTabsComponent implements OnInit, OnChanges {
    mode: string;
    canEdit: boolean;
    instanceId: string;
    meetingId: string;

    public modalReference: any;
    public AvailableAttendees: Array<any>;
    public AvailableActionAttendees: Array<any>;
    public StatusCollection: Array<any>;
    actionDueDate: NgbDateStruct;
    actionDate: NgbDateStruct;
    // meetingTime: NgbTimeStruct;
    dateFormat: { year: number, month: number };
    toggleTime = false;
    @ViewChild('tabs') tabs: NgbTabset;
    @Output() createAgenda = new EventEmitter;


    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _showPanel: string;
    public get showPanel(): string {
        return this._showPanel;
    }

    private _time: Date;
    public get time(): Date {
        return this._time;
    }

    public set time(v: Date) {
        this._time = v;
    }

    @Input()
    public set showPanel(v: string) {
        this._showPanel = v;
        console.log(v);
    }

    private _selectedAgenda: MeetingAgenda;
    public get selectedAgenda(): MeetingAgenda {
        return this._selectedAgenda;
    }

    // @Input()
    public set selectedAgenda(v: MeetingAgenda) {
        this._selectedAgenda = v;
    }

    private _selectedAction: MeetingAction;
    public get selectedAction(): MeetingAction {
        return this._selectedAction;
    }

    public set selectedAction(v: MeetingAction) {
        this._selectedAction = v;
    }

    private _selectedDecision: MeetingDecision;
    public get selectedDecision(): MeetingDecision {
        return this._selectedDecision;
    }

    public set selectedDecision(v: MeetingDecision) {
        this._selectedDecision = v;
    }

    constructor(
        private _modalService: NgbModal,
        private _meetingService: MeetingService,
        private _notificationService: NotificationsService,
        private _profileService: ProfileService,
        private _statusService: StatusService,
        private _breadcrumbService: BreadcrumbService,
        private _urlService: UrlService
    ) {
        this._selectedAgenda = new MeetingAgenda();
    }

    ngOnInit() {
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._breadcrumbService._LastUpdated$.subscribe(q => {
            this.mode = this._urlService.getModeQueryString(window.location.href);
        });
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService._Panel$.subscribe(panel => {
            this._showPanel = panel;
        });

        this._meetingService._CurrentAction$.subscribe(action => {
            console.log(action);
            this._selectedAction = action;
        });

        this._meetingService._CurrentDecision$.subscribe(decision => {
            this._selectedDecision = decision;
        });

        this._meetingService._CurrentAgenda$.subscribe(agendaItem => {
            this._selectedAgenda = agendaItem;
        });
        this.selectedAction = new MeetingAction();
        this.setStatusCollection();
    }

    ngOnChanges() {
        if (this.selectedAgenda.id) {
            this.tabs.select('agenda');
            this._showPanel = 'agenda';
        }
    }

    setDate(inputDate: any): void {
        const time = new Date(this.meeting.time);
        const date = new Date(
            inputDate.year,
            inputDate.month,
            inputDate.day,
            time.getHours(),
            time.getMinutes());
        this._meeting.date = date;
        this._meetingService.Meeting(this._meeting, this.user);
    }

    // public updateShowPanel(panelName: string): void {
    //     this._showPanel = panelName;
    //     console.log(panelName);
    // }

    public updateAgendaPurpose(editorContent: string): void {
        this.meeting.purpose = editorContent;
        this._meetingService.Meeting(this._meeting, this.user);
    }

    public updateAgendaOutcome(editorContent: string): void {
        this.meeting.outcome = editorContent;
        this._meetingService.Meeting(this._meeting, this.user);
    }

    public updateAgendaText(editorContent: string): void {
        if (this._meeting) {
            if (this.meeting.meetingAgendaCollection) {
                this._meeting.meetingAgendaCollection.forEach(agenda => {
                    if (agenda.id === this.selectedAgenda.id) {
                        agenda.agendaText = editorContent;
                    }
                });
                this._meetingService.Meeting(this._meeting, this._user);
            }
        }
        // this.saveAgendaText(editorContent);
    }

    public agendaEditorBlurred(editorContent: string): void {
        if (this._meeting) {
            if (this._meeting.meetingAgendaCollection) {
                this._meeting.meetingAgendaCollection.forEach(agenda => {
                    if (agenda.id === this.selectedAgenda.id) {
                        agenda.agendaText = editorContent;
                    }
                });
                this._meetingService.Meeting(this._meeting, this._user);
            }
        }
        // this.saveAgendaText(editorContent);
    }

    public agendaEditorFocused(editorContent: string): void {
        // this.saveAgendaText(editorContent);
    }

    public updateActionText(editorContent: string): void {
        this.saveActionText(editorContent);
    }

    public actionEditorBlurred(editorContent: string): void {
        this.saveActionText(editorContent);
    }

    public actionEditorFocused(editorContent: string): void {
        this.saveActionText(editorContent);
    }

    public updateDecisionText(editorContent: string): void {
        this.savedDecisionText(editorContent);
    }

    public decisionEditorBlurred(editorContent: string): void {
        this.savedDecisionText(editorContent);
    }

    public decisionEditorFocused(editorContent: string): void {
        this.savedDecisionText(editorContent);
    }

    public addTopic(): void {
        this.createAgenda.emit(this.selectedAgenda);
    }

    public removeTopic(): void {
        this.selectedAgenda = new MeetingAgenda();
    }

    public InviteUsers(): void {
        // this._meetingRepo.sendMeetingInvitations(this.meeting.id)
        //     .subscribe(() => {
        //         this.saveMeeting();
        //     }, (err) => {
        //         this.displayError(err);
        //         this.modalReference.close();
        //     });
    }

    public openModal(content: ElementRef) {
        if (this.meeting.meetingAttendeeCollection.length > 0) {
            this.modalReference = this._modalService.open(content);
        } else {
            this.saveMeeting();
        }
    }

    public closeModal() {
        this.saveMeeting();
        this.modalReference.close();
    }

    public dismissModal() {
        this.modalReference.dismiss();
    }

    public saveMeeting() {
        this._meetingService.Meeting(this.meeting, this.user);
        // .subscribe(() => {
        //     this._router.navigate(['/dashboard']);
        // }, (err) => {
        //     this.displayError(err);
        // });
    }

    public displayError(err: string): void {
        // const userMessage: Notification = new Notification({
        //   type: 'Error',
        //   message: err
        // });
        //
        // this._notificationService.add(new Notification(userMessage));
    }

    public saveAgendaText(editorContent: string) {
        if (this.meeting) {
            this.meeting.meetingAgendaCollection.forEach((agenda) => {
                if (agenda.id === this.selectedAgenda.id) {
                    agenda.agendaText = editorContent;
                    agenda.agendaHeading = this.selectedAgenda.agendaHeading;
                }
            });
            this._meetingService.Meeting(this._meeting, this.user);
        }
    }

    public saveActionText(editorContent: string) {
        if (this.meeting) {
            this.meeting.meetingActionCollection.forEach((action) => {
                if (action.id === this.selectedAction.id) {
                    action.actionText = editorContent;
                }
            });
            this._meetingService.Meeting(this._meeting, this.user);
        }
    }

    public savedDecisionText(editorContent: string) {
        if (this.meeting) {
            this.meeting.meetingDecisionCollection.forEach((decision) => {
                if (decision.id === this.selectedDecision.id) {
                    decision.decisionComment = editorContent;
                    decision.decisionText = this.selectedDecision.decisionText;
                }
            });
            this._meetingService.Meeting(this._meeting, this.user);
        }
    }

    public setAvailableAttendees() {
        this.AvailableAttendees = [];
        this.AvailableAttendees.push({id: 0, text: 'Select a user'});
        if (this._meeting.availableAttendeeCollection) {
            for (let i = 0; i < this._meeting.availableAttendeeCollection.length; i++) {
                const currentUser = this._meeting.availableAttendeeCollection[i];
                let username = '';
                if (currentUser.name) {
                    username = currentUser.name;
                } else {
                    username = currentUser.personIdentity;
                }

                this.AvailableAttendees.push({id: currentUser.personIdentity, text: username});
            }
        }
    }

    //#region Status
    public setStatusCollection() {
        this.StatusCollection = [];
        this.StatusCollection.push({id: 0, text: 'Status'});
        const statusCollection = this._statusService.StatusCollection;
        if (statusCollection) {
            for (let i = 0; i < statusCollection.length; i++) {
                const currentStatus = statusCollection[i];
                this.StatusCollection.push({id: currentStatus, text: currentStatus});
            }
        }
    }

    public setDueDate($event: any) {
        console.log($event);
    }

    public statusSelected($event: any) {
        console.log($event);
    }

    //#endregion

    //#region Action Attendees

    //#endregion

}
