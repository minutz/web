import {Component, Input, OnInit} from '@angular/core';
import {
    BreadcrumbService,
    MeetingService,
    ProfileService,
    UrlService,
    StatusService
} from 'src/app/shared/services';
import {
    MeetingAction, MeetingAttendee, MeetingModel
} from 'src/app/shared/models';
import { MeetingAttendeeService } from 'src/app/shared/repositories/features/meeting';
import {UserProfile} from 'src/app/shared/models';
import {MatDatepickerInputEvent} from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormControl } from '@angular/forms';
import * as _moment from 'moment';
import {default as _rollupMoment} from 'moment';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';
const moment = _rollupMoment || _moment;

export const ACTION_FORMATS = {
    parse: {
      dateInput: 'LL',
    },
    display: {
      dateInput: 'LL',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };
@Component({
    selector: 'app-meeting-actions',
    templateUrl: './meeting-actions.component.html',
    styleUrls: ['./meeting-actions.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingActionsComponent implements OnInit {
    @Input() showPanel: string;
    public actions: Array<MeetingAction>;
    raisedDatePicker = new FormControl(moment());
    dueDatePicker = new FormControl(moment());
    oldText: string;
    title: string;
    isActive: boolean;
    selectedAction: MeetingAction;
    selectedAttendee: MeetingAttendee;
    raisedDate: any;
    dueDate: any;
    status: string;
    meetingAttendees: Array<MeetingAttendee>;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    busy: boolean;
    editorConfig: AngularEditorConfig = {
        editable: true
    };

    private _statusCollection: Array<string>;
    public get statusCollection(): Array<string> {
        return this._statusCollection;
    }

    public set statusCollection(v: Array<string>) {
        this._statusCollection = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    public get action() {
        return this._action;
    }

    private _action: string;
    public set action(value: string) {
        this._action = value;
    }
    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }
    constructor(
        private _urlService: UrlService,
        private _profileService: ProfileService,
        public _breadcrumbService: BreadcrumbService,
        private _meetingService: MeetingService,
        private _statusService: StatusService,
        private _attendeeService: MeetingAttendeeService) {
        this.selectedAction = new MeetingAction();
        this.selectedAction.actionTitle = '';
        this.oldText = '';
    }

    ngOnInit() {
        this.editorConfig.editable = false;
        this._user = this._profileService.get();
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._statusCollection = this._statusService.MeetingActionStatusCollection;

        this._meetingService.meetingActionCollectionMessageReceived
            .subscribe((message: any) => {
            this.actions = message.data.actions;
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
                this.editorConfig.editable = this.canEdit;
        });

        this._meetingService.meetingMessageReceived
            .subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            this.editorConfig.editable = this.canEdit;
        });

        this._meetingService.assignAttendeeActionMessage
            .subscribe( (message: any) => {
            this.actions.forEach( (action: any) => {
                if (action.id === this.selectedAction.id) {
                   // action.personId = this.selectedAttendee;
                }
            });
        });

        this._attendeeService.getCollection(this._user, this.meetingId, this.instanceId)
        .subscribe (att => {
            if (att) {
                att.forEach( (item: any) => {
                    if (!item.name) {
                        if (item.email) {
                            item.name = item.email.split('@')[0].replace('.', ' ');
                        }
                    }
                });
            }
            this._meetingService.meetingAttendees = this.meetingAttendees = att;
        });

        this._meetingService.assignCreatedActionMessage
            .subscribe((message: any) => {
                if (message.data.condition) {
                    const x = moment(message.data.message).local().format();
                    const date = new Date(x);
                    this.raisedDate = new Date(message.data.message);
                    this.raisedDatePicker.setValue(this.raisedDate);
                    this.selectedAction.createdDate = message.data.message;
                }
            });

        this._meetingService.assignDueActionMessage
            .subscribe((message: any) => {
                if (message.data.condition) {
                    this.dueDate = new Date(message.data.message);
                    this.dueDatePicker.setValue(this.dueDate);
                    this.selectedAction.dueDate = this.dueDate;
                }
            });

        this._meetingService.assignTextActionMessage
            .subscribe((message: any) => {
                console.log(message.data.message);
                this.oldText = message.data.message;
                this.busy = false;
            });

        this._meetingService.assignTitleActionMessage
            .subscribe((message: any) => {
                console.log(message.data.message);
                this.busy = false;
            });

        this._meetingService._CurrentAction$
            .subscribe(action => {
                if (this.selectedAction) {
                    if (this.oldText) {
                        if (this.oldText !== this.action) {
                            this.save(this.action, this.selectedAction.id);
                        }
                    }
                }
            this.actions.forEach( (item: any)  => {
                if (item.id === action.id) {
                    this.editorConfig.editable = true;
                    this.selectedAttendee = null;
                    this.selectedAction = item;
                    this.raisedDatePicker.setValue(item.createdDate);
                    this.action = this.selectedAction.actionText;
                    if (item.dueDate === '0001-01-01T00:00:00') {
                        this.dueDate = new Date();
                    } else {
                        this.dueDate = new Date(item.dueDate);
                    }
                    this.dueDatePicker.setValue(this.dueDate);
                    if (action.personId) {
                        this.meetingAttendees.forEach( (attendee: any) => {
                            if (attendee.id === action.personId) {
                                this.selectedAttendee = attendee;
                                this.selectedAttendee.referenceId = this.meetingId;
                                this.selectedAttendee.instanceId = this.instanceId;
                            }
                        });
                    }
                    if (action.isComplete) {
                        this.status = 'Complete';
                    } else {
                        this.status = 'Pending';
                    }
                    this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
                    this.editorConfig.editable = this.canEdit;
                    this.oldText = this.selectedAction.actionText;
                }
            });
        });
    }

    titleChange() {
        if (this.selectedAction) {
            this._meetingService.sendGroupMessage({
                task: 'assignTitleAction',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: {
                    title: this.selectedAction.actionTitle,
                    actionId: this.selectedAction.id
                }
            }, this.instanceId);
        }
        this.busy = true;
    }

    blur() {
        this.selectedAction.actionText = this.action;
        if (this.selectedAction) {
            this._meetingService.sendGroupMessage({
                task: 'assignTextAction',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: {
                    text: this.selectedAction.actionText,
                    actionId: this.selectedAction.id
                }
            }, this.instanceId);
        }
        this.busy = true;
    }

    save(text: string, id: string) {
        this.selectedAction.actionText = this.action;
        if (this.selectedAction) {
            this._meetingService.sendGroupMessage({
                task: 'assignTextAction',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: {
                    text: text,
                    actionId: id
                }
            }, this.instanceId);
        }
        this.busy = true;
    }

    addRaisedEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        const x = moment(event.value).local().format();
        const date = new Date(x);
        const nowDate = this._meetingService.toLocalTime(date);
        this._meetingService.sendGroupMessage({
            task: 'assignCreatedAction',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { created: nowDate, actionId: this.selectedAction.id }
        }, this.instanceId);
    }

    addDueEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        const x = moment(event.value).local().format();
        const date = new Date(x);
        const nowDate = this._meetingService.toLocalTime(date);
        this._meetingService.sendGroupMessage({
            task: 'assignDueAction',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { due: nowDate, actionId: this.selectedAction.id }
        }, this.instanceId);
    }

    onStatusChange(status: any) {
        if (status.value === 'Pending') {
            this._meetingService.sendGroupMessage({
                task: 'assignStatusAction',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: { isComplete: false, actionId: this.selectedAction.id }
            }, this.instanceId);
        } else {
            this._meetingService.sendGroupMessage({
                task: 'assignStatusAction',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: { isComplete: true, actionId: this.selectedAction.id }
            }, this.instanceId);
        }
        this.status = status.value;
        console.log(this.status);
        // this.busy = true;
    }

    onAssignedAttendeeChange(att: MeetingAttendee) {
        this._meetingService.sendGroupMessage({
            task: 'assignAttendeeAction',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: { email: att.id, actionId: this.selectedAction.id }
        }, this.instanceId);
    }

}
