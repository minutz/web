import {Component, Input, OnInit} from '@angular/core';
import {MeetingService} from 'src/app/shared/services/meeting.service';
import {ProfileService} from 'src/app/shared/services/profile.service';
import {
    MeetingObjectivePurposeService
} from 'src/app/shared/repositories/features/meeting/objective-purpose/meeting-objective-purpose.service';
import {BreadcrumbService, UrlService} from 'src/app/shared/services';

import {MeetingModel, UserProfile} from 'src/app/shared/models';
import 'quill-mention';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-meeting-tab-objective',
    templateUrl: './meeting-tab-objective.component.html',
    styleUrls: ['./meeting-tab-objective.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})

export class MeetingTabObjectiveComponent implements OnInit {
    @Input() showPanel: string;
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;
    busy: boolean;
    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true
    };

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _outcome: string;
    public get outcome() {
        return this._outcome;
        // return this.meeting.outcome;
    }

    public set outcome(value: string) {
        this._outcome = value;
        // this.objectiveChange(value);
    }

    private _purpose: string;
    public set purpose(value: string) {
        this._purpose = value;
        // this.outcomeChange(value);
    }

    public get purpose() {
        return this._purpose;
        // return this.meeting.purpose;
    }

    constructor(
        private _profileService: ProfileService,
        private _meetingService: MeetingService,
        private _meetingObjectivePurposeService: MeetingObjectivePurposeService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService) {
        this._meeting = new MeetingModel();
        this.instanceId = '';
    }

    ngOnInit() {
        this.busy = false;
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this.meeting = message.data.meeting;
            this.purpose = message.data.meeting.purpose;
            this.outcome = message.data.meeting.outcome;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            this.editorConfig.editable = this.canEdit;
        });

        this._meetingService.meetingObjectiveChangeMessage.subscribe((message: any) => {
            this.purpose = message.data.meeting.purpose;
            this.busy = false;
        });
        this._meetingService.meetingOutcomeChangeMessage.subscribe((message: any) => {
            this.outcome = message.data.meeting.outcome;
            this.busy = false;
        });
    }

    outcomeBlur() {
        // objective = outcome
        this._meetingService.sendGroupMessage({
            task: 'meetingOutcomeChange',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {outcome: this.outcome}
        }, this.meetingId);
        this.busy = true;
    }

    purposeBlur() {
        // outcome = purpose
        this._meetingService.sendGroupMessage({
            task: 'meetingObjectiveChange',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {purpose: this.purpose}
        }, this.meetingId);
        this.busy = true;
    }
}
