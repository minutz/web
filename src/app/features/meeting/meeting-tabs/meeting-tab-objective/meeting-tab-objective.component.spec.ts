import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingTabObjectiveComponent } from './meeting-tab-objective.component';

describe('MeetingTabObjectiveComponent', () => {
  let component: MeetingTabObjectiveComponent;
  let fixture: ComponentFixture<MeetingTabObjectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingTabObjectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingTabObjectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
