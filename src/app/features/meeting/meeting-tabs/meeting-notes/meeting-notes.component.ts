import { Component, Input, OnInit } from '@angular/core';
import { MeetingModel, MeetingNote, UserProfile } from 'src/app/shared/models';
import { BreadcrumbService, MeetingService, ProfileService, UrlService } from 'src/app/shared/services';
import { NoteService } from 'src/app/shared/repositories/features/meeting';
import { NgbDateStruct } from 'node_modules/@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';
const moment = _rollupMoment || _moment;
export const NOTES_FORMATS = {
    parse: {
      dateInput: 'LL',
    },
    display: {
      dateInput: 'LL',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };
@Component({
    selector: 'app-meeting-notes',
    templateUrl: './meeting-notes.component.html',
    styleUrls: ['./meeting-notes.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingNotesComponent implements OnInit {
    raisedDatePicker = new FormControl(moment());
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    selectedNote: MeetingNote;
    raisedDate: any;
    isActive: boolean;
    busy: boolean;
    oldText: string;
    @Input() showPanel: string;

    private _user: UserProfile;
    Show: boolean;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _note: string;
    public get note() {
        return this._note;
    }

    public set note(value: string) {
        this._note = value;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    editorConfig: AngularEditorConfig = {
        editable: true
    };

    constructor(
        public _breadcrumbService: BreadcrumbService,
        private _meetingService: MeetingService,
        private _profileService: ProfileService,
        private _noteService: NoteService,
        private _urlService: UrlService
    ) {
        this.selectedNote = new MeetingNote();
        this.selectedNote.noteText = '';
        this.isActive = false;
        this.oldText = '';
    }

    ngOnInit() {
        this.editorConfig.editable = false;
        this.canEdit = false;
        this.busy = false;
        this.Show = false;
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);

        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
        });

        this._meetingService._CurrentNote$.subscribe(x => {
            if (this.selectedNote) {
                if (this.oldText) {
                    if (this.oldText !== this.note) {
                        const note = this.selectedNote;
                        this.save(note);
                    }
                }
            }
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            this.editorConfig.editable = this.canEdit;
            this.isActive = true;
            this.selectedNote = x;
            this.raisedDate = '';
            this.note = x.noteText;
            if (x.createdDate.toString() !== '0001-01-01T00:00:00') {
                if (x.createdDate) {
                    const d = new Date(x.createdDate);
                    this.raisedDate = {
                        year: d.getFullYear(),
                        month: d.getMonth() + 1,
                        day: d.getDate()
                    };
                } else {
                    const d = new Date();
                    this.raisedDate = {
                        year: d.getFullYear(),
                        month: d.getMonth() + 1,
                        day: d.getDate()
                    };
                }
            }
            this.oldText = this.selectedNote.noteText;
            this.Show = true;
        });
        this._meetingService.assignNoteUpdateMessage.subscribe((message: any) => {
            this.selectedNote = message.data.note;
            this.note = this.selectedNote.noteText;
            this.oldText = this.selectedNote.noteText;
            this.busy = false;
        });
    }

    update() {
        if (this.selectedNote.referenceId) {
            if (this.selectedNote.referenceId !== '') {
                this.selectedNote.instanceId = this.instanceId;
                this._noteService.update(
                    this._user,
                    this.selectedNote)
                    .subscribe(
                        () => {
                        // this._meetingService.CurrentDecision(res);
                        // this._breadcrumbService.LastUpdated();
                    }
                );
            }
        }
    }

    addRaisedEvent(date: NgbDateStruct) {
        if (this.selectedNote.referenceId) {
            if (this.selectedNote.referenceId !== '') {
                this.selectedNote.instanceId = this.instanceId;
                const stringDate = `${date.month}/${date.day}/${date.year}`;
                this.selectedNote.createdDate = new Date(stringDate);
                // this._meetingService.CurrentNote(this.selectedNote);
                this.selectedNote.noteText = this.note;
                this.selectedNote.instanceId = this.instanceId;
                this._meetingService.sendGroupMessage({
                    task: 'assignNoteUpdate',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: {note : this.selectedNote }
                }, this.meetingId);
                this.busy = true;
            }
        }
    }

    blur() {
        this.selectedNote.noteText = this.note;
        this.selectedNote.instanceId = this.instanceId;
        this._meetingService.sendGroupMessage({
            task: 'assignNoteUpdate',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {note : this.selectedNote }
        }, this.meetingId);
        this.busy = true;
    }

    save(note: any) {
        this.selectedNote.noteText = this.note;
        this.selectedNote.instanceId = this.instanceId;
        this._meetingService.sendGroupMessage({
            task: 'assignNoteUpdate',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {note : note }
        }, this.meetingId);
        this.busy = true;
    }
}
