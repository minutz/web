import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingTabAgendaComponent } from './meeting-tab-agenda.component';

describe('MeetingTabAgendaComponent', () => {
  let component: MeetingTabAgendaComponent;
  let fixture: ComponentFixture<MeetingTabAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingTabAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingTabAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
