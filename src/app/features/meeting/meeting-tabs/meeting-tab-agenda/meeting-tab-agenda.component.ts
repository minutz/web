import {Component, OnInit, Input, ElementRef} from '@angular/core';
import {
    MeetingAgenda,
    MeetingModel,
    UserProfile
} from 'src/app/shared/models';
import { ProfileService } from 'src/app/shared/services/profile.service';
import { MeetingService } from 'src/app/shared/services/meeting.service';
import {
    MeetingAgendaService
} from 'src/app/shared/repositories/features/meeting/agenda/meeting-agenda.service';
import {BreadcrumbService, UrlService} from 'src/app/shared/services';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-meeting-tab-agenda',
    templateUrl: './meeting-tab-agenda.component.html',
    styleUrls: ['./meeting-tab-agenda.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingTabAgendaComponent implements OnInit {

    isActive: boolean;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    busy: boolean;

    private _selectedAgenda: MeetingAgenda;
    public get selectedAgenda(): MeetingAgenda {
        return this._selectedAgenda;
    }

    public set selectedAgenda(v: MeetingAgenda) {
        this._selectedAgenda = v;
    }

    @Input() showPanel: string;
    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }
    private _oldAgenda: string;

    private _agenda: string;
    public get agenda() {
        return this._agenda;
    }

    public set agenda(value: string) {
        this._agenda = value;
    }

    editorConfig: AngularEditorConfig = {
        editable: true,
        spellcheck: true
    };

    constructor(
        private elementRef: ElementRef,
        private _profileService: ProfileService,
        private _meetingAgendaService: MeetingAgendaService,
        private _meetingService: MeetingService,
        private _agendaService: MeetingAgendaService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService) {
        this.selectedAgenda = new MeetingAgenda();
        this.selectedAgenda.agendaText = '';
        this.selectedAgenda.agendaHeading = '';
        this.instanceId = '';
        this.isActive = false;
    }

    ngOnInit() {
        this.editorConfig.editable = false;
        this.canEdit = false;
        this._user = this._profileService.get();
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this.busy = false;
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
            // this.editorConfig.editable = this.canEdit;
        });
        this._meetingService._CurrentAgenda$.subscribe(agendaItem => {
            if (this.selectedAgenda.id !== '') {
                const editor = document.getElementById('editor');
                if (editor.innerHTML !== this._oldAgenda) {
                    this.save(this.selectedAgenda.id, editor.innerHTML);
                }
            }
            this.editorConfig.editable = this.canEdit;
            this.isActive = true;
            this._selectedAgenda = agendaItem;
            this.agenda = agendaItem.agendaText;
        });
        this._meetingService.assignOrderAgendaMessage.subscribe((message: any) => {
            this.selectedAgenda.order = message.data.order;
            this.busy = false;
        });
        this._meetingService.assignDurationAgendaMessage.subscribe((message: any) => {
            this._selectedAgenda.duration = message.data.duration;
            this.busy = false;
        });
        this._meetingService.assignTitleAgendaMessage.subscribe((message: any) => {
            this._selectedAgenda.agendaHeading = message.data.agendaHeading;
            this.busy = false;
        });
        this._meetingService.assignTextAgendaMessage.subscribe((message: any) => {
            if (this.selectedAgenda.id === message.data.agendaId) {
                this._selectedAgenda.agendaText = message.data.agendaText;
                this.agenda = message.data.agendaText;
                this._oldAgenda = this.agenda;
            } else {
                this._meetingService.sendGroupMessage({
                    task: 'getMeetingCollectionAgenda',
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: 'get data'
                }, this.meetingId);
            }

            this.busy = false;
        });
        this._meetingService.assignAttendeeAgendaMessage.subscribe((message: any) => {
            this.selectedAgenda.meetingAttendeeId = message.data.message;
            this.busy = false;
        });
    }

    updateAgendaHeading(): void {
        this.selectedAgenda.instanceId = this.instanceId;
        this._meetingService.sendGroupMessage({
            task: 'assignTitleAgenda',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {
                AgendaHeading : this.selectedAgenda.agendaHeading,
                agendaId: this.selectedAgenda.id
            }
        }, this.meetingId);
        this.busy = true;
    }

    blur() {
        this.selectedAgenda.agendaText = this.agenda;
        this.selectedAgenda.instanceId = this.instanceId;
        this._meetingService.sendGroupMessage({
            task: 'assignTextAgenda',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {
                AgendaText : this.agenda,
                agendaId: this.selectedAgenda.id }
        }, this.meetingId);
        this.busy = true;
    }

    save(agendaId: string, agendaText: string) {
        this._meetingService.sendGroupMessage({
            task: 'assignTextAgenda',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {
                AgendaText : agendaText,
                agendaId: agendaId }
        }, this.meetingId);
        this.busy = true;
    }

    update(value: any): void {
        if (this.selectedAgenda) {
            this.selectedAgenda.duration = value;
            this.selectedAgenda.instanceId = this.instanceId;
            this._meetingService.sendGroupMessage({
                task: 'assignDurationAgenda',
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: {
                    duration : this.selectedAgenda.duration,
                    agendaId: this.selectedAgenda.id
                }
            }, this.meetingId);
            this.busy = true;
        }
    }
}
