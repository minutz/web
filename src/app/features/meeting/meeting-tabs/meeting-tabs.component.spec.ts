import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingTabsComponent } from './meeting-tabs.component';

describe('MeetingTabsComponent', () => {
  let component: MeetingTabsComponent;
  let fixture: ComponentFixture<MeetingTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
