import { Component, Input, OnInit } from '@angular/core';
import { MeetingDecision, UserProfile, MeetingModel } from 'src/app/shared/models';
import {BreadcrumbService, MeetingService, ProfileService, UrlService} from 'src/app/shared/services';
import { DecisionService } from 'src/app/shared/repositories/features/meeting';
import { MatDatepickerInputEvent } from '@angular/material';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { FormControl } from '@angular/forms';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';
const moment = _rollupMoment || _moment;
export const DECISION_FORMATS = {
    parse: {
      dateInput: 'LL',
    },
    display: {
      dateInput: 'LL',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
    },
  };
@Component({
    selector: 'app-meeting-decisions',
    templateUrl: './meeting-decisions.component.html',
    styleUrls: ['./meeting-decisions.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingDecisionsComponent implements OnInit {
    raisedDatePicker = new FormControl(moment());
    @Input() showPanel: string;
    selectedDecision: MeetingDecision;
    raisedDate: any;
    isActive: boolean;
    instanceId: string;
    meetingId: string;
    canEdit: boolean;
    mode: string;
    busy: boolean;
    oldText: string;

    private _user: UserProfile;
    Show: boolean;
    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    private _decision: string;
    public get decision() {
        return this._decision;
    }

    public set decision(value: string) {
        this._decision = value;
    }
    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }

    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    editorConfig: AngularEditorConfig = {
        editable: true
    };
    constructor(
        public _breadcrumbService: BreadcrumbService,
        private _meetingService: MeetingService,
        private _profileService: ProfileService,
        private _decisionService: DecisionService,
        private _urlService: UrlService
    ) {
        this.selectedDecision = new MeetingDecision();
        this.selectedDecision.decisionText = '';
        this.selectedDecision.decisionComment = '';
        this.isActive = false;
        this.instanceId = '';
    }

    ngOnInit() {
        this.busy = false;
        this.Show = false;
        this.editorConfig.editable = false;
        this._user = this._profileService.get();
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService._CurrentDecision$
            .subscribe(x => {
                this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
                this.editorConfig.editable = this.canEdit;

                if (this.selectedDecision.id !== '') {
                    if (this.decision !== this.oldText) {
                        const decision = this.selectedDecision;
                        decision.decisionText = this.decision;
                        this.save(decision);
                    }
                }

                this.isActive = true;
                this.selectedDecision = x;
                this.decision = this.selectedDecision.decisionText;
                this.raisedDate = '';
                if (x.createdDate) {
                    this.raisedDatePicker.setValue(x.createdDate);
                }
                this.Show = true;
        });

        this._meetingService.assignDecisionUpdateMessage
            .subscribe((message: any) => {
                if (this.selectedDecision.id === message.data.decisionId) {
                    this.selectedDecision.decisionText = message.data.decision.decisionText;
                    this.decision = message.data.decision.decisionText;
                    this.oldText = this.decision;
                } else {
                    this._meetingService.sendGroupMessage({
                        task: 'getMeetingCollectionDecision',
                        instanceId: this.instanceId,
                        meetingId: this.meetingId,
                        data: 'get data'
                    }, this.meetingId);
                }
            this.busy = false;
        });
    }

    addRaisedEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        this.raisedDate = event.value;
        if (this.selectedDecision.referenceId) {
            if (this.selectedDecision.referenceId !== '') {
                this.selectedDecision.createdDate = this.raisedDate;
                this.selectedDecision.instanceId = this.instanceId;
                this._decisionService.update(this._user,  this.selectedDecision).subscribe();
            }
        }
    }

    update() {
        if (this.selectedDecision.referenceId) {
            if (this.selectedDecision.referenceId !== '') {
                this.blur();
            }
        }
    }

    blur() {
        this.selectedDecision.decisionText = this.decision;
        this.selectedDecision.instanceId = this.instanceId;
        this._meetingService.sendGroupMessage({
            task: 'assignDecisionUpdate',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {decision : this.selectedDecision }
        }, this.meetingId);
        this.busy = true;
    }

    save(decision: MeetingDecision) {
        this._meetingService.sendGroupMessage({
            task: 'assignDecisionUpdate',
            instanceId: this.instanceId,
            meetingId: this.meetingId,
            data: {decision : decision }
        }, this.meetingId);
        this.busy = true;
    }
}
