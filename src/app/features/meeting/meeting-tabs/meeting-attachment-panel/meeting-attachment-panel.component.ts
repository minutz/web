import { Component, OnInit, Input } from '@angular/core';
import { MeetingAttachment, MeetingModel, UserProfile } from '../../../../shared/models';
import { MeetingService } from '../../../../shared/services/meeting.service';
import { ProfileService } from '../../../../shared/services/profile.service';
import {BreadcrumbService, UrlService} from '../../../../shared/services';
import { PDFProgressData } from 'pdfjs-dist';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {MY_FORMATS} from '../../../dashboard/dashboard.component';

@Component({
    selector: 'app-meeting-attachment-panel',
    templateUrl: './meeting-attachment-panel.component.html',
    styleUrls: ['./meeting-attachment-panel.component.scss'],
    providers: [{
        provide: DateAdapter,
        useClass: MomentDateAdapter,
        deps: [MAT_DATE_LOCALE]
    }, {
        provide: MAT_DATE_FORMATS,
        useValue: MY_FORMATS}, {
        provide: MAT_DATE_LOCALE, useValue: 'en-gb'},
    ]
})
export class MeetingAttachmentPanelComponent implements OnInit {

    @Input() showPanel: string;
    loading: string;
    pdfLink: string;
    fileLink: string;
    instanceId: string;
    meetingId: string;
    mode: string;
    canEdit: boolean;

    private _user: UserProfile;
    public get user(): UserProfile {
        return this._user;
    }
    public set user(v: UserProfile) {
        this._user = v;
    }

    private _meeting: MeetingModel;
    public get meeting(): MeetingModel {
        return this._meeting;
    }
    public set meeting(v: MeetingModel) {
        this._meeting = v;
    }

    private _selectedAttachment: MeetingAttachment;
    public get selectedAttachment(): MeetingAttachment {
        return this._selectedAttachment;
    }

    public set selectedAttachment(v: MeetingAttachment) {
        this._selectedAttachment = v;
    }

    constructor(
        private _meetingService: MeetingService,
        private _profileService: ProfileService,
        private _urlService: UrlService,
        public _breadcrumbService: BreadcrumbService
    ) {
        this.selectedAttachment = new MeetingAttachment();
        this.selectedAttachment.fileData = '';
        this.selectedAttachment.fileExtension = 'pdf';
        this.pdfLink = '';
        this.fileLink = '';
        this.loading = 'Select an attachment.';
    }

    ngOnInit() {
        this.instanceId = this._urlService.getInstanceQueryString(window.location.href);
        this.meetingId = this._urlService.getMeetingQueryString(window.location.href);
        this.mode = this._urlService.getModeQueryString(window.location.href);
        this._user = this._profileService.get();
        this._meetingService.meetingMessageReceived.subscribe((message: any) => {
            this._meeting = message.data.meeting;
            this.canEdit = this._meetingService.canEdit(this.meeting, this.user, this.mode);
        });
        this._meetingService._CurrentAttachment$.subscribe(attachment => {
            let ext = '';
            this._selectedAttachment = attachment;
            if (attachment.fileExtension) {
                ext = attachment.fileExtension;
                if (attachment.fileExtension === '') {
                    const s = attachment.name.split('.');
                    ext = s[s.length - 1];
                }
            } else {
                const s = attachment.name.split('.');
                ext = s[s.length - 1];
            }
            attachment.fileExtension = ext;
            const viewer = (document.getElementsByClassName('ng2-pdf-viewer-container')[0] as HTMLElement);
            if (attachment.fileExtension === 'pdf') {
                this.pdfLink = attachment.src;
                this.fileLink = '';
                this.loading = 'Getting attachment.';
                viewer.style.display = 'block';
            } else {
                this.fileLink = attachment.src;
                this.pdfLink = '';
                this.loading = '';
                viewer.style.display = 'none';
            }
        });
    }

    onProgress(progressData: PDFProgressData) {
        this.loading = '';
    }
}
