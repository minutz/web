import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingAttachmentPanelComponent } from './meeting-attachment-panel.component';

describe('MeetingAttachmentPanelComponent', () => {
  let component: MeetingAttachmentPanelComponent;
  let fixture: ComponentFixture<MeetingAttachmentPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingAttachmentPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingAttachmentPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
