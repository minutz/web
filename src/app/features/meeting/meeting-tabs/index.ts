export  * from './meeting-tab-objective/meeting-tab-objective.component';
export * from './meeting-tab-agenda/meeting-tab-agenda.component';
export * from './meeting-attachment-panel/meeting-attachment-panel.component';
export * from './meeting-decisions/meeting-decisions.component';
export * from './meeting-person-panel/meeting-person-panel.component';
export * from './meeting-notes/meeting-notes.component';
export * from './meeting-actions/meeting-actions.component';
export * from './meeting-tabs.component';
