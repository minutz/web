import { NewUser } from "src/app/shared/models/new-user.model";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { UserProfile } from "src/app/shared/models";
import { ProfileService, NotificationsService } from "src/app/shared/services";
import { AdminRepositoryService } from "src/app/shared/repositories";
import { Notification } from "src/app/shared/models/notification";
import { UserService } from "src/app/shared/repositories/features/admin/user.service";
import { AdminService } from "src/app/shared/services/Hub/admin.service";

@Component({
    selector: "app-admin-action-bar",
    templateUrl: "./admin-action-bar.component.html",
    styleUrls: ["./admin-action-bar.component.scss"]
})
export class AdminActionBarComponent implements OnInit {
    public modalReference: any;
    public newUserForm: FormGroup;
    public buttonState = false;
    instanceId: string;
    public users: any;

    private _user: UserProfile;

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    constructor(
        private _modalService: NgbModal,
        private router: Router,
        private route: ActivatedRoute,
        private _fb: FormBuilder,
        private _profileService: ProfileService,
        private _adminRepositoryService: AdminRepositoryService,
        private adminService: AdminService,
        private _notificationService: NotificationsService,
        private _userService: UserService
    ) {
        this.route.queryParams.subscribe(params => {
            this.instanceId = params["instanceId"];
        });
    }

    ngOnInit() {
        this._user = this._profileService.get();
        this._userService.users(this._user).subscribe(u => {
            this.users = u;
        });
    }

    public openNewUserModal(newuser: ElementRef): void {
        this.buildForm();
        this.modalReference = this._modalService.open(newuser);
    }

    public closeModal(newuser: ElementRef): void {
        this.modalReference.close();
        this.clearModalData();
    }

    public dismissModal(): void {
        this.modalReference.dismiss();
        this.clearModalData();
    }

    public CreateUsers(): void {
        const newUser = new NewUser({
            email: this.newUserForm.get("email").value,
            firstName: this.newUserForm.get("firstname").value,
            lastName: this.newUserForm.get("lastname").value,
            status: "Pending",
            profilePicture: "default",
            // shortName: this.newUserForm.get('shortname').value,
            // organization: this.newUserForm.get('organisation').value,
            // representing: this.newUserForm.get('representing').value,
            role: "User"
        });
        this.adminService.sendAdminMessage(
            {
                task: "CreateUser",
                instanceId: this.instanceId,
                meetingId: "",
                data: { user: newUser }
            },
            this.instanceId
        );
        this.dismissModal();
        this.clearModalData();
        this.router.navigate(["/admin"], {
            queryParams: {
                instanceId: this.instanceId
            }
        });
        // this._userService.user(this.user, newUser)
        //     .subscribe(() => {
        //         this.dismissModal();
        //         this.clearModalData();
        //         this.createUserNotification('Success', 'New user created');
        //         this.router.navigate(['/admin']);
        //     }, (error: Error) => {
        //         this.dismissModal();
        //         this.clearModalData();
        //         this.createUserNotification('Error', error.message);
        //     });
    }

    public saveUpdates(): void {
        this.buttonState = true;
        this._adminRepositoryService.saveUpdates(this.user).subscribe(
            () => {
                this.buttonState = false;
                this.createUserNotification("Success", "Updates saved.");
            },
            (error: Error) => {
                this.buttonState = false;
                this.createUserNotification("Error", error.message);
            }
        );
    }

    public buyLicences(): void {
        this.buttonState = true;
        this._adminRepositoryService.buyMoreLicences(this.user).subscribe(
            () => {
                this.buttonState = false;
                this.createUserNotification("Success", "Updates saved.");
            },
            (error: Error) => {
                this.buttonState = false;
                this.createUserNotification("Error", error.message);
            }
        );
    }

    private buildForm(): void {
        this.newUserForm = this._fb.group({
            firstname: ["", [Validators.required]],
            lastname: ["", [Validators.required]],
            // shortname: ['', [Validators.required]],
            email: ["", [Validators.required]]
            // organisation: ['', [Validators.required]],
            // representing: ['', [Validators.required]],
        });
    }

    private createUserNotification(type: string, message: string) {
        const userMessage: Notification = new Notification({
            type: type,
            message: message
        });
        this._notificationService.add(new Notification(userMessage));
    }

    private clearModalData(): void {
        this.newUserForm.reset();
    }
}
