import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsBillingsComponent } from './accounts-billings.component';

describe('AccountsBillingsComponent', () => {
  let component: AccountsBillingsComponent;
  let fixture: ComponentFixture<AccountsBillingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountsBillingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsBillingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
