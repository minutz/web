import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComapnySettingsComponent } from './comapny-settings.component';

describe('ComapnySettingsComponent', () => {
  let component: ComapnySettingsComponent;
  let fixture: ComponentFixture<ComapnySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComapnySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComapnySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
