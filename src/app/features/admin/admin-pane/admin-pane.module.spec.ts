import { AdminPaneModule } from './admin-pane.module';

describe('AdminPaneModule', () => {
  let adminPaneModule: AdminPaneModule;

  beforeEach(() => {
    adminPaneModule = new AdminPaneModule();
  });

  it('should create an instance', () => {
    expect(adminPaneModule).toBeTruthy();
  });
});
