import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UserService } from "src/app/shared/repositories/features/admin/user.service";
import {
    AccountsBillingsComponent,
    ComapnySettingsComponent,
    UserActivityComponent,
    UserRolesComponent
} from ".";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatFormFieldModule, MatSelectModule } from "@angular/material";
import {
    MatDatepickerModule,
    MatSnackBarModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule
} from "@angular/material";
import { FormsModule } from "@angular/forms";

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDatepickerModule,
        MatSnackBarModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatInputModule,
        MatIconModule,
        MatNativeDateModule,
        MatExpansionModule,
        MatListModule,
        MatDividerModule,
        MatDialogModule,
        MatButtonModule,
        FormsModule
    ],
    declarations: [
        UserRolesComponent,
        UserActivityComponent,
        ComapnySettingsComponent,
        AccountsBillingsComponent
    ],
    providers: [UserService]
})
export class AdminPaneModule {}
