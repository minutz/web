import { Component, OnInit } from "@angular/core";
import { ProfileService, UrlService } from "src/app/shared/services";
import { UserProfile, UserRolesUser } from "src/app/shared/models";
import { AdminService } from "../../../../shared/services/Hub/admin.service";
import { ActivatedRoute } from "@angular/router";

@Component({
    selector: "app-user-roles",
    templateUrl: "./user-roles.component.html",
    styleUrls: ["./user-roles.component.scss"]
})
export class UserRolesComponent implements OnInit {
    mode: string;
    meetingId: string;
    instanceId: string;
    users: UserRolesUser[];
    requests: UserProfile[];

    private _user: UserProfile;
    statusCollection: Array<string>;

    public get user(): UserProfile {
        return this._user;
    }

    public set user(v: UserProfile) {
        this._user = v;
    }

    constructor(
        private adminService: AdminService,
        private _profileService: ProfileService,
        private _urlService: UrlService,
        private route: ActivatedRoute
    ) {
        this.users = [];
        this.requests = [];
        this.route.queryParams.subscribe(params => {
            this.instanceId = params["instanceId"];
        });
        this.statusCollection = [];
        this.statusCollection.push("Guest");
        this.statusCollection.push("User");
        this.statusCollection.push("Admin");
    }

    ngOnInit() {
        this.user = this._profileService.get();

        this.adminService.registerAdminServerEvents();
        this.adminService.startConnection();
        this.adminService.connectionEstablished.subscribe(() => {
            this.adminService.joinAdminHub(this.instanceId);
        });

        this.adminService.instanceJoinMessage.subscribe((message: any) => {
            console.log(message);
            this.adminService.sendAdminMessage(
                {
                    task: "GetUsers",
                    instanceId: this.instanceId,
                    meetingId: this.meetingId,
                    data: "get data"
                },
                this.instanceId
            );
        });

        this.adminService.deleteUserAsync.subscribe((data: any) => {
            const deleteUser = this.users.find(
                i => i.id === data.message.user.id
            );
            if (deleteUser) {
                console.log(deleteUser);
            }
        });

        this.adminService.getUsersAsync.subscribe((data: any) => {
            console.log(data);
            this.users = [];
            data.data.users.forEach((user: any) => {
                this.users.push(user);
            });
        });

        this.adminService.createUserAsync.subscribe((message: any) => {
            console.log(message);
            this.users.push(message.data.user);
        });
    }

    onStatusChange($event: any, user: any) {
        console.log($event.value);
        user.role = $event.value;
        this.adminService.sendAdminMessage(
            {
                task: "UpdateUser",
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: { user: user }
            },
            this.instanceId
        );
    }

    deleteAction(user: any) {
        this.adminService.sendAdminMessage(
            {
                task: "DeleteUser",
                instanceId: this.instanceId,
                meetingId: this.meetingId,
                data: { user: user }
            },
            this.instanceId
        );
    }
}
