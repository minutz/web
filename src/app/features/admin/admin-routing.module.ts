import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
    AdminComponent,
    UserRolesComponent,
    AccountsBillingsComponent,
    ComapnySettingsComponent,
    UserActivityComponent


} from '.';

const routes: Routes = [
    {
        path: 'admin',
        component: AdminComponent,
        children: [
            {
                path: 'userroles',
                component: UserRolesComponent,
                outlet: 'pane'
            },
            {
                path: 'companysettings',
                component: ComapnySettingsComponent,
                outlet: 'pane'
            },
            {
                path: 'useractivity',
                component: UserActivityComponent,
                outlet: 'pane'
            },
            {
                path: 'accounts',
                component: AccountsBillingsComponent,
                outlet: 'pane'
            },
            {
                path: '',
                component: UserRolesComponent,
                outlet: 'pane'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AdminRoutingModule { }


