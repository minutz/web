import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { AdminPaneModule } from "./admin-pane/admin-pane.module";
import { AdminComponent, AdminMenuComponent, AdminActionBarComponent } from ".";
import { AdminRoutingModule } from "./admin-routing.module";
import {
    MatDatepickerModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatNativeDateModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatDialogModule,
    MatButtonModule
} from "@angular/material";

@NgModule({
    imports: [
        RouterModule,
        ReactiveFormsModule,
        AdminPaneModule,
        AdminRoutingModule,
        MatDatepickerModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        MatInputModule,
        MatIconModule,
        MatNativeDateModule,
        MatSelectModule,
        MatExpansionModule,
        MatListModule,
        MatDividerModule,
        MatDialogModule,
        MatButtonModule
    ],
    exports: [AdminComponent],
    declarations: [AdminComponent, AdminMenuComponent, AdminActionBarComponent],
    providers: []
})
export class AdminModule {}
