import { MatRadioModule } from '@angular/material/radio';
import { PrintPreviewComponent } from './shared/components/bread-crumb/modal-windows/print-preview/print-preview.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {LOCALE_ID, NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {
    NgbModule,
    NgbDropdownModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    NgbCalendar
} from '@ng-bootstrap/ng-bootstrap';
import { LoadingModule } from 'ngx-loading';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { SideBarComponent } from './shared/components/side-bar/side-bar.component';
import { UserNotificationsComponent } from './shared/components/user-notifications/user-notifications.component';
import { VersionComponent } from './shared/components/version/version.component';

import { AppRoutingModule } from './app-routing.module';
import { SharedPipesModule } from './shared/pipes/shared-pipes/shared-pipes.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import {
    MatDatepickerModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatIconModule,
    MatNativeDateModule,
    MatSelectModule,
    MatExpansionModule,
    MatListModule,
    MatDividerModule,
    MatDialogModule, MatGridListModule, MatMenuModule, MatButtonModule
} from '@angular/material';
import { HeaderAlertsComponent } from './shared/components/header/header-alerts/header-alerts.component';
import { HeaderMessagesComponent } from './shared/components/header/header-messages/header-messages.component';
import { HeaderSearchComponent } from './shared/components/header/header-search/header-search.component';
import { HeaderLogoComponent } from './shared/components/header/header-logo/header-logo.component';
import { HeaderUserComponent } from './shared/components/header/header-user/header-user.component';
import { SideBarNavigationComponent } from './shared/components/side-bar/side-bar-navigation/side-bar-navigation.component';
import {
    SideBarCompanyInformationComponent
} from './shared/components/side-bar/side-bar-company-information/side-bar-company-information.component';
import { ChatComponent } from 'src/app/shared/components/chat/chat.component';
import { AdminModule } from './features/admin/admin.module';
import { MeetingModule} from './features/meeting/meeting.module';
import { DashboardModule } from './features/dashboard/dashboard.module';
import { DashboardRepositoryModule } from './shared/repositories/features/dashboard-repository.module';
import { SharedModule } from './shared/shared.module';
import { DemoComponent } from './features/demo/demo.component';
import { LayoutModule } from '@angular/cdk/layout';
import { QuillModule } from 'ngx-quill';
import { SendInvitationComponent } from './shared/components/bread-crumb/modal-windows/send-invitation/send-invitation.component';
import { MeetingInfoComponent } from './shared/components/bread-crumb/modal-windows/meeting-info/meeting-info.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { IntercomModule } from 'ng-intercom';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        SignUpComponent,
        NotFoundComponent,
        HeaderComponent,
        SideBarComponent,
        UserNotificationsComponent,
        VersionComponent,
        HeaderAlertsComponent,
        HeaderMessagesComponent,
        HeaderSearchComponent,
        HeaderLogoComponent,
        HeaderUserComponent,
        SideBarNavigationComponent,
        SideBarCompanyInformationComponent,
        DemoComponent,
        PrintPreviewComponent,
        SendInvitationComponent,
         ChatComponent,
        MeetingInfoComponent,
        ForgotPasswordComponent
    ],
    exports: [
        MatNativeDateModule
    ],
    imports: [
        MatSnackBarModule, // material modules
        MatFormFieldModule,
        QuillModule,
        // DndModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        PdfViewerModule,
        MatInputModule,
        MatIconModule,
        DragDropModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        MatSelectModule,
        MatExpansionModule,
        MatListModule,
        MatDividerModule,
        MatDialogModule,
        BrowserAnimationsModule,
        MatTooltipModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        SharedModule.forRoot(),
        HttpClientModule,
        RouterModule,
        NgbModule.forRoot(),
        NgbTimepickerModule.forRoot(),
        NgbDatepickerModule.forRoot(),
        NgbDropdownModule.forRoot(),
        BrowserModule,
        LoadingModule,
        SharedPipesModule,
        // Select2Module,
        FormsModule,
        ReactiveFormsModule,
        AdminModule,
        DashboardModule,
        DashboardRepositoryModule,
        MeetingModule,
        AppRoutingModule,
        MatGridListModule,
        MatMenuModule,
        MatButtonModule,
        LayoutModule,
        IntercomModule.forRoot({
            appId: 'oili1ns3', // from your Intercom config
            updateOnRouterChange: true // will automatically run `update` on router event changes. Default: `false`
        })
    ],
    providers: [
        { provide: LOCALE_ID, useValue: 'en-gb' }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        PrintPreviewComponent,
        SendInvitationComponent,
        MeetingInfoComponent
    ]
})
export class AppModule { }
