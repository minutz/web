import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from '../shared/services/session-storage.service';
import { LocalStorageService } from '../shared/services/local-storage.service';
import { SignUpRepositoryService } from '../shared/repositories';
import { environment } from '../../environments/environment';
import { trigger, state, style, transition, animate } from '@angular/animations';
import {MatSnackBar} from '@angular/material';
import { UserProfile} from '../shared/models/user-profile';
const { version: appVersion } = require('package.json');

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  public appVersion;
  public loading = false;
  minutzApp = 'minutz-app';
  minutz = 'minutz';
  private _errorMessage: string;
  public get errorMessage(): string {
    return this._name;
  }
  public set errorMessage(v: string) {
    this._errorMessage = v;
  }

  private _name: string;
  public get name(): string {
    return this._name;
  }
  public set name(v: string) {
    this._name = v;
  }

  private _email: string;
  public get email(): string {
    return this._email;
  }
  public set email(v: string) {
    this._email = v;
  }

  private _username: string;
  public get username(): string {
    return this._username;
  }
  public set username(v: string) {
    this._username = v;
  }

  private _password: string;
  public get password(): string {
    return this._password;
  }
  public set password(v: string) {
    this._password = v;
  }

  private _password2: string;
  public get password2(): string {
    return this._password2;
  }
  public set password2(v: string) {
    this._password2 = v;
  }

  constructor(
    private router: Router,
    public _signUpRepository: SignUpRepositoryService,
    private _sessionStorageService: SessionStorageService,
    private _localStorageService: LocalStorageService,
    public snack: MatSnackBar,
    // public userProfile: UserProfile
  ) { }

  ngOnInit() {
    this.appVersion = appVersion;
    this.errorMessage = '';
  }

  public register(): void {
    const name = this._name;
    const email = this._email;
    const username = this._username;
    const password = this._password;
    let RefInstanceId = '';
    const r = sessionStorage.getItem('r');
    if (r && r !== '') {
        RefInstanceId = r;
    } else {
        if (this._localStorageService.get('refInstanceId')) {
            RefInstanceId = JSON.parse(this._localStorageService.get('refInstanceId'));
          }
    }

    let refMeetingId = '';
    if (this._localStorageService.get('refInstanceId')) {
      refMeetingId = JSON.parse(this._localStorageService.get('refInstanceId'));
    }
    let role = 'User';
    if (this._localStorageService.get('referenceRole')) {
      role = 'Guest';
    }
    if (this._password) {
      if (this._password === this.password2) {
        if (!this._email) {
            this.snack.open(
                `Please provide a valid email address.`,
                '',
                { duration : 2000 });
        } else {
          this.loading = true;
          this._signUpRepository.signup(name, email, username, password, RefInstanceId, refMeetingId, role)
            .subscribe((userProfile: any) => {
              const profile = userProfile;
              const jsonProfile = JSON.stringify(profile);
              this._sessionStorageService.set(this._sessionStorageService.access_token, userProfile.accessToken);
              this._sessionStorageService.set(this._sessionStorageService.expires_at, userProfile.tokenExpire);
              this._sessionStorageService.set(this._sessionStorageService.id_token, userProfile.id_token);
              this._sessionStorageService.set(this._sessionStorageService.minutzApp, userProfile.sub);
              this._sessionStorageService.set(this._sessionStorageService.refInstanceId, userProfile.instanceId);
              this._sessionStorageService.set(this._sessionStorageService.referenceRole, userProfile.role);
              this._sessionStorageService.set(this._sessionStorageService.minutz, jsonProfile);
              this._localStorageService.set(this._sessionStorageService.minutzApp, userProfile.sub);
              this.loading = false;
              window.location.href = environment.redirectUri;
            }, err => {
             // this.loading = false;
                this.snack.open(
                    `oops ! Looks like your password is either not strong enough or your you have us a invalid email`,
                    '',
                    { duration : 6000 });
            });
        }
      } else {
          this.snack.open(
              `Passwords do not match.`,
              '',
              { duration : 2000 });
      }
    } else {
        this.snack.open(
            `Please provide a password.`,
            '',
            { duration : 2000 });
    }
  }

}
