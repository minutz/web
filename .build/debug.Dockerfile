FROM  node:11
LABEL node="11"
RUN apt-get update \
&& apt-get install -y build-essential \
&& npm config set always-auth false \
&& npm install yarn \
&& yarn config set strict-ssl false \
&& yarn global add typescript \
&& yarn global add @angular/cli \
&& yarn global add @angular/compiler-cli \
&& npm i @angular-devkit/build-angular

WORKDIR /build
COPY package.json package.json
COPY angular.json angular.json
COPY tsconfig.json tsconfig.json
COPY tslint.json tslint.json
RUN yarn install
