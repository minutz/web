# MINUTZ-WEB BUILD IMAGE

The Dockerfile is used to create the UI build image, used when building the application in the build pipeline.

Any alterations to the infrastructure when building the web application this will need to be updated.

Examples of update:

- Angular version update
- Webpack update
- OS version update
- Nodejs update
- NPM update