FROM microsoft/aspnetcore-build:2.0
LABEL NAME="DEVOPS ANGULAR CLI IMAGE"
LABEL CLI-VERSION="2.3.1"
WORKDIR C:\\App
RUN npm install -g @angular/cli; npm install -g @angular/compiler-cli --save; npm install typescript@'>=2.1.0 <2.4.0'; npm install typescript@'2.4.2';