FROM nginx
ADD nginx.conf /host/path/nginx.conf
COPY ./dist/minutz /usr/share/nginx/html
EXPOSE 80
