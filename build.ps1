$tag = $args[0];
$packageJson = Get-Content 'package.json' -raw | ConvertFrom-Json;
$packageJson.version=$tag
$packageJson | ConvertTo-Json  | set-content 'package.json'